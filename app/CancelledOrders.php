<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CancelledOrders extends Model
{
     protected $table = 'cancelled_order_details';
    protected $primaryKey = 'order_id';
	public $timestamps = false;
	// protected $dates = ['created_at', '	start_job_date', 'end_job_date'];
    protected $fillable = [
    	'order_details_id',
    	'order_id',
    	's_id',
    	'amount',
    	'quantity'
];
    
    public function order()
  {
    return $this->belongsTo('App\Order', 'order_id');
  }
}
