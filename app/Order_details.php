<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order_details extends Model
{
     protected $table = 'order_details';
    protected $primaryKey = 'order_details_id';
	  public $timestamps = false;
	// protected $dates = ['created_at', '	start_job_date', 'end_job_date'];
      protected $fillable = [
        'order_details_id',
        'order_id',
        's_id',
        'amount',
        'quatity'
  ];
    
    public function order()
  {
    return $this->belongsTo('App\Order', 'order_id');
  }
}
