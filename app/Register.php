<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Register extends Model
{
    protected $table = 'per_info';
    protected $primaryKey = 'regId';
    public $timestamps = false;
    protected $fillable = [
				'fullName',
				'nickName',
				'phn_no',
				'city_id',
				'terr_id',
				's_id',
				'password',
				'r_id',
				'imei1',
				'imei2',
				'phn_reg_no',
				'job_status',
				'image',
				'email',
				'pri_lat',
				'pri_long',
				'session_id',
				
    ];
    
     public function address()
  {
    return $this->belongsTo('App\Address', 'regId');
  }

}
