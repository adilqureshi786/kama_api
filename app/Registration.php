<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Registration extends Model
{
    protected $table = 'register';
    protected $primaryKey = 'regId';
    public $timestamps = false;
    protected $fillable = [
				'fullName',
				'nickName',
				'phn_no',
				'phn_no2',
				'phn_no3',
				'city_id',
                'status',
                'session_id',
                'area',
                'address',
                'image',
                'verified_by',
				'others'

    ];
}
