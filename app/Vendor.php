<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vendor extends Model
{
      protected $table = 'vendor';
    public $timestamps = false;
    protected $fillable = [
				'shop_name',
				'owner_name',
				'shop_address',
				'longitude',
				'latitude',
				'landline',
				'mobile1',
				'mobile2',
				'mobile3',
				'image',
				'image2',
				'image3',
				'cnic',
				's_id',
				'others'
    ];
}
