<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Services;
use App\Cities;
use App\Territory;
use App\Register;
use App\Users;
use App\Assign_jobs;
use App\Orders;
use App\CancelledOrders;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Http\Controllers\kamayUserController;

class ShowProfile extends Controller
{
    
    protected $kamayUserController;
    public function __construct(kamayUserController $kamayUserController)
    {
        $this->kamayUserController = $kamayUserController;
    }
    
    public function getSessionId($session_id)
    {
        if ($session_id != '')
        {
            $user = DB::table('per_info')->where('session_id', $session_id)->first();
            if ($user === null)
            {
                return response()->json(array(
                    "status" => "false",
                    "message" => "Session Expired",
                    "user" => null
                ));
            }
            else
            {
                return '1';
            }
        }
        else
        {
            return response()
                ->json(array(
                "status" => "false",
                "message" => "Authentication Error",
                "user" => null
            ));
        }
    }


    public function getServices()
    {
        return response()->json(DB::table('services')
                ->where([['status', 1], ['parent_id', 0]])
                ->select('s_id', 'name')
                ->get() , 200);
    }

    public function showCities()
    {
        return response()->json(DB::table('city')
                ->where('status', 1)
                ->get(['city_id', 'name']));
    }

    public function showTerritories($city_id)
    {
        return response()->json(Territory::where([['status', '=', 1], ['city_id', '=', $city_id], ])->orWhere('city_id', 1)
                ->get(['terr_id', 'name']) , 200);
    }

    public function userRegistration(Request $request)
    {

        $register = $request->isMethod('put') ? Register::findOrFail($request->regId) : new Register;
        $register->regId = $request->input('regId');
        $register->fullName = $request->input('fullName');
        $register->nickName = $request->input('nickName');
        $register->phn_no = $request->input('phn_no');
        $register->city_id = $request->input('city_id');
        $register->terr_id = $request->input('terr_id');
        $register->s_id = $request->input('s_id');
        $register->r_id = $request->input('r_id');
        $register->imei1 = $request->input('imei1');
        $register->s_id = $request->input('s_id');
        $register->phn_reg_no = $request->input('phn_reg_no');     
        $filename = '';
         if ($request->hasFile('image')) {
            $image = $request->file('image');
            $filename = rand() . '.' . $image->getClientOriginalExtension();
            $path = $request->file('image')
            ->move(public_path("images/customer") , $filename);
            $register->image = url('images/customer/' . $filename);
        }
        
        $register->save();
        if($request->isMethod('put')){
            DB::table('user_services')->where('reg_id', $register->regId)->delete();
            $parts=explode(",",$request->input('s_id'));
            $parts=array_filter($parts);
            for($i = 0; $i < count($parts); $i++){
                DB::table('user_services')->insert(
                    ['s_id' => $parts[$i], 'reg_id' => $register->regId]
                );
            }
        }else{
            $parts=explode(",",$request->input('s_id'));
            $parts=array_filter($parts);
            for($i = 0; $i < count($parts); $i++){
                DB::table('user_services')->insert(
                    ['s_id' => $parts[$i], 'reg_id' => $register->regId]
                );
            }
        }
        $users = new Users(['regId' => $register->regId, 'r_id' => $register->r_id]);
        $users->save();
        $body = "new_kama";
        // $title = "New kama request";
        $notify_status = 0;
        if($request->input('r_id') == 1){
            //kamay
            $notify_status = 1;
        }
        DB::table('admin_notify')->insert(
            [
                'notify_status' => $notify_status,
                'user_id' => $register->regId
            ]
        );
        
        $this->kamayUserController->backOfficeNotification($body, $register->regId);
        
        return response()
            ->json(['status' => 'true', 'message' => 'data inserted']);
    }

    public function userExistence(Request $request)
    {
        
        $r_id = 1;

        $phn_no = $request->input('phn_no');
        $validator = Validator::make($request->all() , ['phn_no' => 'required', ]);
        if ($validator->fails())
        {
            return response()
                ->json(['errors' => $validator->messages() , 'code' => 422, "status" => "false", "message" => "Required Fields."]);
        }
        else
        {
            $results = DB::table('per_info')->where([['phn_no', $phn_no],['r_id', $r_id]])->select('regId', 'fullName', 'phn_no')->first();
            
            if ($results)
            {
                $someArray = json_encode($results, true);
                $someArray = json_decode($someArray, true);
                $regId = $someArray["regId"];
                $time = md5(date("h:i:sa"));
                $session = ($time . $regId);
                $value = DB::table('per_info')->where([['phn_no', '=', $phn_no], ['r_id', '=', $r_id]])->update(['session_id' => $session]);
                if ($value = 1)
                {
                    return response()
                        ->json(DB::table('per_info')
                        ->join('user_services', 'user_services.reg_id', '=', 'per_info.regId')
                        ->join('services', 'user_services.s_id', '=', 'services.s_id')
                        ->join('city', 'per_info.city_id', '=', 'city.city_id')
                        ->join('territory', 'per_info.terr_id', '=', 'territory.terr_id')
                        ->select('per_info.regId', 'per_info.fullName', 'per_info.nickName', 'per_info.phn_no', 'per_info.email','per_info.vehicle_no','per_info.cnic', 'per_info.age', 'per_info.image','per_info.session_id','per_info.r_id',  DB::raw('group_concat(services.name) as expertise'), 'city.name as city', 'territory.name as territory_name', 'per_info.status')
                        ->where([
                                ['phn_no', '=', $phn_no], 
                                ['r_id', '=', $r_id],
                                ])
                        ->get());
                }
            }
            else
            {
                return response()->json(array(
                    // "status" => "false",
                    // "message" => "Wrong credentials.",
                    // "user" => $results
                ));
            }
        }


    }
    
    public function kamaProfile(Request $request)
    {
        $headers = $request->header('Authentication');
        $response = $this->getSessionId($headers);
        if ($response != '1')
        {
            return $response;
        }
        else
        {
            
                $userSession = DB::table('per_info')->where('session_id', $request->header('Authentication'))
                ->first();
            
                $users = DB::table('per_info')
                    ->join('user_services', 'user_services.reg_id', '=', 'per_info.regId')
                    ->join('services', 'user_services.s_id', '=', 'services.s_id')
                    ->join('city', 'city.city_id', '=', 'per_info.city_id')
                    ->select('per_info.fullName', 'per_info.image', 'per_info.vehicle_no','per_info.email','per_info.cnic', DB::raw('group_concat(services.name) as expertise') , 'city.name as kamay_city', 'per_info.phn_no', 'per_info.regId', 'per_info.pri_lat', 'per_info.pri_long','per_info.rating_number','per_info.rating_count')
                    ->where('per_info.regId', '=', $userSession->regId)
                    ->get();
                    
                    
                   
                    

                if ($users->count() > 0)
                {
                    
                    
                    if($users[0]->rating_number != 0){
                        $users[0]->ratings = $users[0]->rating_number / $users[0]->rating_count;
                    }else{
                        $users[0]->ratings = 0;
                    }
                    
                    
                    return response()
                        ->json(array(
                        "status" => "true",
                        "message" => "Kama Found",
                        'kama_profile' => $users
                    ));
                }

                else
                {
                    return response()->json(array(
                        "status" => "true",
                        "message" => "Kama not Found",
                        'kama_profile' => $users
                    ));
                }
            

             return response()
                    ->json(array(
                    "status" => "true",
                    "data" => $results,
                ));
            
        }
    }

    public function supervisor(Request $request)
    {
        $headers = $request->header('Authentication');
        $response = $this->getSessionId($headers);
        if ($response != '1')
        {
            return $response;
        }
        else
        {
            $user = DB::table('per_info')->where('session_id', $request->header('Authentication'))
                ->first();
            $results = [];
            if($user->status != 0){
                $results = DB::select(DB::raw("select regId, fullName as supervisor_name,phn_no,email, image from per_info where regId = (select verufied_by from per_info where regId =  :reg_id)") , array(
                    'reg_id' => $user->regId
             ));
            }    
                return response()
                    ->json(array(
                        "status" => "true",
                        "message" => "Supervisor Profile",
                        "data" => $results,
                ));
        }
    }

    public function activeJobs(Request $request)
    {
        $headers = $request->header('Authentication');
        $response = $this->getSessionId($headers);
        if ($response != '1')
        {
            return $response;
        }
        else
        {
            $user = DB::table('per_info')->where('session_id', $request->header('Authentication'))
                ->first();

            $results = DB::select(DB::raw("SELECT * FROM (SELECT `assign_jobs`.`order_id`,MAX(services.s_id) AS service_id,services.name, services.service_image,orders.create_at,  orders.latitude, orders.longitude
                FROM `assign_jobs` 
                INNER JOIN `order_details` ON `assign_jobs`.`order_id` = `order_details`.`order_id` 
                INNER JOIN `services` ON order_details.s_id = services.s_id
                INNER JOIN `orders` ON order_details.order_id = orders.order_id
                WHERE `assign_jobs`.`regId` = :regId AND `assign_jobs`.`is_accept` IS NULL
                GROUP BY order_id) T WHERE T.ORDER_ID IS NOT NULL") , array(
                       'regId' => $user->regId
                ));
            return response()
                    ->json(array(
                        "status" => "true",
                        "message" => "Active Jobs",
                        "data" => $results,
                ));
        }
    }
    public function unassignedJobs(Request $request , $latitude, $longitude)
    {
        $headers = $request->header('Authentication');
        $response = $this->getSessionId($headers);
        if ($response != '1')
        {
            return $response;
        }
        else
        {
            $user = DB::table('per_info')->where('session_id', $request->header('Authentication'))
                ->first();
           
            
            $row = $user->s_id;
            $idsArr = explode(',',$row);
             //dd($user);die();
            // $results = DB::select(DB::raw("select os.order_id, GROUP_CONCAT(s.name) as service_name, s.service_image as service_image, DATE_FORMAT(os.create_at, '%d-%b-%y %I:%i%p') as create_at, os.status, os.latitude, os.longitude, os.total_amount from orders os, order_details od, services s where os.status IN ('callcenter') AND os.assign_to is null and od.order_id = os.order_id and s.s_id = od.s_id group by os.order_id ORDER BY os.order_id DESC") , array(
            //           'regId' => $user->regId
            //     ));
            
            $results = DB::table('orders')
                        ->join('services', 'services.s_id', '=', 'orders.s_id')
                        ->select('orders.order_id', 'services.name as service_name', 'services.service_image', DB::raw('DATE_FORMAT(create_at, "%d-%b-%y %h:%i%p") as create_at'), 'orders.status', 'orders.latitude','orders.longitude','orders.total_amount')
                        ->where('orders.status', 'CallCenter')
                        ->whereIn('orders.s_id', $idsArr)
                        ->orderBy('orders.order_id','DESC')
                        ->paginate(50);
                        
           // dd($order);die();
                // $let =24.863903393275887;
                // $lon =67.0733141905002;
                
            
            // dd($results);die();
               
            if (count($results) != 0) {
                    foreach($results as $getjobs){
                        //
                        $getjobs->distance = $this->distanceCalculation($getjobs->latitude, $getjobs->longitude, $latitude, $longitude, "K");
                        
                        $getInterestedWorker = DB::table('job_interest')
                            ->select('customer_info.fullName as cust_name','customer_info.regId as cust_id','job_interest.id as int_id','job_interest.order_id as order_id')
                            ->join('per_info as customer_info','job_interest.reg_id','customer_info.regId')
                            ->where([
                                    ['job_interest.order_id',$getjobs->order_id],
                                    ['customer_info.regId',$user->regId],
                                    ['customer_info.status', 1],
                                    // ['customer_info.job_status', 1]
                                ])
                            ->first();
                            //dump($getInterestedWorker->int_id);die();
                            if ($getInterestedWorker != null) {
                                  $getjobs->applied = "Applied";
                                  
                                 }else{
                                  $getjobs->applied = "";
                                }
                             
                    }//dump($results);die();
                                 
                
            }
                
            return response()
                    ->json(array(
                        "status" => "true",
                        "message" => "Unassigned Jobs",
                        "data" => $results,
                ));
        }
    }
    
    public function applyJob(Request $request)
    {
        $headers = $request->header('Authentication');
        $response = $this->getSessionId($headers);
        if ($response != '1')
        {
            return $response;
        }
        else
        {
            
           $checkData = DB::table('job_interest')
                        ->where([
                                    ['order_id', $request->input('order_id')],
                                    ['reg_id', $request->input('regId')]
                                ])
                        ->first();
                        
            if($checkData != null){
                
                 return response()
                    ->json(array(
                    "status" => "true",
                    "message" => "You have already applied for this job",
                    "data" => false
                ));
                
            }
           
           
             //return $request; exit();
             
            $form_data = DB::table('job_interest')->insert([
                    'order_id' => $request->input('order_id'),
                    'reg_id' => $request->input('regId')
                   
                ]);

           
             return response()
                    ->json(array(
                    "status" => "true",
                    "message" => "You have applied succesfully",
                    "data" => $form_data
                ));
        }
               
    }
    
    
    public function orderDetails($order_id, Request $request)
    {
        $headers = $request->header('Authentication');
        $response = $this->getSessionId($headers);
        if ($response != '1')
        {
            return $response;
        }
        else
        {
            // $results = DB::select(DB::raw("select od.order_id,concat(os.total_amount) as total_amount, s.name, p.regId as customer_id, p.fullName as customer_name, p.image as customer_image, pc.name as city_name, (select name from services where s_id = s.parent_id) as service_name, od.quantity,os.payment_method, os.full_address from  order_details od, services s, orders os, per_info p, city pc   WHERE od.order_id =  :order_id and s.s_id = od.s_id and p.regId = os.regId and p.city_id = pc.city_id and os.order_id = od.order_id and p.r_id= 3") , array(
            //             'order_id' => $order_id,
            //             ));
            $order = DB::table('orders')
                        ->join('services', 'services.s_id', '=', 'orders.s_id')
                        ->select('order_id', 'total_amount', DB::raw('DATE_FORMAT(create_at, "%d-%b-%y %I:%i%p") as service_date'), 'amount_received', 'payment_method', 'assign_to', 'regId', 'full_address','services.name as service_name')
                        
                        ->where('order_id', $order_id)
                        ->first();
            $customer = DB::table('per_info')
                                    ->leftjoin('city', 'city.city_id', '=', 'per_info.city_id')
                                    ->leftjoin('territory', 'territory.terr_id', '=', 'per_info.terr_id')
                                    ->select('image',
                                             'fullName as name',
                                             'per_info.regId',
                                             'city.name as location',
                                             'territory.name as area'
                                    )
                                    ->where('per_info.regId', $order->regId)
                                    ->first();
            $service_details = DB::table('orders')
                    ->join('order_details', 'order_details.order_id', '=', 'orders.order_id')
                    ->join('services', 'services.s_id', '=', 'order_details.s_id')
                    ->select('order_details.order_id',
                             'services.name', 
                             'order_details.quantity',
                             'orders.assign_to as assign_to'
                    )
                    ->where('orders.order_id', $order_id)
                    ->get();
                                    
            return response()
                    ->json(array(
                        "status" => "true",
                        "message" => "Job Order Details",
                        "service_details" => $service_details,
                        "customer" => $customer,
                        "order" => $order
                ));

        }

    }
    
    

    // public function isAccept(Request $request, Assign_jobs $order)
    // {
    //     $headers = $request->header('Authentication');
    //     $response = $this->getSessionId($headers);
    //     if ($response != '1')
    //     {
    //         return $response;
    //     }
    //     else
    //     {
    //         $checker = DB::table('orders')
    //                 ->where('order_id', $order->order_id)
    //                 ->whereNotNull('assign_to')
    //                 ->count();
    //         if($checker == 1){
    //             return response()->json(
    //                 $result = DB::table('orders')
    //                             ->where('order_id', $order->order_id)
    //                             ->update(['status' => 'Assigned', 'assign_to' => $order->regId]));
    //             $order->update($request->all());
    //             $regID = DB::table('orders')->select('orders.regId')
    //                 ->where('orders.order_id', '=', $order->order_id)
    //                 ->first();
    //             $stoken = DB::table('users')->select('users.firebase_token')
    //                 ->where('users.regId', '=', $regID->regId)
    //                 ->get();
    
    //             $title = "Job Accepted";
    //             $body = "Kama has been assigned";
    //             $token = $stoken[0]->firebase_token;
    
    //             if ($order->is_accept == 1)
    //             {
    
    //                 $this->send_notification($order, $order->order_id, $token, $title, $body);
    
    //                 return response()->json($result = DB::table('orders')->where('order_id', $order->order_id)
    //                     ->update(['status' => 'Assigned', 'assign_to' => $order->regId]));
    
    //             }
    
    //             else
    //             {
    //                 return response()
    //                     ->json($result = DB::table('orders')->where('order_id', $order->order_id)
    //                     ->update(['status' => 'Cancelled']));
    //             }
    //         }else{
    //             return response()->json(array(
    //                 "status" => "true",
    //                 "message" => "Order has Assigned to Someone Else"
    //             ));
    //         }

    //     }

    // }
    
    public function isAccept(Request $request, Assign_jobs $order){
    
    $headers = $request->header('Authentication');
        $response = $this->getSessionId($headers);
        if ($response != '1')
        {
            return $response;
        }
        else
        {
            $order->update($request->all());
                      
           // return response()->json($order->is_accept, 200);
           
                $regID = DB::table('orders')
                            ->select('orders.regId', 'orders.assign_to')
                            ->where('orders.order_id', '=', $order->order_id)
                            ->first();
                if($regID->assign_to == null){
                    $stoken = DB::table('users')
                            ->select('users.firebase_token')
                            ->where('users.regId', '=', $regID->regId)
                            ->get();
                            
                    $title = "title Job Accepted";
                    $body = "Job Accepted";
                    $token = $stoken[0]->firebase_token;
           
                    if ($order->is_accept  == 1){
                        $orderobg = db::table('orders')->where('order_id', $order->order_id)->first();
                        $result = DB::table('orders')
                              ->where('order_id', $order->order_id)
                              ->update(['status' => 'Assigned', 'assign_to' => $order->regId]);
                        $this->send_notification($orderobg, $order->order_id, $token, $title,$body);
                        return response()
                            ->json(array(
                            'status' => "true",
                            'message' => "You accepted of order No. " . $order->order_id,
                        ));
                              
                              
                    }else{
                        $orderNew = DB::table('orders')->where('order_id', $order->order_id)->first();
                        $datetime = Carbon::parse($orderNew->request_time)
                                        ->subMinutes(2)
                                        ->format('Y-m-d H:i:s');
                        DB::table('orders')
                            ->where('order_id', $orderNew->order_id)
                            ->update(['request_time' => $datetime]);

                        DB::table('request_orders')
                            ->where('order_id', $orderNew->order_id)
                            ->where('reg_id', $order->regId)
                            ->update(['flag' => 0]);


                        return response()
                            ->json(array(
                            'status' => "true",
                            'message' => "You cancelled of order No. " . $orderNew->order_id,
                        ));
                    }
                }else{
                    return response()
                        ->json(array(
                        'status' => "error",
                        'message' => "Job Already Assigned",
                    ));
                }
                
        }
        // return $order->is_accept;
    }


    public function viewMyBooking(Request $request)
    {
        $headers = $request->header('Authentication');
        $response = $this->getSessionId($headers);
        if ($response != '1')
        {
            return $response;
        }
        else
        {
            $user = DB::table('per_info')->where('session_id', $request->header('Authentication'))
                ->first();
            $assigned = DB::select(DB::raw("select order_id, create_at from orders where assign_to = :reg_id and status = 'pending'") , array(
                'reg_id' => $user->regId
            ));
            $history = DB::select(DB::raw("select orders.order_id, DATE_FORMAT(orders.create_at, '%d-%b-%y %I:%i%p') as start_job_date , orders.total_amount as amount_received, services.name from orders 
      join services on services.s_id = orders.s_id
      where orders.status = 'Completed' and orders.status IS NOT NULL and assign_to = :reg_id ORDER BY orders.order_id DESC") , array(
                'reg_id' => $user->regId
            ));

            $current = DB::select(DB::raw("select os.order_id, GROUP_CONCAT(s.name) as service_name, DATE_FORMAT(os.create_at, '%d-%b-%y %I:%i%p') as create_at, os.status, os.latitude, os.longitude, os.total_amount from orders os, order_details od, services s where os.status NOT IN ('pending') AND DATE_FORMAT(`create_at`, '%Y-%m-%d') = CURDATE() AND os.assign_to = :reg_id and od.order_id = os.order_id and s.s_id = od.s_id group by os.order_id ORDER BY os.order_id DESC") , array(
                'reg_id' => $user->regId
            ));
            
            $schedule = DB::table('orders')
                        ->join('services', 'services.s_id', '=', 'orders.s_id')
                        ->select('order_id',  DB::raw('DATE_FORMAT(schedule_date, "%d-%b-%y %h:%i%p") as schedule_date'), DB::raw('DATE_FORMAT(schedule_date, "%h:%i %p") as schedule_time'), 'services.name as service_name')
                        
                        ->where('is_schedule', 1)
                        ->where('assign_to', $user->regId)
                        ->get();

            return response()
                ->json(array(
                'schedule' => $schedule,
                'history' => $history,
                'current' => $current
            ));
        }
    }

    public function timecurrent(Request $request)
    {
        $headers = $request->header('Authentication');
        $response = $this->getSessionId($headers);
        if ($response != '1')
        {
            return $response;
        }
        else
        {
            return response()->json($results = DB::select(DB::raw("SELECT DATE_SUB(NOW(), INTERVAL 3 HOUR) as time") , array(
                'order_id' => 1,
            )));
        }
    }

    public function orderStart($order_id)
    {
        $headers = $request->header('Authentication');
        $response = $this->getSessionId($headers);
        if ($response != '1')
        {
            return $response;
        }
        else
        {
            return response()->json($result = DB::table('orders')->where('order_id', $order_id)->update(['status' => 'In progress']));
        }

    }

    public function userOrder($order_id, Request $request)
    {
        $headers = $request->header('Authentication');
        $response = $this->getSessionId($headers);
        if ($response != '1')
        {
            return $response;
        }
        else
        {
            return response()->json($users = DB::table('per_info')->join('city', 'per_info.city_id', '=', 'city.city_id')
                ->join('orders', 'orders.regId', '=', 'per_info.regId')
                ->select('per_info.regId as Customer_id', 'per_info.fullName', 'per_info.phn_no', 'per_info.image', 'city.name as city', 'orders.full_address as address')
                ->where('order_id', $order_id)->get());
        }
    }

    public function customerOrderDetail($order_id, Request $request)
    {
        $headers = $request->header('Authentication');
        $response = $this->getSessionId($headers);
        if ($response != '1')
        {
            return $response;
        }
        else
        {
            return response()->json($results = DB::select( DB::raw("select per_info.fullName as customer_name, os.order_id, os.total_amount, (select IFNULL(GROUP_CONCAT(s1.name),'Only Visit Charges') from order_details o1,services s1
        where o1.s_id=s1.s_id
        and o1.order_id=os.order_id) as services, (select IFNULL(COUNT(s1.name),0) from order_details o1,services s1
        where o1.s_id=s1.s_id
        and o1.order_id=os.order_id) as no_of_services, (select  count(distinct 1) from order_details o1,services s1
        where o1.s_id=s1.s_id
        and o1.order_id=os.order_id
        and o1.add_work=1) as additional_work , DATE_FORMAT(os.start_job_date, '%I:%i%p %d-%b-%y') as start_job_date, DATE_FORMAT(os.end_job_date, '%I:%i%p %d-%b-%y') as end_job_date, os.s_id as service_id , (select name from services where s_id = os.s_id) as service_name, DATE_FORMAT(os.create_at, '%d-%b-%y') as service_date, os.payment_method, os.payment_id, os.arrived_at,os.status from   services s, orders os, per_info 
        WHERE os.order_id = :order_id  and os.regID = per_info.regID
        group by os.order_id"), array('order_id' => $order_id,)
    ));
        }

    }

    public function updateOrderTime(Request $request, $order_id, $value)
      {
        $headers = $request->header('Authentication');
        $response = $this->getSessionId($headers);
        if ($response != '1')
        {
            return $response;
        }
        else
        {
          //For Notification
          // return response()->json($order->is_accept, 200);
          $regID = DB::table('orders')->select('orders.regId')
              ->where('orders.order_id', '=', $order_id)->first();
          $stoken = DB::table('users')->select('users.firebase_token')
              ->where('users.regId', '=', $regID->regId)
              ->get();

          $token = $stoken[0]->firebase_token;
          $title = 'Status';
          $order_obg = db::table('orders')->where('order_id', $order_id)->first();
          if ($value == 1){
              $mytime = Carbon::now();
              $reqdata = $request->all();
              $reqdata['start_job_date'] = $mytime;
              $reqdata['status'] = 'In progress';
              $notifyStatus = 'In progress';
              $lesson = Orders::where('order_id', $order_id)->update($reqdata);
              $update_order_obg = Orders::where('order_id', $order_id)->first();
              if ($lesson){
                  
                  $body = $notifyStatus;
                  $this->send_notification($update_order_obg, $order_id, $token, $title, $body);
                  return Carbon::parse($mytime->toDateTimeString())->format('h:iA d-M-y');
              }else{
                  return 'false';
              }
          }else if ($value == 2){
              $mytime = Carbon::now();
              $reqdata = $request->all();
              $reqdata['end_job_date'] = $mytime;
              $reqdata['status'] = 'Invoice Pending';
              $notifyStatus = 'Invoice Pending';
              $lesson = Orders::where('order_id', $order_id)->update($reqdata);
              $update_order_obg = Orders::where('order_id', $order_id)->first();
              if ($lesson){
                  Carbon::resetToStringFormat();
                  $body = $notifyStatus;
                  $this->send_notification($update_order_obg, $order_id, $token, $title, $body);
                  return Carbon::parse($mytime->toDateTimeString())->format('h:iA d-M-y');
              }else{
                  return 'false';
              }
          }
      }
    }

    public function orderCompleted(Request $request, Orders $order_id)
    {
        $headers = $request->header('Authentication');
        $response = $this->getSessionId($headers);
        if ($response != '1')
        {
            return $response;
        }
        else
        {
            
             $checkstatus = DB::table('orders')
              ->where('order_id', '=', $order_id->order_id)
              ->where('status', '=', 'Completed')
              ->first();
            $title = "status";
            
            if($request->has('rate_by_kama')) {
                $per_info = DB::table('per_info')
                                ->where('regId', $order_id->regId)
                                ->first();
                $rating_number = $per_info->rating_number + $request->input('rate_by_kama');
                $rating_count = $per_info->rating_count + 1;
                DB::table('per_info')
                    ->where('regId', $per_info->regId)
                    ->update([
                        'rating_number' => $rating_number,
                        'rating_count' => $rating_count
                    ]);
            }
            

          $order_id->update($request->all());

          $updateStatus = $request->input('status');
          $stoken = DB::table('users')->select('users.firebase_token')
              ->where('users.regId', '=', $order_id->regId)
              ->get();
            
          $token = $stoken[0]->firebase_token;
            $orderobg = db::table('orders')->where('order_id',$order_id->order_id)->first();
           
            //   dump($orderobg);die();
               
              
              if($checkstatus == null){
                $this->send_notification($orderobg, $order_id->order_id ,$token, $title, $updateStatus);  
              }
              
          
          
          
          return response()->json($order_id, 200);
      }
    }

    public function myKamaProfile(Request $request)
    {
        $headers = $request->header('Authentication');
        $response = $this->getSessionId($headers);
        if ($response != '1')
        {
            return $response;
        }
        else
        {
            

                $id = DB::table('per_info')->where('session_id', $request->header('Authentication'))->first();
                $users = DB::table('per_info')
                    ->join('user_services', 'user_services.reg_id', '=', 'per_info.regId')
                    ->join('services', 'user_services.s_id', '=', 'services.s_id')
                    ->join('city', 'city.city_id', '=', 'per_info.city_id')
                    ->select('per_info.fullName', 'per_info.image', 'per_info.vehicle_no','per_info.email','per_info.cnic', DB::raw('group_concat(services.name) as expertise'), 'city.name as kamay_city', 'per_info.phn_no', 'per_info.regId', 'per_info.pri_lat', 'per_info.pri_long','per_info.rating_number','per_info.rating_count')
                    ->where('per_info.regId', '=', $id->regId)->get();

                if ($users->count() > 0)
                {

                     if($users[0]->rating_number != 0){
                        $users[0]->ratings = $users[0]->rating_number / $users[0]->rating_count;
                    }else{
                        $users[0]->ratings = 0;
                    }

                    return response()
                        ->json(array(
                        "status" => "true",
                        "message" => "Kama Found",
                        'kama_profile' => $users
                    ));
                }

                else
                {
                    return response()->json(array(
                        "status" => "true",
                        "message" => "Kama not Found",
                        'kama_profile' => $users
                    ));
                }
            

             return response()
                    ->json(array(
                    "status" => "true",
                    "message" => "Near By Kamay " . $distance,
                    "data" => $results,
                ));
            
        }
    }

    public function checkOrderStatus(Request $request)
    {
      $headers = $request->header('Authentication');
      $response = $this->getSessionId($headers);
      if ($response != '1')
      {
          return $response;
      }
      else
      {
        $user = DB::table('per_info')->where('session_id', $request->header('Authentication'))->first();
        
        
        $orderStatus = DB::select(DB::raw("select `assign_jobs`.`order_id`, `orders`.`status` from `assign_jobs` join `orders` on `assign_jobs`.`order_id` = `orders`.`order_id` where `assign_jobs`.`regId` = $user->regId and( `status` = 'Enroute' or `status` = 'Arrived' or `status` = 'Invoice Pending'  or `status` = 'Invoice Accepted' or `status` = 'In Progress' or `status` = 'Assigned' or `status` = 'Estimate Pending' or `status` = 'Estimate Accepted')"));
        if($user->rating_number != 0){
            $kamaRating = $user->rating_number / $user->rating_count;
        }else{
            $kamaRating = 0;
        }
        
        
           // dump($kamaRating);die();
        
        
         return response()
                        ->json(array(
                        "status" => "true",
                        "message" => "$user->job_status",
                        'order_status' => $orderStatus,
                        'kamaRating' => $kamaRating
                    ));
      
          
      }

    }
    
   

    public function addWork($order_id, Request $request)
    {
        $headers = $request->header('Authentication');
        $response = $this->getSessionId($headers);
        if ($response != '1')
        {
            return $response;
        }
        else
        {
            return response()->json(DB::select(DB::raw("SELECT od.order_details_id, od.s_id, od.order_id, od.amount, od.quantity, s.s_id as service_id, s.name as service_name FROM services s, order_details od where od.order_id = $order_id and s.s_id in (od.s_id)")));
        }
    }

    public function addWorkremove($order_details_id, Request $request)
    {
        $headers = $request->header('Authentication');
        $response = $this->getSessionId($headers);
        if ($response != '1'){
            return $response;
        }else{
            $orderData = DB::table('order_details')
                            ->select('order_details_id', 'order_id', 's_id', 'amount', 'quantity')
                            ->where('order_details_id', '=', $order_details_id)
                            ->first();
        
            if($order_details_id == $orderData->order_details_id){
                
                //insert data cancelled orders table
                $addCancellData = new CancelledOrders([
                    'order_details_id' => $orderData->order_details_id,
                    'order_id'     => $orderData->order_id,
                    's_id'            => $orderData->s_id,
                    'amount'           => $orderData->amount,
                    'quantity'    => $orderData->quantity,
                ]);
                
                $addCancellData->save();
                //delete data
                DB::table('order_details')->where('order_details_id', '=', $order_details_id)->delete();

                //Deleting Order Share
                DB::table('order_share')->where('order_details_id', '=', $order_details_id)->delete();


                $order_details_count = DB::table('order_details')->where('order_id', $orderData->order_id)->count();

                if($order_details_count == 0){
                    DB::table('orders')
                        ->where('order_id', $orderData->order_id)
                        ->update(
                            ['total_amount' => 300]
                        );

                    // Kama Share ID 6 
                    // Company Share 7
                    $order_share_total_amount_delete = DB::table('order_share_total_amount')
                                                            ->where('order_id', '=', $orderData->order_id)
                                                            ->delete();

                    $order_share_total_amount = DB::table('no_service_charges')->get();

                    if(count($order_share_total_amount) != 0){
                        foreach($order_share_total_amount as $order_share){
                            DB::table('order_share_total_amount')->insert([
                                'order_id' => $orderData->order_id,
                                'share_id' => $order_share->share_id,
                                'total_amount' => $order_share->price
                            ]);
                        }
                    }

                }else{
                    $order_total_price = DB::table('order_details')
                                            ->select(DB::raw('SUM(amount) as total_amount'))
                                            ->where('order_id', $orderData->order_id)
                                            ->groupBy('order_id')
                                            ->first();
                    DB::table('orders')
                        ->where('order_id', $orderData->order_id)
                        ->update(
                            ['total_amount' => $order_total_price->total_amount]
                        );

                    DB::table('order_share_total_amount')->where('order_id', '=', $orderData->order_id)->delete();

                    $order_share_data = DB::table('order_share')
                                            ->selectRaw('*, sum(price) as sum')
                                            ->groupBy('share_id')
                                            ->where('order_id', $request->input('order_id'))
                                            ->get();
                                            
                    if(count($order_share_data) != 0){
                        for($i = 0; $i < count($order_share_data); $i++){
                            DB::table('order_share_total_amount')->insert([
                                'order_id' => $orderData->order_id,
                                'share_id' => $order_share_data[$i]->share_id,
                                'total_amount' => $order_share_data[$i]->sum
                            ]);
                        }
                    }

                }
                return response()->json(['data'=>'Successfull']);
            }else{
                return response()->json(['data'=>'Order details record not found']);
            }
        }

    }

    public function addOrders(Request $request)
    {
        $headers = $request->header('Authentication');
        $response = $this->getSessionId($headers);
        if ($response != '1')
        {
            return $response;
        }
        else
        {
            $validator = Validator::make($request->all() , 
                [
                    'order_id' => 'required',
                    's_id' => 'required',
                    'amount' => 'required',
                    'quantity' => 'required',
                ]
            );
            
            if ($validator->fails())
            {
                return response()
                    ->json(['errors' => $validator->messages() , 'code' => 422, "status" => "false", "message" => "Required Fields."]);
            }

            $order_details_id = DB::table('order_details')->insertGetId([
                                    'order_id'     => $request->input('order_id'),
                                    's_id'            => $request->input('s_id'),
                                    'amount'           => $request->input('amount'),
                                    'quantity'    => $request->input('quantity'),
                                    'add_work'    => 1,
                                ]);

            $services_share = DB::table('services_share')->where('service_id', $request->input('s_id'))->get();
            if($services_share != null){
                foreach($services_share as $services_shares){
                    DB::table('order_share')->insert([
                        'order_id' => $request->input('order_id'),
                        'share_id' => $services_shares->share_id,
                        'order_details_id' => $order_details_id,
                        'price' => $services_shares->share_actual_price * $request->input('quantity'),
                        'quantity' => $request->input('quantity'),
                    ]);
                }
            }

            $order_total_price = DB::table('order_details')
                                    ->select(DB::raw('SUM(amount) as total_amount'))
                                    ->where('order_id', $request->input('order_id'))
                                    ->groupBy('order_id')
                                    ->first();

            DB::table('orders')->where('order_id', $request->input('order_id'))->update(['total_amount' => $order_total_price->total_amount]);

            $order_share_data = DB::table('order_share')
                                    ->selectRaw('*, sum(price) as sum')
                                    ->groupBy('share_id')
                                    ->where('order_id', $request->input('order_id'))
                                    ->get();

            DB::table('order_share_total_amount')->where('order_id', '=', $request->input('order_id'))->delete();

            if(count($order_share_data) != 0){
                for($i = 0; $i < count($order_share_data); $i++){
                    DB::table('order_share_total_amount')->insert([
                        'order_id' => $request->input('order_id'),
                        'share_id' => $order_share_data[$i]->share_id,
                        'total_amount' => $order_share_data[$i]->sum
                    ]);
                }
            }
          
    
            return response()->json(['success'=>true, 'message' => 'order successfull add'], 200 ); 
        }

    }

    public function payment(Request $request)
    {
        $headers = $request->header('Authentication');
        $response = $this->getSessionId($headers);
        if ($response != '1')
        {
            return $response;
        }
        else
        {
            $user = DB::table('per_info')->where('session_id', $request->header('Authentication'))
                ->first();
            $results = DB::select(DB::raw("SELECT * FROM (SELECT `orders`.`order_id`,MAX(services.s_id) AS service_id,services.name, services.service_image,DATE_FORMAT(orders.create_at, '%D %b %I %i %p') as create_at, orders.total_amount,DATE_FORMAT(orders.end_job_date, '%d-%b-%y %I:%i%p') as end_job_date 
                FROM `orders` 
                INNER JOIN `order_details` ON `orders`.`order_id` = `order_details`.`order_id` 
                INNER JOIN `services` ON order_details.s_id = services.s_id
                INNER JOIN `assign_jobs` ON order_details.order_id = assign_jobs.order_id
                WHERE `assign_jobs`.`regId` = :regId AND `assign_jobs`.`is_accept` = 1 AND orders.STATUS = 'Completed'
                GROUP BY order_id order by order_id DESC ) T WHERE T.ORDER_ID IS NOT NULL") , array(
                'regId' => $user->regId
            ));

            return $results;
        }
    }

    public function jobStatus(Request $request)
    {
        $headers = $request->header('Authentication');
        $response = $this->getSessionId($headers);
        $user = DB::table('per_info')->where('session_id', $request->header('Authentication'))
                ->first();
        $regId = Register::where('regId', $user->regId)->first();
        if ($response != '1')
        {
            return $response;
        }
        else
        {
            $regId->update($request->all());
        }
    }

    public function updateKamaLocation(Request $request)
    {
        
        $headers = $request->header('Authentication');
        $response = $this->getSessionId($headers);
        $user = DB::table('per_info')->where('session_id', $request->header('Authentication'))
                ->first();
        if($user != null){
            $regId = Register::where('regId', $user->regId)->first();
            if($regId != null){
                if ($response != '1'){
                    return $response;
                }else{
                    $regId->update($request->all());
                }
            }
        }
    }

    public function updateFirebaseToken(Request $request)
    {
        $headers = $request->header('Authentication');
        $response = $this->getSessionId($headers);
        if ($response != '1')
        {
            return $response;
        }
        else
        {
            $user = DB::table('per_info')->where('session_id', $request->header('Authentication'))
                ->first();
            // CURL DATA SEND START
            $post_data = [
                'session_id' => $request->header('Authentication'),
                'firebase_token' => $request->input('firebase_token')
            ];
            
            $curl = curl_init();
                                
            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://shop.kamay.biz/api/user/update_firebase_token",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => json_encode($post_data),
                CURLOPT_HTTPHEADER => array(
                    // Set here requred headers
                    "accept: */*",
                    "accept-language: en-US,en;q=0.8",
                    "content-type: application/json",
                ),
            ));
            
            $response = curl_exec($curl);
            $err = curl_error($curl);
            curl_close($curl);
            
            // CURL DATA SEND END
            return response()
                ->json($result = DB::table('users')->where('regId', $user->regId)
                ->update(['firebase_token' => $request->input('firebase_token') ]));
        }
    }

    public function send_notification($order, $order_id, $token, $title, $body)
    {
        $url = "https://fcm.googleapis.com/fcm/send";

        $serverKey = 'AAAAwHKXicQ:APA91bEsDHSUpggTIpe0d3aM19NoG3WTFzH7bcmgQm-mVpghtkOSYVa_D70wLfgitImGi1pKAjjInVMpQfTCUmS8XfWxE-cJHQ_PvZ6tfCtD7YNVz3EE3tunnYEHo6z26c_1lF1ApKOP';
        $title = $title;
        $body = $body;
        $order->title = $title;
        $order->body = $body;
        $service = DB::table('services')
                        ->where('s_id', $order->s_id)
                        ->first();
        
        
        $order->service = $service->name;
        $order->flag = 'service';
        
        if($body == 'Job Accepted'){
                
            $getNotification = DB::table('notifications')->where('id', 1)->first();
            $search = '$order_id';
            $getbody = str_replace($search, $order_id, $getNotification->body) ;
            
            $order->title = $getNotification->title;
            $order->body =  $getbody;
            $title = $getNotification->title;
            $body = $getbody;
        
            
        }elseif($body == 'Arrived'){
            
            $getNotification = DB::table('notifications')->where('id', 2)->first();
            $order->title = $getNotification->title;
            $order->body =  $getNotification->body;
            $title = $getNotification->title;
            $body = $getNotification->body;
            
            
        }elseif($body == 'Enroute'){
            
            $getNotification = DB::table('notifications')->where('id', 3)->first();
            $getbody = $getNotification->body;

            $search = '$order_id';
            $search2 = '$service->name';
            $getbody = str_replace($search, $order_id, $getbody) ;
            $getbody = str_replace($search2, $service->name, $getbody);
            
            $order->title = $getNotification->title;
            $order->body =  $getbody;
            $title = $getNotification->title;
            $body = $getbody;
    
        }elseif($body == 'Estimate Pending'){
            
            $getNotification = DB::table('notifications')->where('id' , 4)->first();
            $order->title = $getNotification->title;
            $order->body =  $getNotification->body; 
            $title = $getNotification->title;
            $body = $getNotification->body;
            
        }elseif($body == 'In progress'){
            
            $getNotification = DB::table('notifications')->where('id', 5)->first();
            $getbody = $getNotification->body;

            $search = '$order_id';
            $search2 = '$service->name';
            $getbody = str_replace($search, $order_id, $getbody) ;
            $getbody = str_replace($search2, $service->name, $getbody) ;
            
            $order->title = $getNotification->title;
            $order->body =  $getbody;
            $title = $getNotification->title;
            $body = $getbody;
            
        }elseif($body == 'Invoice Pending'){
            
            $getNotification = DB::table('notifications')->where('id', 6)->first();
            $getbody = $getNotification->body;

            $search = '$order_id';
            $search2 = '$service->name';
            $getbody = str_replace($search, $order_id, $getbody) ;
            $getbody = str_replace($search2, $service->name, $getbody) ;
            
            $order->title = $getNotification->title;
            $order->body =  $getbody;
            $title = $getNotification->title;
            $body = $getbody;
            
        }elseif($body == 'Completed'){
            
            $getNotification = DB::table('notifications')->where('id', 7)->first();
            $getbody = $getNotification->body;

            $search = '$order_id';
            $search2 = '$service->name';
            $search3 = '$order->total_amount';
            $getbody = str_replace($search, $order_id, $getbody);
            $getbody = str_replace($search2, $service->name, $getbody);
            $getbody = str_replace($search3, $order->total_amount, $getbody);
            
            $order->title = $getNotification->title;
            $order->body =  $getbody;
            $title = $getNotification->title;
            $body = $getbody;
            
        }
            
        $notification = array(
            'title' => $title,
            'body' => $body,
            'sound' => 'default',
            'badge' => '1',
            'order' => $order,
            'order_id' => $order_id,
            'flag' => 'service'
        );
        $arrayToSend = array(
            'order' => $order,
            'order_id' => $order_id,
            'to' => $token,
            'notification' => $notification,
            'data' => $order,
            'priority' => 'high'
        );
        $json = json_encode($arrayToSend);
        $headers = array();
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Authorization: key=' . $serverKey;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        //Send the request
        $response = curl_exec($ch);
        //Close request
        if ($response === false)
        {
            die('FCM Send Error: ' . curl_error($ch));
        }
        curl_close($ch);

    }

    public function userProfileUpdate(Request $request){
        $headers = $request->header('Authentication');
        $response = $this->getSessionId($headers);
        if ($response != '1')
        {
            return $response;
        }
        else
        {
            $user = DB::table('per_info')->where('session_id', $request->header('Authentication'))
                ->first();
            $wordlist = Register::where([
                ['session_id', '=', $request->header('Authentication')]]) ->get();
            if($wordlist->count()>0) {
                $results = DB::select( DB::raw("SELECT COUNT(order_id) as number_of_order FROM `orders` WHERE `regId` = $user->regId AND `status` = 'Completed'"));
                
                if($results > 0){
                    return response()->json(array("status"=>"true", "message"=>"order_status found",'order_status'=>$results));
                }
            }else{
                return response()->json(array("status"=>"false", "message"=>"Sesion Expired",'status'=>array()));
            }
        }
    }

    public function panic(Request $request){
        $headers = $request->header('Authentication');
        $response = $this->getSessionId($headers);
        if ($response != '1')
        {
            return $response;
        }
        else
        {
            $user = DB::table('per_info')->where('session_id', $request->header('Authentication'))
                ->first();
            $order_id = $request->input('order_id');
            $reg_id = $user->regId;
            $role_id = $user->r_id;
            $reason = $request->input('reason');
            
            $checkData = DB::table('panic_reported')
                        ->where([
                                    ['order_id', $order_id],
                                    ['reg_id', $reg_id]
                                ])
                        ->first();

            if($checkData != null){
                
                 return response()
                    ->json(array(
                    "status" => "true",
                    "message" => "You have already action that panic",
                    "data" => false
                ));
                
            }
        
            $results = DB::table('panic_reported')->insertGetId(
                [
                    'order_id' => $order_id,
                    'reg_id' => $reg_id,
                    'role_id' => $role_id,
                    'reason' => $reason,
                    'status' => 2,
                    'supervisor_id' => $user->assigned_supervisor
                ]
            );

            DB::table('admin_notify')->insert([
                'notify_status' => 3,
                'panic_id'=> $results

            ]);
            
            $panic_details = DB::table('per_info')
                                    ->join('orders', 'per_info.regId', '=', 'orders.regId')
                                    ->join('per_info as worker_info', 'orders.assign_to', '=', 'worker_info.regId')
                                    ->select(
                                        'per_info.fullName AS reported_full_name',
                                        'per_info.r_id AS reported_role_id',
                                        'per_info.image as image',
                                        'per_info.phn_no as phone_number',
                                        'orders.order_id AS order_id',
                                        'orders.assign_to as assign_to',
                                        'worker_info.fullName AS worker_full_name',
                                        'worker_info.assigned_supervisor AS supervisor_id'
                                    )
                                    //->where('per_info.assigned_supervisor', $user->regId)
                                    ->where('orders.order_id', $order_id)
                                    ->first();
                                    
            //dump($panic_details);die();
            $getKamaFirbaseToken = DB::table('users')->select('users.firebase_token')
                        ->where('users.regId', '=', $panic_details->supervisor_id)
                        ->first();
            
            $getNotification = DB::table('notifications')->where('id', 9)->first();
            $getbody = $getNotification->body;

            $search2 = '$panic_details->order_id';
            $getbody = str_replace($search2, $panic_details->order_id, $getbody) ;

            $text = $getNotification->title;
            $body = $getbody;
            $flag = "panic";
            $this->panic_send_notification($getKamaFirbaseToken->firebase_token,  $panic_details->order_id, null, null, null, null, null, $text, $body, $flag);
            
            $body = 'kama_panic';
            $this->kamayUserController->backOfficeNotification($body, $results);
            return response()->json(array("status"=>"true", "message"=>"Panic generated",'data'=> true));
        }
    }
    
    
    function distanceCalculation($lat1, $lon1, $lat2, $lon2, $unit) {
        if (($lat1 == $lat2) && ($lon1 == $lon2)) {
            return 0;
        }else {
            $theta = $lon1 - $lon2;
            $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
            $dist = acos($dist);
            $dist = rad2deg($dist);
            $miles = $dist * 60 * 1.1515;
            $unit = strtoupper($unit);
            if ($unit == "K") {
                return bcdiv(($miles * 1.609344), 1, 2) . ' KM';
            } else if ($unit == "N") {
                return bcdiv(($miles * 0.8684), 1, 2) . ' NMI';
            } else {
                return bcdiv($miles, 1, 2) . ' M';
            }
        }
    }


    

    
    
    public function panic_send_notification($token, $order_id, $longitude, $latitude, $total_amount, $created_at, $distance, $text, $body, $flag)
    {
        $url = "https://fcm.googleapis.com/fcm/send";
        $serverKey = 'AAAAwHKXicQ:APA91bEsDHSUpggTIpe0d3aM19NoG3WTFzH7bcmgQm-mVpghtkOSYVa_D70wLfgitImGi1pKAjjInVMpQfTCUmS8XfWxE-cJHQ_PvZ6tfCtD7YNVz3EE3tunnYEHo6z26c_1lF1ApKOP';
        $title = $text;
        $body = $body;
        $order = DB::table('orders')->where('order_id',$order_id)->first();
        $service = DB::table('services')->where('s_id', $order->s_id)->first();
        $data = array(
            // "order" => $order,
            "order_id" => $order_id,
            "service_name" => $service->name,
            "longitude" => $longitude,
            "latitude" => $latitude,
            "total_amount" => $total_amount,
            "created_at" => $created_at,
            "service_image" => $service->service_image,
            "distance" => $distance,
            "title" => $title,
            "body" => $body,
            "flag" => $flag
        );
        $notification = array(
            'title' => $title,
            'body' => $body,
            'sound' => 'default',
            'badge' => '1'
        );
        $arrayToSend = array(
            'to' => $token,
            //'notification' => $notification,
            'priority' => 'high',
            'data' => $data
        );
        $json = json_encode($arrayToSend);
        $headers = array();
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Authorization: key=' . $serverKey;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        //Send the request
        $response = curl_exec($ch);
        //Close request
        if ($response === false)
        {
            die('FCM Send Error: ' . curl_error($ch));
        }
        curl_close($ch);

    }

}