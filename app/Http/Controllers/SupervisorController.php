<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Http\Controllers\kamayUserController;

class SupervisorController extends Controller
{
    protected $kamayUserController;
    public function __construct(kamayUserController $kamayUserController)
    {
        $this->kamayUserController = $kamayUserController;
    }
    public function getSessionId($session_id){
        if ($session_id != '')
        {
            $user = DB::table('per_info')->where('session_id', $session_id)->first();
            if ($user === null)
            {
                return response()->json(array(
                    "status" => "false",
                    "message" => "Session Expired",
                    "user" => null
                ));
            }
            else
            {
                return '1';
            }
        }
        else
        {
            return response()
                ->json(array(
                "status" => "false",
                "message" => "Authentication Error",
                "user" => null
            ));
        }
    }

    public function loginSupervisor(Request $request){
        // r_id = 1 // Kamay login
        // r_id = 2 // Supervisor login
        $r_id = 2;

        $phn_no = $request->input('phn_no');
        $validator = Validator::make($request->all() , ['phn_no' => 'required', ]);
        if ($validator->fails())
        {
            return response()
                ->json(['errors' => $validator->messages() , 'code' => 422, "status" => "false", "message" => "Required Fields."]);
        }else{
            $results = DB::table('per_info')->where([['phn_no', $phn_no], ['r_id', $r_id]])->select('regId', 'fullName', 'phn_no')
                ->first();

            if ($results)
            {
                $someArray = json_encode($results, true);
                $someArray = json_decode($someArray, true);
                $regId = $someArray["regId"];
                $time = md5(date("h:i:sa"));
                $session = ($time . $regId);
                $value = DB::table('per_info')->where([['phn_no', '=', $phn_no], ['r_id', '=', $r_id]])->update(['session_id' => $session]);
                if ($value = 1)
                {
                    $user_details = DB::table('per_info')->where([['phn_no', $phn_no], ['r_id', $r_id], ['status', 1]])->select('regId', 'fullName', 'phn_no', 'email', 'r_id', 'session_id', 'pri_add', 'pri_lat', 'pri_long', 'image')
                        ->first();
                    return response()
                        ->json(array(
                        "status" => "true",
                        "message" => "Logged in.",
                        "user" => $user_details
                    ));
                }
            }
            else
            {
                return response()->json(array(
                    "status" => "false",
                    "message" => "Wrong credentials.",
                    "user" => $results
                ));
            }
        }
    }
    
    public function orderDetails($order_id, Request $request)
        {
        $headers = $request->header('Authentication');
        $response = $this->getSessionId($headers);
        if ($response != '1')
        {
            return $response;
        }
        else
        {
            // $results = DB::select(DB::raw("select od.order_id,concat(os.total_amount) as total_amount, s.name, p.regId as customer_id, p.fullName as customer_name, p.image as customer_image, pc.name as city_name, (select name from services where s_id = s.parent_id) as service_name, od.quantity,os.payment_method, os.full_address from  order_details od, services s, orders os, per_info p, city pc   WHERE od.order_id =  :order_id and s.s_id = od.s_id and p.regId = os.regId and p.city_id = pc.city_id and os.order_id = od.order_id and p.r_id= 3") , array(
            //             'order_id' => $order_id,
            //             ));
            $order = DB::table('orders')
                        ->join('services', 'services.s_id', '=', 'orders.s_id')
                        ->select('order_id', 'total_amount', DB::raw('DATE_FORMAT(create_at, "%d-%b-%y %I:%i%p") as service_date'), 'amount_received', 'payment_method', 'assign_to', 'regId', 'full_address','services.name as service_name')
                        
                        ->where('order_id', $order_id)
                        ->first();
            $customer = DB::table('per_info')
                                    ->leftjoin('city', 'city.city_id', '=', 'per_info.city_id')
                                    ->leftjoin('territory', 'territory.terr_id', '=', 'per_info.terr_id')
                                    ->select('image',
                                             'fullName as name',
                                             'per_info.regId',
                                             'city.name as location',
                                             'territory.name as area'
                                    )
                                    ->where('per_info.regId', $order->regId)
                                    ->first();
            $service_details = DB::table('orders')
                    ->join('order_details', 'order_details.order_id', '=', 'orders.order_id')
                    ->join('services', 'services.s_id', '=', 'order_details.s_id')
                    ->select('order_details.order_id',
                             'services.name', 
                             'order_details.quantity',
                             'orders.assign_to as assign_to'
                    )
                    ->where('orders.order_id', $order_id)
                    ->get();
                                    
            return response()
                    ->json(array(
                        "status" => "true",
                        "message" => "Job Order Details",
                        "service_details" => $service_details,
                        "customer" => $customer,
                        "order" => $order
                ));

        }

    }

     public function scheduleOrderDetails($order_id, Request $request)
        {
        $headers = $request->header('Authentication');
        $response = $this->getSessionId($headers);
        if ($response != '1')
        {
            return $response;
        }
        else
        {
            // $results = DB::select(DB::raw("select od.order_id,concat(os.total_amount) as total_amount, s.name, p.regId as customer_id, p.fullName as customer_name, p.image as customer_image, pc.name as city_name, (select name from services where s_id = s.parent_id) as service_name, od.quantity,os.payment_method, os.full_address from  order_details od, services s, orders os, per_info p, city pc   WHERE od.order_id =  :order_id and s.s_id = od.s_id and p.regId = os.regId and p.city_id = pc.city_id and os.order_id = od.order_id and p.r_id= 3") , array(
            //             'order_id' => $order_id,
            //             ));
            $order = DB::table('schedule_orders')
                        ->join('services', 'services.s_id', '=', 'schedule_orders.s_id')
                        ->select('id as order_id', 'total_amount', DB::raw('DATE_FORMAT(create_at, "%d-%b-%y %I:%i%p") as service_date'), 'amount_received', 'payment_method', 'assign_to', 'regId', 'full_address','services.name as service_name')
                        
                        ->where('id', $order_id)
                        ->first();
            $customer = DB::table('per_info')
                                    ->leftjoin('city', 'city.city_id', '=', 'per_info.city_id')
                                    ->leftjoin('territory', 'territory.terr_id', '=', 'per_info.terr_id')
                                    ->select('image',
                                             'fullName as name',
                                             'per_info.regId',
                                             'city.name as location',
                                             'territory.name as area'
                                    )
                                    ->where('per_info.regId', $order->regId)
                                    ->first();
            $service_details = DB::table('schedule_orders')
                    ->join('schedule_order_details', 'schedule_order_details.order_id', '=', 'schedule_orders.id')
                    ->join('services', 'services.s_id', '=', 'schedule_order_details.s_id')
                    ->select('schedule_order_details.order_id',
                             'services.name', 
                             'schedule_order_details.quantity',
                             'schedule_orders.assign_to as assign_to'
                    )
                    ->where('schedule_orders.id', $order_id)
                    ->get();
                                    
            return response()
                    ->json(array(
                        "status" => "true",
                        "message" => "Schedule Job Order Details",
                        "service_details" => $service_details,
                        "customer" => $customer,
                        "order" => $order
                ));

        }

    }
    
    
    public function supervisorProfile(Request $request)
    {
        $headers = $request->header('Authentication');
        $response = $this->getSessionId($headers);
        if ($response != '1')
        {
            return $response;
        }
        else
        {
            $user = DB::table('per_info')->where('session_id', $request->header('Authentication'))
                ->first();
             
                $results = DB::table('per_info')
                ->join('city', 'per_info.city_id', '=', 'city.city_id')
                ->leftjoin('territory', 'territory.terr_id', '=', 'per_info.terr_id')
                ->select('per_info.regId', 'per_info.fullName', 'per_info.nickName', 'per_info.phn_no', 'territory.name as area','per_info.pri_add','per_info.cnic', 'per_info.email', 'per_info.age', 'per_info.image', 'city.name as city')
                ->where('session_id', $request->header('Authentication'))
                ->first();
                $kamayCount = DB::table('per_info')
                ->select('per_info *')
                ->where('assigned_supervisor', $results->regId)
                ->count();
                
                $rating = DB::table('orders')
                                ->join('services', 'services.s_id', '=', 'orders.s_id')
                                ->join('per_info', 'per_info.regId', '=', 'orders.assign_to')
                                ->select(
                                    DB::raw('sum(orders.rate_by_customer) as total_customer_rating'),
                                    DB::raw('count(orders.order_id) as total_customers')
                                   
                                )
                                ->where('per_info.assigned_supervisor',$user->regId)
                                ->get();
                    //dd($rating[0]->total_customer_rating);die();
                    if($rating[0]->total_customers != 0){
                        $rating[0]->ratings = $rating[0]->total_customer_rating / $rating[0]->total_customers;
                    }else{
                        $rating[0]->ratings = 0;
                    }
                   //dd($rating[0]->ratings);die();             
                //$ratings = $rating[0]->total_customer_rating / $rating[0]->total_customers;
                
                $bages =1;
                $expertise = 1;
                $earning = 0;
                $reviews = 0;
                $data = [];
                $data['results'] = $results;
                $data['kamayCount'] = $kamayCount;
                $data['ratings'] = $rating[0]->ratings;
                $data['bages'] = $bages;
                $data['expertise'] = $expertise;
                $data['earning'] = $earning;
                $data['reviews'] = $reviews;
                return response()
                    ->json(array(
                        "status" => "true",
                        "message" => "Supervisor Profile",
                        "data" => $data,
                ));
        }
    }
    
    public function supervisorDashboard(Request $request, $duration){
        $headers = $request->header('Authentication');
        $response = $this->getSessionId($headers);
        if ($response != '1')
        {
            return $response;
        }
        else
        {
            $user = DB::table('per_info')->where('session_id', $request->header('Authentication'))
                ->first();
            // Panic Count - New Jobs Count, Complains Count, New Kamay, Unapproved Inovices - Upcoming Interviews
            if($duration == 'a'){
                
                $output_value = 'All';
                $panic_count =  $this->panic_count(Carbon::today(), '<=', 'panic_reported.created_at',  '!=', 0, $user->regId);
                $new_kamay = DB::table('per_info')
                            ->where('per_info.status', 0)
                            ->where('per_info.assigned_supervisor', $user->regId)
                            ->whereBetween('per_info.assigned_supervisor_date', [Carbon::now()->subDays(10), Carbon::now()])
                            ->where('per_info.r_id', 1)
                            ->count();
                $new_jobs = DB::table('orders')
                            ->join('services', 'services.s_id', '=', 'orders.s_id')
                            ->where('orders.assign_to', null)
                            ->count();
                
                $in_comp_doc = DB::table('per_info')
                                ->where('per_info.status', 0)
                                ->where('per_info.assigned_supervisor', $user->regId)
                                ->where('per_info.r_id', 1)
                                ->where('per_info.assigned_supervisor_date','<=', Carbon::now()->subDays(10))

                                
                                ->select(
                                    'per_info.image AS image',
                                    'per_info.assigned_supervisor_date AS assigned_date',
                                    'per_info.regId AS regId',
                                    'per_info.fullName AS fullName'
                                  )->count();
                                
                $poor_review_count = DB::table('orders')
                                ->join('services', 'services.s_id', '=', 'orders.s_id')
                                ->join('per_info', 'per_info.regId', '=', 'orders.assign_to')
                                ->select(
                                    'orders.order_id AS order_id',
                                    'orders.regId AS worker_id',
                                    'orders.create_at AS job_date',
                                    'orders.rate_by_customer AS customer_rating',
                                    'per_info.fullname as worker_name',
                                    'per_info.image as worker_image',
                                    'per_info.email as worker_email',
                                    'per_info.phn_no as worker_phone',
                                    'services.s_id AS service_id',
                                    'services.name AS service_name',
                                    'services.service_image AS service_image',
                                    'orders.full_address AS full_address',
                                    'orders.payment_method AS payment_method',
                                    'orders.total_amount as total_amount'
                                )
                                ->where('per_info.assigned_supervisor',$user->regId)
                                ->whereBetween('orders.rate_by_customer', [1, 2])
                                ->orderBy('orders.order_id', 'desc')
                                ->count();
                       
                $circle_radius = 3959;
                $distance = 3;
                $near_by_count = DB::select(
                            'SELECT COUNT(*) as kamay_count FROM
                                (SELECT *, (' . $circle_radius . ' * acos(cos(radians(' . $user->pri_lat . ')) * cos(radians(pri_lat)) *
                                    cos(radians(pri_long) - radians(' . $user->pri_long . ')) +
                                    sin(radians(' . $user->pri_lat . ')) * sin(radians(pri_lat))))
                                    AS distance
                                    FROM per_info) AS distances
                                WHERE distance < ' . $distance . '
                                AND assigned_supervisor = ' . $user->regId . '
                                AND r_id = 1
                                AND status = 1
                                AND job_status = 1
                                ORDER BY distance;
                            ');
            
            $assigned_count = DB::table('order_info_v')
                ->join('per_info', 'order_info_v.worker_id', 'per_info.regId')
                ->where('order_info_v.status','InProgress')
                ->where('per_info.assigned_supervisor', $user->regId)
                    ->count();
            $map_active_count = DB::table('per_info')
                    ->where("assigned_supervisor", $user->regId)
                    ->where("status", 1)
                    ->where("job_status", 1)
                    ->where("r_id", 1)
                    ->count();
            }else if($duration == 't'){
                $output_value = 'Today';
                $panic_count =  $this->panic_count(Carbon::now(), '=', 'panic_reported.created_at',  '!=', 0, $user->regId);
                $new_kamay = DB::table('per_info')
                            ->where('per_info.status', 0)
                            ->where('per_info.assigned_supervisor', $user->regId)
                            ->whereBetween('per_info.assigned_supervisor_date', [Carbon::now()->subDays(10), Carbon::now()])
                            ->where('per_info.r_id', 1)
                            ->count();

                
                $new_jobs = DB::table('orders')
                            ->join('services', 'services.s_id', '=', 'orders.s_id')
                            ->where('orders.assign_to', null)
                            ->whereDate('orders.create_at', Carbon::today())
                            ->count();
                
                $in_comp_doc = DB::table('per_info')
                                ->where('per_info.status', 0)
                                ->where('per_info.assigned_supervisor', $user->regId)
                                ->where('per_info.r_id', 1)
                                ->where('per_info.assigned_supervisor_date','<=', Carbon::now()->subDays(10))
                                ->select(
                                    'per_info.image AS image',
                                    'per_info.assigned_supervisor_date AS assigned_date',
                                    'per_info.regId AS regId',
                                    'per_info.fullName AS fullName'
                                  )->count();
                                  
                $poor_review_count = DB::table('orders')
                                ->join('services', 'services.s_id', '=', 'orders.s_id')
                                ->join('per_info', 'per_info.regId', '=', 'orders.assign_to')
                                ->select(
                                    'orders.order_id AS order_id',
                                    'orders.regId AS worker_id',
                                    'orders.create_at AS job_date',
                                    'orders.rate_by_customer AS customer_rating',
                                    'per_info.fullname as worker_name',
                                    'per_info.image as worker_image',
                                    'per_info.email as worker_email',
                                    'per_info.phn_no as worker_phone',
                                    'services.s_id AS service_id',
                                    'services.name AS service_name',
                                    'services.service_image AS service_image',
                                    'orders.full_address AS full_address',
                                    'orders.payment_method AS payment_method',
                                    'orders.total_amount as total_amount'
                                )
                                ->where('per_info.assigned_supervisor',$user->regId)
                                ->whereBetween('orders.rate_by_customer', [1, 2])
                                ->whereDate('orders.create_at', Carbon::today())
                                ->orderBy('orders.order_id', 'desc')
                                ->count();
                $assigned_count = DB::table('order_info_v')
                ->join('per_info', 'order_info_v.worker_id', 'per_info.regId')
                ->where('order_info_v.status','Assigned')
                ->where('per_info.assigned_supervisor', $user->regId)
                ->whereDate('order_info_v.sdate', Carbon::today())
                    ->count();
                            
                $circle_radius = 3959;
                $distance = 3;
                $near_by_count = DB::select(
                            'SELECT COUNT(*) as kamay_count FROM
                                (SELECT *, (' . $circle_radius . ' * acos(cos(radians(' . $user->pri_lat . ')) * cos(radians(pri_lat)) *
                                    cos(radians(pri_long) - radians(' . $user->pri_long . ')) +
                                    sin(radians(' . $user->pri_lat . ')) * sin(radians(pri_lat))))
                                    AS distance
                                    FROM per_info) AS distances
                                WHERE distance < ' . $distance . '
                                AND assigned_supervisor = ' . $user->regId . '
                                AND r_id = 1
                                 AND status = 1
                                AND job_status = 1
                                ORDER BY distance;
                            ');
                            
            $map_active_count = DB::table('per_info')
                    ->where("assigned_supervisor", $user->regId)
                    ->where("status", 1)
                    ->where("job_status", 1)
                    ->where("r_id", 1)
                    ->count();
            }else if($duration == 'w'){
                $output_value = "Week";
                $panic_count =  $this->panic_count(Carbon::today()->subDays(7), '>=', 'panic_reported.created_at',  '!=', 0, $user->regId);
                $new_kamay = DB::table('per_info')
                            ->where('per_info.status', 0)
                            ->where('per_info.assigned_supervisor', $user->regId)
                            ->whereBetween('per_info.assigned_supervisor_date', [Carbon::now()->subDays(10), Carbon::now()])
                            ->where('per_info.r_id', 1)
                            ->count();

                $new_jobs = DB::table('orders')
                            ->join('services', 'services.s_id', '=', 'orders.s_id')
                            ->where('orders.assign_to', null)
                            ->whereDate('orders.create_at', '>=', Carbon::today()->subDays(7))
                            ->count();
                $in_comp_doc = DB::table('per_info')
                                ->where('per_info.status', 0)
                                ->where('per_info.assigned_supervisor', $user->regId)
                                ->where('per_info.r_id', 1)
                                ->where('per_info.assigned_supervisor_date','<=', Carbon::now()->subDays(10))
                                
                                ->select(
                                    'per_info.image AS image',
                                    'per_info.assigned_supervisor_date AS assigned_date',
                                    'per_info.regId AS regId',
                                    'per_info.fullName AS fullName'
                                  )->count();
                 $assigned_count = DB::table('order_info_v')
                ->join('per_info', 'order_info_v.worker_id', 'per_info.regId')
                ->where('order_info_v.status','Assigned')
                ->where('per_info.assigned_supervisor', $user->regId)
                ->whereDate('order_info_v.sdate', '>=', Carbon::today()->subDays(7))
                    ->count();
                $circle_radius = 3959;
                $distance = 3;
                $near_by_count = DB::select(
                            'SELECT COUNT(*) as kamay_count FROM
                                (SELECT *, (' . $circle_radius . ' * acos(cos(radians(' . $user->pri_lat . ')) * cos(radians(pri_lat)) *
                                    cos(radians(pri_long) - radians(' . $user->pri_long . ')) +
                                    sin(radians(' . $user->pri_lat . ')) * sin(radians(pri_lat))))
                                    AS distance
                                    FROM per_info) AS distances
                                WHERE distance < ' . $distance . '
                                AND assigned_supervisor = ' . $user->regId . '
                                AND r_id = 1
                                 AND status = 1
                                AND job_status = 1
                                ORDER BY distance;
                            ');
                $poor_review_count = DB::table('orders')
                                ->join('services', 'services.s_id', '=', 'orders.s_id')
                                ->join('per_info', 'per_info.regId', '=', 'orders.assign_to')
                                ->select(
                                    'orders.order_id AS order_id',
                                    'orders.regId AS worker_id',
                                    'orders.create_at AS job_date',
                                    'orders.rate_by_customer AS customer_rating',
                                    'per_info.fullname as worker_name',
                                    'per_info.image as worker_image',
                                    'per_info.email as worker_email',
                                    'per_info.phn_no as worker_phone',
                                    'services.s_id AS service_id',
                                    'services.name AS service_name',
                                    'services.service_image AS service_image',
                                    'orders.full_address AS full_address',
                                    'orders.payment_method AS payment_method',
                                    'orders.total_amount as total_amount'
                                )
                                ->where('per_info.assigned_supervisor',$user->regId)
                                ->whereBetween('orders.rate_by_customer', [1, 2])
                                ->whereDate('orders.create_at', '>=', Carbon::today()->subDays(7))
                                ->orderBy('orders.order_id', 'desc')
                                ->count();
            $map_active_count = DB::table('per_info')
                    ->where("assigned_supervisor", $user->regId)
                    ->where("status", 1)
                    ->where("job_status", 1)
                    ->where("r_id", 1)
                    ->count();
            }else if($duration == 'm'){
                $output_value = "Month";
                $panic_count =  $this->panic_count(Carbon::today()->subDays(30), '>=', 'panic_reported.created_at',  '!=', 0, $user->regId);
                $new_kamay = DB::table('per_info')
                            ->where('per_info.status', 0)
                            ->where('per_info.assigned_supervisor', $user->regId)
                            ->whereBetween('per_info.assigned_supervisor_date', [Carbon::now()->subDays(10), Carbon::now()])
                            ->where('per_info.r_id', 1)
                            ->count();

                $new_jobs = DB::table('orders')
                            ->join('services', 'services.s_id', '=', 'orders.s_id')
                            ->where('orders.assign_to', null)
                            ->whereDate('orders.create_at', '>=', Carbon::today()->subDays(30))
                            ->count();
                $in_comp_doc = DB::table('per_info')
                                ->where('per_info.status', 0)
                                ->where('per_info.assigned_supervisor', $user->regId)
                                ->where('per_info.r_id', 1)
                                ->where('per_info.assigned_supervisor_date','<=', Carbon::now()->subDays(10))
                                
                                ->select(
                                    'per_info.image AS image',
                                    'per_info.assigned_supervisor_date AS assigned_date',
                                    'per_info.regId AS regId',
                                    'per_info.fullName AS fullName'
                                  )->count();
                 $assigned_count = DB::table('order_info_v')
                ->join('per_info', 'order_info_v.worker_id', 'per_info.regId')
                ->where('order_info_v.status','Assigned')
                ->where('per_info.assigned_supervisor', $user->regId)
                ->whereDate('order_info_v.sdate', '>=', Carbon::today()->subDays(30))
                    ->count();
                $poor_review_count = DB::table('orders')
                                ->join('services', 'services.s_id', '=', 'orders.s_id')
                                ->join('per_info', 'per_info.regId', '=', 'orders.assign_to')
                                ->select(
                                    'orders.order_id AS order_id',
                                    'orders.regId AS worker_id',
                                    'orders.create_at AS job_date',
                                    'orders.rate_by_customer AS customer_rating',
                                    'per_info.fullname as worker_name',
                                    'per_info.image as worker_image',
                                    'per_info.email as worker_email',
                                    'per_info.phn_no as worker_phone',
                                    'services.s_id AS service_id',
                                    'services.name AS service_name',
                                    'services.service_image AS service_image',
                                    'orders.full_address AS full_address',
                                    'orders.payment_method AS payment_method',
                                    'orders.total_amount as total_amount'
                                )
                                ->where('per_info.assigned_supervisor',$user->regId)
                                ->whereBetween('orders.rate_by_customer', [1, 2])
                                ->whereDate('orders.create_at', '>=', Carbon::today()->subDays(30))
                                ->orderBy('orders.order_id', 'desc')
                                ->count();
                            
                $circle_radius = 3959;
                $distance = 3;
                $near_by_count = DB::select(
                            'SELECT COUNT(*) as kamay_count FROM
                                (SELECT *, (' . $circle_radius . ' * acos(cos(radians(' . $user->pri_lat . ')) * cos(radians(pri_lat)) *
                                    cos(radians(pri_long) - radians(' . $user->pri_long . ')) +
                                    sin(radians(' . $user->pri_lat . ')) * sin(radians(pri_lat))))
                                    AS distance
                                    FROM per_info) AS distances
                                WHERE distance < ' . $distance . '
                                AND assigned_supervisor = ' . $user->regId . '
                                AND r_id = 1
                                 AND status = 1
                                AND job_status = 1
                                ORDER BY distance;
                            ');
            $map_active_count = DB::table('per_info')
                    ->where("assigned_supervisor", $user->regId)
                    ->where("status", 1)
                    ->where("job_status", 1)
                    ->where("r_id", 1)
                    ->count();
            }

            
            return response()
                    ->json(array(
                    "status" => "true",
                    "message" => "Logged in.",
                    "panic_count" => $panic_count,
                    "new_kamay" => $new_kamay,
                    "new_jobs" => $new_jobs,
                    "map" => $map_active_count,
                    "near_by" => $near_by_count[0]->kamay_count,
                    "poor_review" => $poor_review_count,
                    "assigned_job" => $assigned_count,
                    "incomplete_docs" => $in_comp_doc,
                    "value" => $output_value
                ));

        }
    }

    public function panic_count($query_value, $query_sign, $query_where, $filter_sign, $status, $regId){
        $panic_count = DB::table('panic_reported')
                        ->where('panic_reported.supervisor_id', $regId)
                        ->where('panic_reported.status', $filter_sign ,$status)
                        ->whereDate($query_where, $query_sign, $query_value)
                        ->count();
        return $panic_count;
    }

    public function supervisorPanicDetails(Request $request, $duration, $status = 0){
        // $status = 1 Resolved
        // $status = 2 unResolved
        $headers = $request->header('Authentication');
        $response = $this->getSessionId($headers);
        if ($response != '1')
        {
            return $response;
        }
        else
        {
            $user = DB::table('per_info')->where('session_id', $request->header('Authentication'))->first();
            if($duration == 'a'){
                $output_value = 'All';
                $query_where = "panic_reported.created_at";
                $query_sign = "<=";
                $query_value = Carbon::today();
            }else if($duration == 't'){
                $output_value = 'Today';
                $query_where = "panic_reported.created_at";
                $query_sign = "=";
                $query_value = Carbon::now();
            }else if($duration == 'w'){
                $output_value = 'Week';
                $query_where = "panic_reported.created_at";
                $query_sign = ">=";
                $query_value = Carbon::today()->subDays(7);
            }else{
                $output_value = 'Month';
                $query_where = "panic_reported.created_at";
                $query_sign = ">=";
                $query_value = Carbon::today()->subDays(30);
            }
            
            $filter_status = "";
                
            if($status == 0){
                $filter_status = "All";
                $filter_sign = ">=";
            }else if($status == 1){
                $filter_status = "Resolved";
                $filter_sign = "=";
            }else if($status == 2){
                $filter_status = "Unresolved";
                $filter_sign = "=";
            }

            $panic_details = DB::table('panic_reported')
                                    ->join('orders', 'orders.order_id', 'panic_reported.order_id')
                                    ->join('per_info as p2', 'orders.regId', '=', 'p2.regId')
                                    ->join('per_info', 'orders.assign_to', '=', 'per_info.regId')
                                    ->join('per_info as p3', 'panic_reported.reg_Id', 'p3.regId')
                                    ->join('services', 'services.s_id', '=', 'orders.s_id')
                                    ->select(
                                        'panic_reported.id AS panic_id',
                                        'panic_reported.reg_Id AS reporter_id',
                                        'panic_reported.created_at',
                                        'p3.image as reporter_image',
                                        'p2.phn_no as customer_phone_number',
                                        'p2.fullName as customer_name',
                                        'p2.image as customer_image',
                                        'panic_reported.reason AS reason',
                                        'panic_reported.status_name AS current_status',
                                        'panic_reported.role_id as panic_reported_role_id',
                                        'orders.order_id AS order_id',
                                        'orders.full_address',
                                        'orders.assign_to as assign_to',
                                        'services.description as service_description',
                                        'services.name as service_name',
                                        'per_info.assigned_supervisor'
                                    )
                                    ->where('panic_reported.status', $filter_sign ,$status)
                                    ->where('per_info.assigned_supervisor', $user->regId)
                                    ->whereDate($query_where, $query_sign, $query_value)
                                    ->orderBy('panic_reported.id', 'DESC')
                                    ->get();
                                    
                                    
                foreach($panic_details as $panic_detail){
                    $panic_detail->diff_time = Carbon::parse($panic_detail->created_at)->diffForHumans();
                    $panic_detail->time = date('l jS F h:i A', strtotime($panic_detail->created_at));
                    $assign = DB::table('per_info')
                                    ->select('regId', 'fullName', 'nickName', 'cnic', 'phn_no', 'city_id', 'terr_id', 's_id', 'email', 'image', 'pri_lat', 'pri_long', 'verufied_by', 'assigned_supervisor', 'callcenter_id', 'r_id', 'status', 'kamay_application_status', 'vehicle_no', 'rating_number', 'rating_count')
                                    ->where('regId', $panic_detail->assign_to)->first();
                    $panic_detail->assign = $assign;
                    if($panic_detail->panic_reported_role_id == 1){
                        $panic_detail->reported_by = 'Kamay';
                    }else if($panic_detail->panic_reported_role_id == 3){
                        $panic_detail->reported_by = 'Customer';
                    }
                }
                

            return response()
                    ->json(array(
                    "status" => "true",
                    "message" => "Logged in.",
                    "data" => $panic_details,
                    "value" => $output_value,
                    "filter_status" => $filter_status
                ));
        }
    }

    public function supervisorNewJobs(Request $request, $duration){
        $headers = $request->header('Authentication');
        $response = $this->getSessionId($headers);
        if ($response != '1')
        {
            return $response;
        }
        else
        {
           if($duration == 'a'){
                $output_value = 'All';
                $query_where = "orders.create_at";
                $query_sign = "<=";
                $query_value = Carbon::today();
            }else if($duration == 't'){
                $output_value = 'Today';
                $query_where = "orders.create_at";
                $query_sign = "=";
                $query_value = Carbon::today();
            }else if($duration == 'w'){
                $output_value = 'Week';
                $query_where = "orders.create_at";
                $query_sign = ">=";
                $query_value = Carbon::today()->subDays(7);
            }else{
                $output_value = 'Month';
                $query_where = "orders.create_at";
                $query_sign = ">=";
                $query_value = Carbon::today()->subDays(30);
            }
            $new_jobs = DB::table('orders')
                                ->join('services', 'services.s_id', '=', 'orders.s_id')
                                ->select(
                                    'orders.order_id AS order_id',
                                    'services.s_id AS service_id',
                                    'services.name AS service_name',
                                    'services.service_image AS service_image',
                                    'orders.full_address AS full_address',
                                    'orders.payment_method AS payment_method',
                                    DB::raw('DATE_FORMAT(orders.create_at, "%d-%b-%y") as service_date'),
                                    'orders.total_amount as total_amount'
                                )
                                ->where('orders.assign_to', null)
                                ->whereDate($query_where, $query_sign, $query_value)
                                ->orderBy('orders.order_id', 'desc')
                                ->get();
                    
            return response()
                    ->json(array(
                    "status" => "true",
                    "message" => "Logged in.",
                    "data" => $new_jobs,
                    "value" => $output_value
                ));
        }
    }
    
    

    public function supervisorNewJobsStatus(Request $request, $a){
        $headers = $request->header('Authentication');
        $response = $this->getSessionId($headers);
        if ($response != '1')
        {
            return $response;
        }
        else
        {
            $value = '';
            $user = DB::table('per_info')->where('session_id', $request->header('Authentication'))->first();
            if($a == 'Completed'){
                $value = array('Completed');
            }else if($a == 'Cancelled'){
                $value = array('Cancelled');
            }else if($a == 'inprogress'){
                $value = array('Enroute', 'Estimate Pending', 'Estimate Accepted', 'In progress', 'Invoice Pending', 'Invoice Accepted', 'Arrived');
            }
            $new_jobs = DB::table('orders')
                                ->join('services', 'services.s_id', '=', 'orders.s_id')
                                ->select(
                                    'orders.order_id AS order_id',
                                    'services.s_id AS service_id',
                                    'services.name AS service_name',
                                    'services.service_image AS service_image',
                                    'orders.full_address AS full_address',
                                    'orders.payment_method AS payment_method',
                                    'orders.total_amount as total_amount'
                                )
                                ->where('orders.assign_to', $user->regId)
                                ->whereIn('orders.status', $value)
                                ->orderBy('orders.order_id', 'desc')
                                ->get();
            return response()
                    ->json(array(
                    "status" => "true",
                    "message" => "All Jobs According to Status.",
                    "data" => $new_jobs,
                    "value" => $a
                ));
        }
    }

    public function supervisorAllJob(Request $request){
        $headers = $request->header('Authentication');
        $response = $this->getSessionId($headers);
        if ($response != '1')
        {
            return $response;
        }
        else
        {
            $user = DB::table('per_info')->where('session_id', $request->header('Authentication'))->first();
            $new_jobs = DB::table('orders')
                            ->join('services', 'services.s_id', '=', 'orders.s_id')
                            ->where('orders.assign_to', null)
                            ->count();
            $in_progress = DB::table('order_info_v')
                            ->join('per_info', 'per_info.regId', '=', 'order_info_v.worker_id')
                            ->where('per_info.assigned_supervisor',  $user->regId)
                            ->where('order_info_v.status','InProgress')
                            ->count();
            $cancelled = DB::table('orders')
                            ->join('services', 'services.s_id', '=', 'orders.s_id')
                            ->where('orders.status', 'Cancelled')
                            ->where('orders.assign_to', $user->regId)
                            ->count();
            $completed = DB::table('order_info_v')
                            ->join('per_info', 'per_info.regId', '=', 'order_info_v.worker_id')
                            ->where('per_info.assigned_supervisor',  $user->regId)
                            ->where('order_info_v.status','Completed')
                            ->count();
                            
            $on_hold = DB::table('order_info_v')
                            ->join('per_info', 'per_info.regId', '=', 'order_info_v.worker_id')
                            ->where('per_info.assigned_supervisor',  $user->regId)
                            ->where('order_info_v.status','OnHold')
                            ->count(); 
            $schedule = DB::table('schedule_orders')
                    ->join('per_info', 'schedule_orders.regId', 'per_info.regId')
                    ->join('schedule_order_details', 'schedule_order_details.order_id', 'schedule_orders.id')
                    ->join('services', 'services.s_id', 'schedule_orders.s_id')
                    ->select(
                            'per_info.fullName as customername',
                            'per_info.pri_add as cust_address',
                            'services.name as service_name',
                            'services.service_image as service_image',
                            'schedule_orders.id as order_id',
                            'schedule_orders.full_address as order_address',
                            DB::raw('DATE_FORMAT(schedule_orders.shedule_time, "%d-%b-%y %h:%i%p") as sdate'),
                            'schedule_orders.total_amount'
                    )
                    ->count();
            
           
                        
            
            return response()
                    ->json(array(
                        "status" => "true",
                        "message" => "All Jobs Count.",
                        "new_jobs" => $new_jobs,
                        "cancelled_count" => $cancelled,
                        "completed_count" => $completed,
                        "inprogress_count" => $in_progress,
                        "schedule" => $schedule,
                        "on_hold" => $on_hold
                    ));
        }
    }
    
    
    public function getPoorReviews(Request $request, $duration){
        
         $headers = $request->header('Authentication');
        $response = $this->getSessionId($headers);
        if ($response != '1')
        {
            return $response;
        }
        else
        {
            $user = DB::table('per_info')->where('session_id', $request->header('Authentication'))
                ->first();
             if($duration == 'a'){
                $output_value = 'All';
                $query_where = "orders.create_at";
                $query_sign = "<=";
                $query_value = Carbon::today();
            }else if($duration == 't'){
                $output_value = 'Today';
                $query_where = "orders.create_at";
                $query_sign = "=";
                $query_value = Carbon::today();
            }else if($duration == 'w'){
                $output_value = 'Week';
                $query_where = "orders.create_at";
                $query_sign = ">=";
                $query_value = Carbon::today()->subDays(7);
            }else{
                $output_value = 'Month';
                $query_where = "orders.create_at";
                $query_sign = ">=";
                $query_value = Carbon::today()->subDays(30);
            }
            
            $jobdetails = DB::table('orders')
                                ->join('services', 'services.s_id', '=', 'orders.s_id')
                                ->join('per_info', 'per_info.regId', '=', 'orders.assign_to')
                                ->select(
                                    'orders.order_id AS order_id',
                                    'orders.regId AS worker_id',
                                    'orders.create_at AS job_date',
                                    'orders.remark_by_customer as comments',
                                    'orders.rate_by_customer AS customer_rating',
                                    'per_info.fullname as worker_name',
                                    'per_info.image as worker_image',
                                    'per_info.email as worker_email',
                                    'per_info.phn_no as worker_phone',
                                    'per_info.assigned_supervisor',
                                    'services.s_id AS service_id',
                                    'services.name AS service_name',
                                    'services.service_image AS service_image',
                                    'orders.full_address AS full_address',
                                    'orders.payment_method AS payment_method',
                                    'orders.total_amount as total_amount'
                                )
                                ->where('per_info.assigned_supervisor',$user->regId)
                                ->whereBetween('orders.rate_by_customer', [1, 2])
                                ->whereDate($query_where, $query_sign, $query_value)
                                ->orderBy('orders.order_id', 'desc')
                                ->get();
                                
            
            $data = [];
            $data['jobdetails'] = $jobdetails;
           // $data['worker_info'] = $worker_info;
                                    
                    
            return response()
                    ->json(array(
                    "status" => "true",
                    "message" => "Logged in.",
                    "data" => $data,
                    "value" => $output_value
                ));
        }
        
    }
    public function supervisorNewJobsDetails(Request $request, $id){
        $headers = $request->header('Authentication');
        $response = $this->getSessionId($headers);
        if ($response != '1')
        {
            return $response;
        }
        else
        {
            $order_details['order_details'] = DB::table('order_details')
                                ->join('services', 'services.s_id', '=', 'order_details.s_id')
                                ->join('orders', 'orders.order_id', '=', 'order_details.order_id')
                                ->select(
                                    'orders.regId AS reg_id',
                                    'orders.order_id AS order_id',
                                    'order_details.order_details_id AS order_details_id',
                                    'order_details.quantity AS order_details_quantity',
                                    'services.s_id AS service_id',
                                    'services.name AS service_name',
                                    'services.service_image AS service_image'
                                )
                                ->where('order_details.order_id', $id)
                                ->get();
            if(count($order_details['order_details']) != 0){
                $order_details['customer_name'] = DB::table('per_info')
                                                ->where('regId', $order_details['order_details'][0]->reg_id)
                                                ->select(
                                                    'per_info.fullName AS customer'
                                                )
                                                ->first();
            }else{
                $order_details['customer_name'] = null;
            }

            $order_details['address']  = DB::table('orders')
                                            ->where('order_id', $id)
                                            ->select(
                                                'orders.full_address AS full_address',
                                                'orders.total_amount AS total_amount',
                                                'orders.payment_method AS payment_method'
                                            )
                                            ->first();

            $order_details['parent_service']  = DB::table('orders')
                                                    ->join('services', 'services.s_id', '=', 'orders.s_id')
                                                    ->where('order_id', $id)
                                                    ->select(
                                                        'services.name'
                                                    )
                                                    ->first();

            return response()
                ->json(array(
                "status" => "true",
                "message" => "Logged in.",
                "data" => $order_details,
            ));
        }
    }

    public function newKamay(Request $request){
        $headers = $request->header('Authentication');
        $response = $this->getSessionId($headers);
        if ($response != '1')
        {
            return $response;
        }
        else
        {
            
            $user = DB::table('per_info')->where('session_id', $request->header('Authentication'))->first();
            $per_info = DB::table('per_info')
                                ->join('user_services', 'user_services.reg_id', '=', 'per_info.regId')
                                ->join('services', 'user_services.s_id', '=', 'services.s_id')
                                
                                
                                ->where('per_info.status', 0)
                                ->where('per_info.assigned_supervisor', $user->regId)
                                ->whereBetween('per_info.assigned_supervisor_date', [Carbon::now()->subDays(10), Carbon::now()])
                                ->where('per_info.r_id', 1)
                                ->select(
                                    'per_info.image AS image',
                                    'per_info.assigned_supervisor_date AS assigned_date',
                                    'per_info.regId AS regId',
                                    'per_info.fullName AS fullName',
                                    'per_info.email AS email',
                                    'per_info.phn_no AS phn_no',
                                    'per_info.cnic AS cnic',
                                     DB::raw('count(per_info.fullName) as total'),
                                     DB::raw('group_concat(services.name) as service')
                                     
                                     
                                     
                                     
                                )->groupBy('regId')
                                ->orderBy('regId', 'desc')
                                ->get();
                           // dump(count($per_info));die;     
            if(count($per_info) != 0){
                if($per_info[0]->total != 0){
                    
                    foreach($per_info as $per_review){
                         
                         $review = DB::table('review_checklist_status')
                                        ->leftjoin('review_status', 'review_checklist_status.review_status', '=', 'review_status.id')
                                        ->select('status_name')
                                        ->where('review_checklist_status.reg_id', $per_review->regId)
                                        ->where('review_checklist_status.review_checklist_id',10)
                                        ->get();
                        if (count($review) != 0) {
                             $per_review->application_status = $review;
                        }else{
                            
                            $per_review->application_status = [];
                        }
                        
                
                    }
                    
                    foreach($per_info as $per_infos){
                        
                        // $percentage = DB::table('review_checklist_status')
                        //                 ->join('review_status', 'review_checklist_status.review_status', '=', 'review_status.id')
                        //                 ->where('review_checklist_status.reg_id', $per_infos->regId)
                        //                 ->where('review_status.status_name', 'Completed')
                                        
                        //                 ->count();

                        $percentage = DB::select( DB::raw(" SELECT
                                            count(review_status.status_name) as counter
                                            FROM
                                            review_checklist
                                            INNER JOIN review_checklist_status ON review_checklist.id = review_checklist_status.review_checklist_id
                                            INNER JOIN review_status ON review_checklist_status.review_status = review_status.id
                                            WHERE (review_checklist_status.reg_id = $per_infos->regId)
                                            and (review_status.status_name = 'Completed' or review_status.status_name = 'Shared' or review_status.status_name = 'Not required' or review_status.status_name = 'Approved' or review_status.status_name = 'Assign training')"));
                   
                     
                        $per_infos->percentage = $percentage[0]->counter;
                    }
                }else{
                    $per_info = [];
                }
            }
            

            return response()
                ->json(array(
                "status" => "true",
                "message" => "New Kamay",
                "data" => $per_info,
                "date" => Carbon::now()->subDays(10),
                // "output_value" =>  $output_value
            ));
        }
    }
    
    
    public function incompleteDoc(Request $request){
        $headers = $request->header('Authentication');
        $response = $this->getSessionId($headers);
        if ($response != '1')
        {
            return $response;
        }
        else
        {
            $user = DB::table('per_info')->where('session_id', $request->header('Authentication'))->first();
            
            $per_info = DB::table('per_info')
                                ->join('user_services', 'user_services.reg_id', '=', 'per_info.regId')
                                ->join('services', 'user_services.s_id', '=', 'services.s_id')
                                
                                
                                ->where('per_info.status', 0)
                                ->where('per_info.assigned_supervisor', $user->regId)
                                ->where('per_info.assigned_supervisor_date','<=', Carbon::now()->subDays(6))
                                ->where('per_info.r_id', 1)
                                ->select(
                                    'per_info.image AS image',
                                    'per_info.assigned_supervisor_date AS assigned_date',
                                    'per_info.regId AS regId',
                                    'per_info.fullName AS fullName',
                                    'per_info.email AS email',
                                    'per_info.phn_no AS phn_no',
                                    'per_info.cnic AS cnic',
                                     DB::raw('count(per_info.fullName) as total'),
                                     DB::raw('group_concat(services.name) as service')
                                     
                                     
                                     
                                     
                                )->groupBy('regId')
                                ->orderBy('regId', 'desc')
                                ->get();
                           // dump(count($per_info));die;     
            if(count($per_info) != 0){
                if($per_info[0]->total != 0){
                    
                    foreach($per_info as $per_review){
                         
                         $review = DB::table('review_checklist_status')
                                        ->leftjoin('review_status', 'review_checklist_status.review_status', '=', 'review_status.id')
                                        ->select('status_name')
                                        ->where('review_checklist_status.reg_id', $per_review->regId)
                                        ->where('review_checklist_status.review_checklist_id',10)
                                        ->get();
                        if (count($review) != 0) {
                             $per_review->application_status = $review;
                        }else{
                            
                            $per_review->application_status = [];
                        }
                        
                
                    }
                    
                    foreach($per_info as $per_infos){
                        
                        $percentage = DB::table('review_checklist_status')
                                        ->join('review_status', 'review_checklist_status.review_status', '=', 'review_status.id')
                                        ->where('review_checklist_status.reg_id', $per_infos->regId)
                                        ->orWhere(function($query) {
                                            $query->where('review_status.status_name', 'Completed')
                                                  ->where('review_status.status_name', 'Not required')
                                                  ->where('review_status.status_name', 'Approved');
                                        })
                                        ->count();
                        
                        $per_infos->percentage = $percentage;
                    }
                }else{
                    $per_info = [];
                }
            }
            
            return response()
                ->json(array(
                "status" => "true",
                "message" => "Incomplete Docus",
                "data" => $per_info,
                "date" => Carbon::now()->subDays(10),
                // "output_value" =>  $output_value
            ));

        }
    }

    public function reviewChecklist(Request $request, $id){
        $headers = $request->header('Authentication');
        $response = $this->getSessionId($headers);
        if ($response != '1')
        {
            return $response;
        }
        else
        {
            $review_checklist = DB::select( DB::raw("select a.name as doc_name, a.reg_id,a.id as doc_id, rcs.review_checklist_id, rcs.review_status as active_status
                                                   ,(select rsa.status_name FROM `review_status` rsa where rsa.id=rcs.review_status) active_status_name
                                                   , a.status_id_list,a.status_list
                                                    from (SELECT p.regid as reg_id,rc.id , rc.name,
                                                    
                                                    (select GROUP_CONCAT(rs.id) from review_status rs where rs.review_checklist_id=rc.id) status_id_list, 
                                                    
                                                    (select GROUP_CONCAT(rs.status_name) from review_status rs where rs.review_checklist_id=rc.id) status_list
                                                    FROM review_checklist rc,per_info p where p.r_id=1) a  
                                                    LEFT join 
                                                    review_checklist_status rcs
                                                    on rcs.reg_id=a.reg_id and rcs.review_checklist_id = a.id
                                                    where a.reg_id=:reg_id
                                                    ORDER BY a.id
                                                    "), array('reg_id' => $id));
                                                    
            // DB::select( DB::raw("SELECT * FROM some_table WHERE some_col = :somevariable"), array(
            //               'somevariable' => $someVariable,
            //              ));
            // $review_checklist = DB::table('review_checklist')
            //                         ->leftjoin('review_checklist_status', 'review_checklist_status.review_checklist_id', '=', 'review_checklist.id')
            //                         ->leftjoin('review_status', 'review_status.id', '=', 'review_checklist_status.review_status')
            //                         ->select(
            //                             'review_checklist_status.id as review_checklist_status_id',
            //                             'review_status.status_name as status',
            //                             'review_checklist_status.review_status as review_status_id',
            //                             'review_checklist_status.reg_id as reg_id',
            //                             'review_checklist.id AS review_checklist_id',
            //                             'review_checklist.name AS review_checklist_name'
            //                         )
            //                         ->where('review_checklist_status.reg_id', $id)
            //                         ->orWhereNull('review_checklist_status.reg_id')
            //                         ->get();
            
            // foreach($review_checklist as $review_checklists){
            //     $status_name = DB::table('review_status')
            //                         ->where('review_status.review_checklist_id', $review_checklists->review_checklist_id)
            //                         ->select(
            //                             'review_status.id AS review_status_id',
            //                             'review_status.status_name AS status_name'
            //                         )
            //                         ->get();
                
            //     $review_checklists->status_name = $status_name; 
            // }

            return response()
                ->json(array(
                "status" => "true",
                "message" => "Review checklist",
                "data" => $review_checklist,
            ));
        }
    }
    
    public function checklistUpdate(Request $request){
        $headers = $request->header('Authentication');
        $response = $this->getSessionId($headers);
        if ($response != '1')
        {
            return $response;
        }
        else
        {
            
            $user = DB::table('per_info')->where('session_id', $request->header('Authentication'))->first();
            
            $reg_id = $request->input('reg_id');
            $review_checklist_id = $request->input('review_checklist_id');
            $review_status = $request->input('review_status');
            if ($review_status == 20){
               
                DB::table('per_info')
                          ->where('regId', $reg_id)
                          ->update([
                                    'verufied_by' => $user->regId
                                    ]);
                $body = "final_approved_supervisor";
                $this->kamayUserController->backOfficeNotification($body, $reg_id);
            }
            $checkIfExist = DB::table('review_checklist_status')
                                ->select('*')
                                ->where('reg_id', $reg_id)
                                ->where('review_checklist_id', $review_checklist_id)
                                ->first();
                                
            if($checkIfExist == null){
                $data = DB::table('review_checklist_status')->insert(
                     array(
                            'reg_id'     =>   $reg_id, 
                            'review_checklist_id'   =>   $review_checklist_id,
                            'review_status'   =>   $review_status,
                     )
                );
                return response()
                    ->json(array(
                    "status" => "true",
                    "message" => "Inserted Checklist",
                    "data" => $data,
                ));
            }else{
                $data = DB::table('review_checklist_status')
                            ->where('reg_id', $reg_id)
                            ->where('review_checklist_id', $review_checklist_id)
                            ->update(
                                array(
                                    'review_status' => $review_status
                                    )
                                );  
                
                return response()
                    ->json(array(
                    "status" => "true",
                    "message" => "Updated Checklist",
                    "data" => $data,
                ));
            }
        }
    }
    
    public function myKamay(Request $request){
        $headers = $request->header('Authentication');
        $response = $this->getSessionId($headers);
        if ($response != '1')
        {
            return $response;
        }
        else
        {
            $user = DB::table('per_info')->where('session_id', $request->header('Authentication'))->first();
            $data = DB::table('per_info')
                    ->where("assigned_supervisor", $user->regId)
                    ->where("r_id", 1)
                    ->where("status", 1)
                    ->orderBy('job_status', 'desc')
                    ->get();
            return response()
                    ->json(array(
                    "status" => "true",
                    "message" => "My Kamay",
                    "data" => $data,
                ));
        }
    }
    
    public function myKamayMap(Request $request){
        $headers = $request->header('Authentication');
        $response = $this->getSessionId($headers);
        if ($response != '1')
        {
            return $response;
        }
        else
        {
            $user = DB::table('per_info')->where('session_id', $request->header('Authentication'))->first();
            $data = DB::table('per_info')
                    ->where("assigned_supervisor", $user->regId)
                    ->where("status", 1)
                    ->where("job_status", 1)
                    ->where("r_id", 1)
                    ->get();
            return response()
                    ->json(array(
                    "status" => "true",
                    "message" => "My Active Kamay",
                    "data" => $data,
                ));
        }
    }
    public function nearByKamay(Request $request, $distance){
        $headers = $request->header('Authentication');
        $response = $this->getSessionId($headers);
        if ($response != '1')
        {
            return $response;
        }
        else
        {
            $user = DB::table('per_info')->where('session_id', $request->header('Authentication'))->first();
            $circle_radius = 3959;
            $results = DB::select(
                            'SELECT * FROM
                                (SELECT *, (' . $circle_radius . ' * acos(cos(radians(' . $user->pri_lat . ')) * cos(radians(pri_lat)) *
                                    cos(radians(pri_long) - radians(' . $user->pri_long . ')) +
                                    sin(radians(' . $user->pri_lat . ')) * sin(radians(pri_lat))))
                                    AS distance
                                    FROM per_info) AS distances
                                WHERE distance < ' . $distance . '
                                AND assigned_supervisor = ' . $user->regId . '
                                AND r_id = 1
                                AND status = 1
                                AND job_status = 1
                                ORDER BY distance;
                            ');
            
            return response()
                    ->json(array(
                    "status" => "true",
                    "message" => "Near By Kamay " . $distance,
                    "data" => $results,
                ));
            
        }
    }
    
    public function kamaProfile(Request $request, $id)
    {
        $headers = $request->header('Authentication');
        $response = $this->getSessionId($headers);
        if ($response != '1')
        {
            return $response;
        }
        else
        {
            

            
                $users = DB::table('per_info')
                    ->join('user_services', 'user_services.reg_id', '=', 'per_info.regId')
                    ->join('services', 'user_services.s_id', '=', 'services.s_id')
                    ->join('city', 'city.city_id', '=', 'per_info.city_id')
                    ->select('per_info.fullName', 'per_info.image', 'per_info.vehicle_no','per_info.email','per_info.cnic', DB::raw('group_concat(services.name) as expertise'), 'city.name as kamay_city', 'per_info.phn_no', 'per_info.regId', 'per_info.pri_lat', 'per_info.pri_long','per_info.rating_number','per_info.rating_count')
                    ->where('per_info.regId', '=', $id)->get();

                if ($users->count() > 0)
                {

                     if($users[0]->rating_number != 0){
                        $users[0]->ratings = $users[0]->rating_number / $users[0]->rating_count;
                    }else{
                        $users[0]->ratings = 0;
                    }

                    return response()
                        ->json(array(
                        "status" => "true",
                        "message" => "Kama Found",
                        'kama_profile' => $users
                    ));
                }

                else
                {
                    return response()->json(array(
                        "status" => "true",
                        "message" => "Kama not Found",
                        'kama_profile' => $users
                    ));
                }
            

             return response()
                    ->json(array(
                    "status" => "true",
                    "message" => "Near By Kamay " . $distance,
                    "data" => $results,
                ));
            
        }
    }
    
    
    public function JobsStatusForSupervisor(Request $request, $status, $duration){
        $headers = $request->header('Authentication');
        $response = $this->getSessionId($headers);
        if ($response != '1')
        {
            return $response;
        }
        else
        {
            $value = '';
            $user = DB::table('per_info')->where('session_id', $request->header('Authentication'))->first();
            
            if($duration == 'a'){
                $output_value = 'All';
                $query_where = "sdate";
                $query_sign = "<=";
                $query_value = Carbon::today();
            }else if($duration == 't'){
                $output_value = 'Today';
                $query_where = "sdate";
                $query_sign = "=";
                $query_value = Carbon::today()->toDateString();
            }else if($duration == 'w'){
                $output_value = 'Week';
                $query_where = "sdate";
                $query_sign = ">=";
                $query_value = Carbon::today()->subDays(7);
            }else{
                $output_value = 'Month';
                $query_where = "sdate";
                $query_sign = ">=";
                $query_value = Carbon::today()->subDays(30);
            }
            
            
            if($status == 'assigned_jobs'){
                $value = 'InProgress';
            }else if($status == 'cancelled_jobs'){
                $value = 'Cancelled';
            }else if($status == 'inprogress_jobs'){
                $value = 'InProgress';
            }else if($status == 'completed_jobs'){
                $value = 'Completed';
            }else if($status == 'onhold_jobs'){
                $value = 'OnHold';
            }else if($status == 'schedule_job'){
                    $value = 'schedule';
                    // $schedual = DB::select("select `wi`.`regId` AS `worker_id`,`wi`.`fullName` AS `workername`,`ci`.`fullName` AS `customername`,`ci`.`pri_add` AS `cust_address`,`o`.`s_id` AS `s_id`,`s`.`name` AS `service_name`,`s`.`service_image` AS `service_image`,(select ifnull(group_concat(`s1`.`name`,'(',`o1`.`quantity`,') ' separator ','),'Only Visit Charges') from (`schedule_order_details` `o1` join `services` `s1`) where `o1`.`s_id` = `s1`.`s_id` and `o1`.`order_id` = `o`.`id`) AS `services`,`o`.`id` AS `order_id`,`o`.`payment_method` AS `payment_method`,`o`.`total_amount` AS `total_amount`,`o`.`full_address` AS `order_address`,`o`.`create_at` AS `sdate`,`o`.`assign_to` AS `assigned_worker` from (((`schedule_orders` `o` left join `per_info` `wi` on(`wi`.`regId` = `o`.`assign_to`)) left join `per_info` `ci` on(`ci`.`regId` = `o`.`regId`)) left join `services` `s` on(`s`.`s_id` = `o`.`s_id`))"); 

                    $schedual = DB::table('schedule_orders')
                    ->join('per_info', 'schedule_orders.regId', 'per_info.regId')
                    ->join('schedule_order_details', 'schedule_order_details.order_id', 'schedule_orders.id')
                    ->join('services', 'services.s_id', 'schedule_orders.s_id')
                    ->select(
                            'per_info.fullName as customername',
                            'per_info.pri_add as cust_address',
                            'services.name as service_name',
                            'services.service_image as service_image',
                            'schedule_orders.id as order_id',
                            'schedule_orders.full_address as order_address',
                            DB::raw('DATE_FORMAT(schedule_orders.shedule_time, "%d-%b-%y %h:%i%p") as sdate'),
                            'schedule_orders.total_amount'
                    )
                    ->whereDate('schedule_orders.create_at', $query_sign, $query_value)
                    ->get();

            
                return response()
                    ->json(array(
                    "status" => "true",
                    "message" => "All Jobs According to Status.",
                    "data" => $schedual,
                    "value" => $value,
                    "duration" => $output_value
                ));
            }
            
            $data = DB::table('order_info_v')
                ->join('per_info', 'per_info.regId', 'order_info_v.worker_id')
                ->where('per_info.assigned_supervisor',  $user->regId)
                ->where('order_info_v.status',$value)
                ->whereDate($query_where, $query_sign, $query_value)
                    ->get();
            return response()
                    ->json(array(
                    "status" => "true",
                    "message" => "All Jobs According to Status.",
                    "data" => $data,
                    "value" => $value,
                    "duration" => $output_value
                ));
        }
    }
    
    
    public function updatePanic(Request $request)
    {
        
        $headers = $request->header('Authentication');
        $response = $this->getSessionId($headers);
        if ($response != '1')
        {
            return $response;
        }
        else
        {
             $user = DB::table('per_info')->where('session_id', $request->header('Authentication'))->first();
            
            $PanicCreatedBy = $request->input('id');
            
            $status = $request->input('status');
             //return $request; exit();
            if($status == 'Resolved'){
                $cstatus = 1;
               
            }else if($status == 'No action required'){
                $cstatus = 1;
            }else if($status == 'False alarm'){
                $cstatus = 1;
            }else if($status == 'Helpline activated'){
                $cstatus = 2;
            }
             

            $form_data = array(
                //'reason'        => $request->input('panic_reason'),
                'status'        => $cstatus,
                'status_name'   => $request->input('status'),
                'resolved_by'   => $user->regId
                );
            
            $perinfo = DB::table('panic_reported')->where('id',$PanicCreatedBy)->update($form_data);
             return response()
                    ->json(array(
                    "status" => "true",
                    "message" => "Panic status updated",
                    "data" => $perinfo
                ));
        }
               
    }

}
