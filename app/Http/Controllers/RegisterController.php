<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Vendorproduct;
use App\Vendor;

class RegisterController extends Controller
{
      public function notification(Request $request){

    $register = $request->isMethod('put') ? Users::findOrFail($request->id) : new Users;

      $register->token = $request->input('token');
     
      $register->save();
      return response()->json([
        
        'status'  => 'true',
        'message' => 'data inserted'

      ]);
  }
  
  public function vendorProducts(){

  	return response()->json(Vendorproduct::get());
  }
  
    public function createVendor(Request $request){

  	  $register = new Vendor;

      // $register->regId = $request->input('regId');
      $register->id = $request->input('id');
      $register->shop_name = $request->input('shop_name');
      $register->owner_name = $request->input('owner_name'); 
      $register->shop_address = $request->input('shop_address');
      $register->longitude = $request->input('longitude');
      $register->latitude = $request->input('latitude'); 
      $register->landline = $request->input('landline');
      $register->mobile1 = $request->input('mobile1');
      $register->mobile2 = $request->input('mobile2');
      $register->mobile3 = $request->input('mobile3');
      $register->cnic = $request->input('cnic');
      $register->others = $request->input('others');
      $register->s_id = $request->input('s_id');      
      $image = $request->file('image');
      $filename = rand().'.'.$image->getClientOriginalExtension();
      $path = $request->file('image')->move(public_path("../public_html/image"), $filename);
      $register->image = url('/image/'.$filename);
     
      $image2 = $request->file('image2');
      $filename = rand().'.'.$image->getClientOriginalExtension();
      $path = $request->file('image2')->move(public_path("../public_html/image"), $filename);
      $register->image2 = url('/image/'.$filename);

      $image3 = $request->file('image3');
      $filename = rand().'.'.$image->getClientOriginalExtension();
      $path = $request->file('image3')->move(public_path("../public_html/image"), $filename);
      $register->image3 = url('/image/'.$filename);
      $register->save();
      return response()->json([
        
        'status'  => 'true',
        'message' => 'data inserted'

      ]);
  }
}
