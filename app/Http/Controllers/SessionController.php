<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;        
use App\Registration;
use App\Register;
use Illuminate\Support\Facades\DB;

class SessionController extends Controller
{
    public function login_details($phn_no, $password){
       $password = md5($password);

        $results = DB::select( DB::raw("select regId, fullName ,phn_no from per_info where phn_no =:phn_no and password =:password and r_id = 2 and r_id = 2  and status = 1"), array(
        'phn_no' => $phn_no,
        'password' => $password,)
);

if ($results){
    // return $results;
    $someArray = json_encode($results, true);
    $someArray = json_decode($someArray, true);
    // print_r($someArray);        // Dump all data of the Array
    $regId = $someArray[0]["regId"]; // Access Array data
    $time=md5(date("h:i:sa"));
    $session=($time.$regId);
    // return $session;
    // DB::statement("UPDATE per_info SET session_id = '".$session."'where regId = $regId");
    $value = DB::table('per_info')
    ->where('regId',$regId)
    ->update(['session_id'=>$session]);
    // return $value;

    if($value = 1){
            $user_details = DB::select( DB::raw("select regId, fullName ,phn_no,session_id from per_info where phn_no =:phn_no and password =:password and r_id = 2 and status = 1"), array(
        'phn_no' => $phn_no,
        'password' => $password,));
        return response()->json(array("status"=>"true", "message"=>"Logged in.", "user"=>$user_details));

    }
    // return "updated";
//     $results = DB::select( DB::raw("select regId, fullName ,phn_no,session from per_info where phn_no =:phn_no and password =:password"), array(
//         'phn_no' => $phn_no,
//         'password' => $password,)
// );

}

else
{

  return response()->json(array("status"=>"false", "message"=>"Invalid Credentials.", "user"=>$results));
}

    }
    

  public function insert_data(Request $request){

        $model = new Registration;
        $token = $request->input('token');
        $reg_id = $request->input('reg_id');
        //$model->regId = $request->input('regId');
    	$model->fullName = $request->input('fullName');
    	$model->nickName = $request->input('nickName');
        $model->phn_no = $request->input('phn_no');
        $model->phn_no2 = $request->input('phn_no2');
        $model->phn_no3 = $request->input('phn_no3');
        $model->cnic = $request->input('cnic');
    // 	$model->address = $request->input('address'); 
        $model->verified_by = $reg_id;
        $model->city_id = $request->input('city_id');
        $model->s_id = $request->input('s_id');
    	// $model->area = $request->input('area');
    	$model->longitude = $request->input('longitude');
    	$model->latitude = $request->input('latitude');     
    	$model->area = $request->input('area');
    	$model->others = $request->input('others');
       
        $results = DB::select( DB::raw("select * from per_info where regId =:regId and session_id =:token and r_id = 2  and status = 1"), array(
            'regId' => $reg_id,
            'token' => $token,));
         if($results){
        
             $image = $request->file('image');
         $filename = rand().'.'.$image->getClientOriginalExtension();
    
    	$path = $request->file('image')->move(public_path("../public_html/image"), $filename);
    	//return $path;
        $model->image = url('/image/'.$filename);
                  if($model->save()){
                        return response()->json([
                'status' => 'true',
                'message' => 'Registrarion No. is ',
                'registration_id' => $model->regId
                        ]); 
            
                }

         }else{
            return response()->json([
                'status' => 'false',
                'message' => 'session expired',
                'registration_id' => $model->regId
                        ]);
         }       

}

public function view_data(){
    
    return response()->json(
          $users = DB::table('register')
            ->join('per_info', 'per_info.regId', '=', 'register.verified_by')
            ->select('register.regId','register.fullName','register.phn_no','register.image','register.s_id','per_info.fullName as Created_by')
            ->get()
    );
}
}