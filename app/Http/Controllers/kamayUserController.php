<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Validator;
use Carbon\Carbon;
use App\Services;
use App\Register;
use App\Orders;
use App\Order_details;
use App\Schedule;
use App\Schedule_details;
use App\Address;
use App\Users;
use App\Person;
use App\Assign_jobs;
use App\OrderBookingLogic;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\ApiJazzCashController;

class kamayUserController extends Controller
{
    public function sheduleCronjobs(){
        $current_date = date('Y-m-d H:i:s');
        $date = date("Y-m-d H:i:s", strtotime('+3 hours', strtotime($current_date)));
        
        $data = DB::table('schedule_orders')
                    ->where('shedule_time', '<=', $date)
                    ->where('shedule_status', 0)
                    ->get();
                    
        if(count($data) != 0){
            foreach($data as $datas){
                $schedule_details = DB::table('schedule_order_details')
                                        ->select('*', 's_id as subServiceId', 'amount as unitPrice')
                                        ->where('order_id', $datas->id)
                                        ->get();
                $request = new Request([
                    's_id'   => $datas->s_id,
                    'longitude' => $datas->longitude,
                    'latitude' => $datas->latitude,
                    'full_address' => $datas->full_address,
                    'total_amount' => $datas->total_amount,
                    'payment_method' => $datas->payment_method,
                    'payment_id' => $datas->payment_id,
                    'city' => $datas->city,
                    'cart_details' => json_encode($schedule_details)
                ]);
                DB::table('schedule_orders')
                    ->where('id', $datas->id)
                    ->update(['shedule_status' => 1]);
                    
                $user = DB::table('per_info')->where('regId', $datas->regId)->first();
                $request->headers->set('Authentication', $user->session_id);
                
                $this->order_booking_1($request);
                
            }
        }
        return response()
            ->json(array(
            "status" => "false",
            "message" => "Schedule Cronjob",
            "date" => $data,
            'format' => $date
        ));
    }

    public function cronjobslogic(){
       $this->cronJobsLogicUnassigned();
    }

    public function cronJobsLogicUnassigned(){
         //Check For 6 Minutes
        $check_for_six_minutes = DB::select(
            DB::raw(
                "SELECT orders.order_id, orders.city, orders.latitude, orders.longitude, orders.status,  TIME_TO_SEC(TIMEDIFF(NOW(), create_at)) as diff
                 FROM orders
                 WHERE TIME_TO_SEC(TIMEDIFF(NOW(), orders.create_at)) > 360 
                 AND (orders.status = 'Pending' or status = 'Cancelled')
                 ORDER BY orders.order_id DESC"
            )
        );
        if(count($check_for_six_minutes) != 0){
            foreach($check_for_six_minutes as $check_for_six_minute){
                //Check If 6 Minutes Order User Has Notification
                $check_if_user_has_flag = DB::select(
                    DB::raw(
                        "SELECT *
                         FROM  request_orders
                         WHERE request_orders.order_id  = " . $check_for_six_minute->order_id ."
                         AND request_orders.flag = 1"
                    )
                );
                if(count($check_if_user_has_flag) == 0){
                    
                    $getUser = DB::table('request_orders')
                                    ->where('order_id', $check_for_six_minute->order_id)
                                    ->where('flag', 1)
                                    ->get();
                    foreach ($getUser as $getUsers) {
                        $firebase = DB::table('users')
                                        ->where('regId', $getUsers->reg_id)
                                        ->first();
                        $this->notification_dismissed($firebase->firebase_token, 'notification_dismissed', null, null, null);

                    }
                    DB::table('orders')
                        ->where('order_id', $check_for_six_minute->order_id)
                        ->update(['status' => 'CallCenter']);
                    $check_user = DB::table('request_orders')
                                    ->where('order_id', $check_for_six_minute->order_id)
                                    ->where('flag', 1)
                                    ->first();
                    if($check_user != null){
                        $firebase = DB::table('users')->where('regId',$check_user->reg_id)->first();
                        $this->notification_dismissed($firebase->firebase_token, 'notification_dismissed', null, null, null);
                        DB::table('request_orders')
                            ->where('order_id', $check_for_six_minute->order_id)
                            ->update([
                                'flag' => 1
                            ]);
                        $city_id = $this->getCityID($check_for_six_minute->city);
                        // $city_id = $this->googleCity($check_for_six_minute->latitude, $check_for_six_minute->longitude);
                        $this->getUserByCity($city_id, $check_for_six_minute->order_id);
                        $body = 'callcenter';
                        $this->backOfficeNotification($body, $check_for_six_minute->order_id);
                    }
                }else{

                    foreach($check_if_user_has_flag as $check_if_user_has_flags){
                        $created_at_date = Carbon::parse($check_if_user_has_flags->created_at)->addMinutes(2);
                        $current_date = Carbon::now();
                        $result = $current_date->gte($created_at_date);
                        if($result){
                            DB::table('orders')
                                ->where('order_id', $check_if_user_has_flags->order_id)
                                ->update(['status' => 'CallCenter']);
                            $check_user = DB::table('request_orders')
                                ->where('order_id', $check_if_user_has_flags->order_id)
                                ->where('flag', 1)
                                ->first();

                            $firebase = DB::table('users')->where('regId',$check_user->reg_id)->first();
                            $this->notification_dismissed($firebase->firebase_token, 'notification_dismissed', null, null, null);
                            DB::table('request_orders')
                                ->where('id', $check_if_user_has_flags->id)
                                ->update([
                                    'flag' => 0
                                ]);
                            $city_id = $this->getCityID($check_for_six_minute->city);
                            // $city_id = $this->googleCity($check_for_six_minute->latitude, $check_for_six_minute->longitude);
                            $this->getUserByCity($city_id, $check_for_six_minute->order_id);
                            $body = 'callcenter';
                            $this->backOfficeNotification($body, $check_for_six_minute->order_id);
                        }
                    }
                    
                }
            }
        }

        //Get JOBS HAVING 2 MINUTES
        $unassingned = DB::select(
            DB::raw(
                "SELECT order_id,status, assign_to, request_time
                 FROM orders
                 WHERE TIME_TO_SEC(TIMEDIFF(NOW(), request_time)) >= 120 
                 AND orders.assign_to is null
                 AND orders.status = 'Pending'
                 ORDER BY order_id ASC"
            )
        );
        if(count($unassingned) != 0){
            foreach($unassingned as $unassingneds){
                $getUser = DB::table('request_orders')
                                    ->where('order_id', $unassingneds->order_id)
                                    ->where('flag', 1)
                                    ->get();
                foreach ($getUser as $getUsers) {
                    $firebase = DB::table('users')
                                    ->where('regId', $getUsers->reg_id)
                                    ->first();
                    $this->notification_dismissed($firebase->firebase_token, 'notification_dismissed', null, null, null);

                }

                DB::table('request_orders')
                    ->where('order_id', $unassingneds->order_id)
                    ->update(['flag' => 0]);
            }
            foreach($unassingned as $unassingneds){
                $this->requestInitiate($unassingneds->order_id);
            }
        }else{
            $data = DB::select('SELECT * FROM request_orders WHERE flag = 1 AND created_at <= NOW() - INTERVAL 2 MINUTE');
            foreach ($data as $datas) {
                $firebase = DB::table('users')
                                ->where('regId', $datas->reg_id)
                                ->first();

                $this->notification_dismissed($firebase->firebase_token, 'notification_dismissed', null, null, null);

                DB::table('request_orders')
                        ->where('order_id', $datas->id)
                        ->update(['flag' => 0]);
            }

        }

        //six mint complete - user has flag 1
        $two_minutes = DB::select(
            DB::raw(
                "SELECT * from request_orders
                 WHERE request_orders.flag = 1 
                 AND TIME_TO_SEC(TIMEDIFF(NOW(), request_orders.created_at)) > 120"
            )
        );
        foreach($two_minutes as $two_minute){
            DB::table('request_orders')
                ->where('id', $two_minute->id)
                ->update(['flag' => 0]);
        }


    }
    

    public function getUserByCity($city_id, $order_id){
        $city_kamay = DB::select(
                        DB::raw(
                            "SELECT *
                             FROM  per_info
                             WHERE per_info.city_id  = " . $city_id ."
                             AND per_info.r_id = 1
                             AND per_info.status = 1
                             AND per_info.job_status = 1"
                        )
                    );
        foreach($city_kamay as $city_kamays){
            $firebase_token = $this->getFirbaseToken($city_kamays->regId);
            if($firebase_token != null){
                $this->send_unassgined_notification($firebase_token,  $order_id);
            }
        }
    }

    public function getFirbaseToken($regId){
        $user = DB::table('users')
                    ->select('firebase_token', 'regId')
                    ->where('regId', $regId)
                    ->first();
        return $user->firebase_token;
    }

    public function newcronjobs(){

            $check_for_eight_minutes = DB::select(DB::raw("SELECT orders.order_id, orders.latitude, orders.longitude, orders.status,  TIME_TO_SEC(TIMEDIFF(NOW(), create_at)) as diff FROM orders WHERE TIME_TO_SEC(TIMEDIFF(NOW(), orders.create_at)) > 360 and (orders.status = 'Pending' or status = 'Cancelled')"));
            if(count($check_for_eight_minutes) != 0){
                foreach($check_for_eight_minutes as $check_for_eight_minute){
                    $order = DB::table('orders')
                                ->where('order_id', $check_for_eight_minute->order_id)
                                ->update(['status' => 'CallCenter']);
                    DB::table('request_orders')
                        ->where('order_id', $check_for_eight_minute->order_id)
                        ->update([
                            'flag' => 0
                        ]);
                    $city_id = $this->googleCity($check_for_eight_minute->latitude, $check_for_eight_minute->longitude);
                    $city_kamay = DB::table('per_info')
                                    ->where('r_id', 1)
                                    ->where('status', 1)
                                    ->where('job_status', 1)
                                    ->where('city_id', $city_id)
                                    ->get();
                    foreach($city_kamay as $city_kamays){
                        $firebase_token = DB::table('users')
                                            ->select('firebase_token', 'regId')
                                            ->where('regId', $city_kamays->regId)
                                            ->first();
                        $this->send_unassgined_notification($firebase_token->firebase_token,  $check_for_eight_minute->order_id);
                    }
                }
            }
         $unassingned = DB::select(DB::raw("SELECT order_id,status, assign_to, request_time,CURRENT_TIMESTAMP CURRENT_TIMESTAMP_a,
(DATE_FORMAT(request_time, '%H')*60)
+DATE_FORMAT(request_time, '%i') dtr,
(DATE_FORMAT(CURRENT_TIMESTAMP, '%H')*60)
+DATE_FORMAT(CURRENT_TIMESTAMP, '%i') dtc
,((DATE_FORMAT(CURRENT_TIMESTAMP, '%H')*60)+DATE_FORMAT(CURRENT_TIMESTAMP, '%i'))-((DATE_FORMAT(request_time, '%H')*60)+DATE_FORMAT(request_time, '%i'))  diff
 FROM `orders` 
 where orders.assign_to is null
 and orders.status = 'Pending'
 and ((DATE_FORMAT(CURRENT_TIMESTAMP, '%H')*60)+DATE_FORMAT(CURRENT_TIMESTAMP, '%i'))-((DATE_FORMAT(request_time, '%H')*60)+DATE_FORMAT(request_time, '%i'))>=2 ORDER BY order_id ASC;"));
 
            if(count($unassingned) != 0){
                foreach($unassingned as $unassingneds){
                    $this->requestInitiate($unassingneds->order_id);
                }
            }else{
                $data = DB::select('SELECT * FROM request_orders WHERE flag = 1 AND created_at <= NOW() - INTERVAL 2 MINUTE');
                foreach ($data as $datas) {
                    DB::table('request_orders')
                            ->where('id', $datas->id)
                            ->update(['flag' => 0]);
                }

            }
            
            return response()
                ->json(array(
                "status" => "false",
                "message" => "Authentication Error",
                "user" => $unassingned,
                "date" => 'datetime' 
            ));
    }
    
    public function cronjobs(){
        
        $all_records = DB::select(
            DB::raw("select distinct o.order_id as order_id from order_booking_logic o where status = 0 AND o.order_id not in (select order_id from order_booking_logic a where a.status=2)"));
            
        $rejected = DB::select(
            DB::raw("select distinct o.order_id from order_booking_logic o where status = 1 AND o.order_id not in (select order_id from order_booking_logic a where a.status in (0,2))"));
            
        foreach($all_records as $all_record){
            $records = DB::table('order_booking_logic')
                        ->where('order_id', $all_record->order_id)
                        ->where('status', 0)
                        ->get();
                        
            if(count($records) != 0){
                
                $order = DB::table('orders')
                        ->where('order_id', $records[0]->order_id)
                        ->whereNull('assign_to')
                        ->first();
                
                if(count((array)$order) != 0){
                    $update_record = DB::table('order_booking_logic')
                                          ->where('id', $records[0]->id)
                                          ->update(['status' => 1]);
                    $stoken = DB::table('users')->select('users.firebase_token')
                        ->where([
                                    ['users.r_id', '=', 1],
                                    ['users.regId', '=', $records[0]->reg_id],
                                    ['users.firebase_token', '!=', '']
                                ])
                        
                        ->orderBy('id', 'desc')
                        ->limit(1)
                        ->first();
                    $token = $stoken->firebase_token;
                    $service = DB::table('services')->where('s_id', $order->s_id)->first();
                    $request_category = DB::table('request_orders')->where('order_id', $order->order_id)->where('reg_id',$records[0]->reg_id)->first();
                    $this->send_notification($token,  $order->order_id, $order->longitude, $order->latitude, $order->total_amount, Carbon::parse(now())->format('d-M-y h:iA'), $request_category->distance);
                }
            }
        }
    }
    
    public function getSessionId($session_id)
    {
        if ($session_id != '')
        {
            $user = DB::table('per_info')->where('session_id', $session_id)->first();
            if ($user === null)
            {
                return response()->json(array(
                    "status" => "false",
                    "message" => "Session Expired",
                    "user" => null
                ));
            }
            else
            {
                return '1';
            }
        }
        else
        {
            return response()
                ->json(array(
                "status" => "false",
                "message" => "Authentication Error",
                "user" => null
            ));
        }
    }
    
    public function jobOrderDetails($order_id, Request $request){
        $headers = $request->header('Authentication');
        $response = $this->getSessionId($headers);
        if ($response != '1')
        {
            return $response;
        }
        else
        {
            $order = DB::table('orders')
                        ->select('order_id', 'total_amount', DB::raw('DATE_FORMAT(create_at, "%d-%b-%y %I:%i%p") as service_date'), 'amount_received', 'payment_method', 'assign_to')
                        ->where('order_id', $order_id)
                        ->first();
            
            
            $service_details = DB::table('orders')
                    ->join('order_details', 'order_details.order_id', '=', 'orders.order_id')
                    ->join('services', 'services.s_id', '=', 'order_details.s_id')
                    ->select('order_details.order_id',
                             'services.name', 
                             'order_details.quantity',
                             'orders.assign_to as assign_to'
                    )
                    ->where('orders.order_id', $order_id)
                    ->get();
            
            if(count($service_details) == 0){
                $kamay = DB::table('per_info')
                                    ->leftjoin('city', 'city.city_id', '=', 'per_info.city_id')
                                    ->join('user_services', 'user_services.reg_id', '=', 'per_info.regId')
                                    ->join('services', 'services.s_id', '=', 'user_services.s_id')
                                    ->select('services.s_id as ss_sid',
                                             'user_services.s_id as us_id',
                                             'image',
                                             'fullName as name',
                                             'per_info.regId',
                                             'city.name as location',
                                             DB::raw('group_concat(services.name) as service_name')
                                    )
                                    ->where('per_info.regId', $order->assign_to)
                                    ->first();
                return response()
                    ->json(array(
                        "status" => "true",
                        "message" => "Job Order Details",
                        "service_details" => $service_details,
                        "order" => $order,
                        "kamay" => $kamay
                ));
            }
            if($service_details[0]->assign_to != null){
                $kamay = DB::table('per_info')
                                    ->leftjoin('city', 'city.city_id', '=', 'per_info.city_id')
                                    ->join('user_services', 'user_services.reg_id', '=', 'per_info.regId')
                                    ->join('services', 'services.s_id', '=', 'user_services.s_id')
                                    ->select('services.s_id as ss_sid',
                                             'user_services.s_id as us_id',
                                             'image',
                                             'fullName as name',
                                             'per_info.regId',
                                             'city.name as location',
                                             DB::raw('group_concat(services.name) as service_name')
                                    )
                                    ->where('per_info.regId', $service_details[0]->assign_to)
                                    ->first();
                return response()
                    ->json(array(
                        "status" => "true",
                        "message" => "Job Order Details",
                        "service_details" => $service_details,
                        "kamay" => $kamay,
                        "order" => $order
                ));
            }else{
                $kamay = DB::table('per_info')
                                    ->leftjoin('city', 'city.city_id', '=', 'per_info.city_id')
                                    ->join('user_services', 'user_services.reg_id', '=', 'per_info.regId')
                                    ->join('services', 'services.s_id', '=', 'user_services.s_id')
                                    ->select('services.s_id as ss_sid',
                                             'user_services.s_id as us_id',
                                             'image',
                                             'fullName as name',
                                             'per_info.regId',
                                             'city.name as location',
                                             DB::raw('group_concat(services.name) as service_name')
                                    )
                                    ->where('per_info.regId', $service_details[0]->assign_to)
                                    ->first();
                return response()
                    ->json(array(
                        "status" => "true",
                        "message" => "Job Order Details",
                        "service_details" => $service_details,
                        "kamay" => null,
                        "order" => $order
                ));
            }
                
            
        }
    }
    
    public function userProfileUpdate(Request $request){
        $headers = $request->header('Authentication');
        $response = $this->getSessionId($headers);
        if ($response != '1')
        {
            return $response;
        }
        else
        {
            $user = DB::table('per_info')->where('session_id', $request->header('Authentication'))
                ->first();
            $wordlist = Register::where([
                ['session_id', '=', $request->header('Authentication')]]) ->get();
            if($wordlist->count()>0) {
                $results = DB::select( DB::raw("SELECT COUNT(order_id) as number_of_order FROM `orders` WHERE `regId` = $user->regId AND `status` = 'Completed'"));
                
                if($results > 0){
                    return response()->json(array("status"=>"true", "message"=>"order_status found",'order_status'=>$results));
                }
            }else{
                return response()->json(array("status"=>"false", "message"=>"Sesion Expired",'status'=>array()));
            }
        }
    }
    
    public function kamay_login(Request $request)
    {
        // r_id = 1 // Kamay login
        $r_id = 1;

        $phn_no = $request->input('phn_no');
        $validator = Validator::make($request->all() , ['phn_no' => 'required', ]);
        if ($validator->fails())
        {
            return response()
                ->json(['errors' => $validator->messages() , 'code' => 422, "status" => "false", "message" => "Required Fields."]);
        }
        else
        {
            $results = DB::table('per_info')->where([['phn_no', $phn_no], ['r_id', $r_id]])->select('regId', 'fullName', 'phn_no')
                ->first();

            if ($results)
            {
                $someArray = json_encode($results, true);
                $someArray = json_decode($someArray, true);
                $regId = $someArray["regId"];
                $time = md5(date("h:i:sa"));
                $session = ($time . $regId);
                $value = DB::table('per_info')->where([['phn_no', '=', $phn_no], ['r_id', '=', $r_id]])->update(['session_id' => $session]);
                if ($value = 1)
                {
                    $user_details = DB::table('per_info')->where([['phn_no', $phn_no], ['r_id', $r_id], ['status', 1]])->select('regId', 'fullName', 'phn_no', 'email', 'r_id', 'session_id', 'pri_add', 'pri_lat', 'pri_long', 'image')
                        ->first();
                    return response()
                        ->json(array(
                        "status" => "true",
                        "message" => "Logged in.",
                        "user" => $user_details
                    ));
                }
            }
            else
            {
                return response()->json(array(
                    "status" => "false",
                    "message" => "Mobile number does not exist",
                    "user" => $results
                ));
            }
        }

    }

    public function services()
    {
        $service = Services::where([['status', '=', 1], ['parent_id', '=', 0]])
            ->get(['s_id', 'name', 'service_image', 'slider_image', 'description']);

        $service_slider = DB::table('slider')->select('title', 'image')->where('status', 1)->get();

        return response()->json(array(
                "services" => $service,
                "services_slider" => $service_slider
            ));
        // return response()->json($service);
    }

    public function subServices($s_id)
    {
        return response()->json(Services::where([['parent_id', '!=', 0], ['status', '=', 1], ['parent_id', '=', $s_id]])->get(['s_id', 'name', 'service_image', 'slider_image', 'description', 'price']));
    }

    public function signUp(Request $request)
    {
        $address = new Address;
        $signup = new Register;
        $results = DB::select(DB::raw("select regId, fullName ,phn_no from per_info where phn_no =:phn_no and r_id =:r_id") , array(
            'phn_no' => $request->input('phn_no') ,
            'r_id' => $request->input('r_id') ,
        ));
        
        
        
        $getCityId = DB::table('city')
                  ->select('city_id', 'name')
                  ->where('name' , $request->input('city_id'))
                  ->first();
                  
        $getTerrId = DB::table('territory')
                  ->select('terr_id', 'name')
                  ->where('name' , $request->input('terr_id'))
                  ->first();
             $terrID = 0;     
        if($getTerrId == null){
            
            $getLastTerrId= DB::table('territory')->insert([
                    'name' =>$request->input('terr_id'),
                ]);
            
             $terrID = $getLastTerrId->terr_id;
        }else{
            
            $terrID = $getTerrId->terr_id;
            
        }
                   
        
        
        
        if ($results)
        {
            return response()->json(array(
                "status" => "false",
                "message" => "Mobile number already exist.",
                "user" => array()
            ));
        }
        $signup->fullName = $request->input('fullName');
        $signup->phn_no = $request->input('phn_no');
        $signup->city_id = $getCityId->city_id;
        $signup->terr_id = $terrID;
        $signup->email = $request->input('email');
        $signup->imei1 = $request->input('imei1');
        $signup->r_id = $request->input('r_id');
        $signup->status = 1;
        $address->title = "Home";
        $address->longitude = $request->input('pri_long');
        $address->latitude = $request->input('pri_lat');
        $address->address = $request->input('address');
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $filename = rand() . '.' . $image->getClientOriginalExtension();
            $path = $request->file('image')
            ->move(public_path("../public/image") , $filename);
            $signup->image = url('/image/' . $filename);
        }
        if ($signup->save())
        {
            if (strlen($address->address) > 0)
            {
                $address->regId = $signup->regId;
                $address->save();
            }
            if ($signup->regId > 0)
            {
                $time = md5(date("h:i:sa"));
                $session = ($time . $signup->regId);
                $value = DB::table('per_info')->where('regId', $signup->regId)
                    ->update(['session_id' => $session]);
                if ($value = 1)
                {
                    $user_details = Register::select('regId', 'fullName', 'phn_no', 'email', 'session_id', 'r_id', 'pri_add', 'pri_lat', 'pri_long', 'image')->where('regId', '=', $signup->regId)
                        ->get();
                    
                    /*Curl Data Start*/
                    $user_details_send = DB::table('per_info')
                        ->join('city', 'per_info.city_id' , '=', 'city.city_id')
                        ->join('territory', 'per_info.terr_id' , '=', 'territory.terr_id')
                        ->select('per_info.regId', 'per_info.fullName', 'per_info.phn_no', 'per_info.email', 'per_info.session_id' , 'per_info.image', 'city.name as city_name', 'city.city_id', 'territory.terr_id', 'territory.name as terr_name')
                        ->where('per_info.regId', $signup->regId)
                        ->first();
                    
                    /*Curl Data End*/
                    
                    $users = new Users(['regId' => $signup->regId, 'r_id' => $signup->r_id]);
                    
                    $users->save();

                    $get_cityname = DB::table('city')
                                    ->where('city_id', $signup->city_id)
                                    ->first();
                    $get_terrname = DB::table('territory')
                                    ->where('terr_id', $signup->terr_id)
                                    ->first();
                    $get_address = DB::table('address')
                                    ->where('regId', $signup->regId)
                                    ->first();
                    $client = new \GuzzleHttp\Client(['base_uri' => 'https://shop.kamay.biz/']);

                    $response = $client->request('POST', '/api/user/register', ['form_params' => [
                        'fullName' => $signup->fullName,
                        'phn_no' => $signup->phn_no,
                        'email' => $signup->email,
                        'session_id' => $session,
                        'image' => $signup->image,
                        'city_name' => $get_cityname->name,
                        'terr_name' => $get_terrname->name,
                        'address' => $get_address->address,
                        'title' => $get_address->title,
                        'longitude' => $get_address->longitude,
                        'latitude' => $get_address->latitude,
                        'location_id' => $get_address->id,
                    ]]);
                    
                    return response()
                        ->json(array(
                        "status" => "true",
                        "message" => "Successfull.",
                        "user" => $user_details
                    ));
                }
            }
        }

    }

    public function login(Request $request)
    {

        $phn_no = $request->input('phn_no');
        $r_id = $request->input('r_id');
        $results = DB::select(DB::raw("select regId, fullName ,phn_no from per_info where phn_no =:phn_no and r_id =:r_id") , array(
            'phn_no' => $phn_no,
            'r_id' => $r_id,
        ));

        if ($results)
        {

            $someArray = json_encode($results, true);
            $someArray = json_decode($someArray, true);
            // print_r($someArray);        // Dump all data of the Array
            $regId = $someArray[0]["regId"]; // Access Array data
            $time = md5(date("h:i:sa"));
            $session = ($time . $regId);
            $get_old_sessionID = DB::table('per_info')
                                    ->where('regId', $regId)
                                    ->first();
            $value = DB::table('per_info')->where([['phn_no', '=', $phn_no], ['r_id', '=', $r_id]])->update(['session_id' => $session]);
            // return $value;
            if ($value = 1)
            {
                $user_details = DB::select(DB::raw("select regId,fullName,phn_no,email, r_id, session_id,pri_add, pri_lat, pri_long, image from per_info where phn_no =:phn_no and r_id =:r_id and status = 1") , array(
                    'phn_no' => $phn_no,
                    'r_id' => $r_id,
                ));
                /* CURL FOR SESSION UPDATE WHEN CUSTOMER LOGIN */
                $client = new \GuzzleHttp\Client(['base_uri' => 'https://shop.kamay.biz/']);
                $get_new_session = DB::table('per_info')
                                    ->where('regId', $regId)
                                    ->first();
                $response = $client->request('POST', '/api/user/session', ['form_params' => [
                    'old_session' => $get_old_sessionID->session_id,
                    'new_session' => $get_new_session->session_id,
                ]]);
                return response()->json(array(
                    "status" => "true",
                    "message" => "Logged in.",
                    "user" => $user_details
                ));

            }

        }

        else
        {

            return response()->json(array(
                "status" => "false",
                "message" => "Mobile number does not exist",
                "user" => $results
            ));
        }

    }
    
    public function deleteLocation(Request $request){
        $address_id = $request->input('address_id');
        $headers = $request->header('Authentication');
        $response = $this->getSessionId($headers);
        if ($response != '1')
        {
            return $response;
        }else{
            $user = DB::table('per_info')->where('session_id', $request->header('Authentication'))
                ->first();
            $wordlist = Register::where([['session_id', '=', $user
                ->session_id]])
                ->get();
            if ($wordlist->count() > 0){
                $address = DB::table('address')->where('id', $address_id)->where('regId', $user->regId)->update(['status' => 0]);
                /* DELETE LOCATION FROM SHOP TABLE */
                $client = new \GuzzleHttp\Client(['base_uri' => 'https://shop.kamay.biz/']);

                $response = $client->request('POST', '/api/user/delete_location', ['form_params' => [
                    'location_id' => $address_id,
                ]]);
                
                
                return response()
                    ->json(array(
                    "status" => "true",
                    "message" => "Address is deleted",
                    'addresses' => $address
                ));
            }
        }
    }

    public function locations(Request $request)
    {
        $headers = $request->header('Authentication');
        $response = $this->getSessionId($headers);
        if ($response != '1')
        {
            return $response;
        }
        else
        {
            $user = DB::table('per_info')->where('session_id', $request->header('Authentication'))
                ->first();
            $wordlist = Register::where([['session_id', '=', $user
                ->session_id]])
                ->get();
            if ($wordlist->count() > 0)
            {
                $locations = Address::where('regId', '=', $user->regId)->where('status', 1)
                    ->get(['id', 'regId', 'title', 'longitude','latitude','address']);
                return response()
                    ->json(array(
                    "status" => "true",
                    "message" => "Address Found",
                    'addresses' => $locations
                ));

            }
            else
            {
                return response()->json(array(
                    "status" => "false",
                    "message" => "Sesion Expired",
                    'addresses' => array()
                ));
            }
        }

    }

    public function update_location(Address $id, Request $request)
    {
        $headers = $request->header('Authentication');
        $response = $this->getSessionId($headers);
        if ($response != '1')
        {
            return $response;
        }
        else
        {
            $id->update($request->all());
            return response()->json(array(
                    "status"=>"true", 
                    "message"=>"Address updated"
            ));
        }

    }

    public function locationInsert(Request $request)
    {
        $headers = $request->header('Authentication');
        $response = $this->getSessionId($headers);
        if ($response != '1')
        {
            return $response;
        }
        else
        {
            $results = DB::select(DB::raw("select * from per_info where session_id =:session_id") , array(
                'session_id' => $request->header('Authentication') ,
            ));

            if ($results)
            {
                $address = new Address;
                $address->title = $request->input('title');
                $address->longitude = $request->input('longitude');
                $address->latitude = $request->input('latitude');
                $address->address = $request->input('address');
                $address->regId = $results[0]->regId;
                $address->save();
                /* CUSTOMER LOCATION INSERTED IN SHOP */
                $client = new \GuzzleHttp\Client(['base_uri' => 'https://shop.kamay.biz/']);
                $response = $client->request('POST', '/api/user/location_insert', ['form_params' => [
                    'location_id' => $address->id,
                    'title' => $address->title,
                    'longitude' => $address->longitude,
                    'latitude' => $address->latitude,
                    'address' => $address->address,
                    'session_id' => $request->header('Authentication'),
                ]]);
                return response()
                    ->json(array(
                    "status" => "true",
                    "message" => "Location added"
                ));
            }
            else
            {
                return response()
                    ->json(array(
                    "status" => "false",
                    "message" => "session expired"
                ));

            }

        }

    }

    public function orderInsert(Request $request)
    {

        $results = DB::select(DB::raw("select * from per_info where regId =:regId and session_id =:session_id") , array(
            'regId' => $request->input('regId') ,
            'session_id' => $request->input('session_id') ,
        ));

        $orders = new Orders;
        $order_details = new Order_details;

        if ($results)
        {

            $orders->longitude = $request->input('longitude');
            $orders->latitude = $request->input('latitude');
            $orders->full_address = $request->input('full_address');
            $orders->regId = $request->input('regId');
            $orders->payment_method = $request->input('payment_method');
            $order_details->s_id = $request->input('s_id');
            $orders->total_amount = $request->input('total_amount');
            $order_details->quantity = $request->input('quantity');

        }

        if ($orders->save())
        {
            if (strlen($orders) > 0)
            {
                $orders->order_id = $order_details->order_id;
                $order_details->save();
            }
            return response()
                ->json(array(
                "status" => "true",
                "message" => "Successfull."
            ));
        }

    }

    public function order_booking(Request $request)
    {
        $headers = $request->header('Authentication');
        $response = $this->getSessionId($headers);
        if ($response != '1')
        {
            return $response;
        }
        else
        {
            $results = DB::select(DB::raw("select * from per_info where session_id =:session_id") , array(
                'session_id' => $request->header('Authentication') ,
            ));

            $user = DB::table('per_info')->where('session_id', $request->header('Authentication'))
                ->first();

            if ($results)
            { //json string form App end
                $i = 0;
                $order = new Orders;
                $assign_jobs = new Assign_jobs;
                $order->s_id = $request->input('s_id');
                $order->longitude = $request->input('longitude');
                $order->latitude = $request->input('latitude');
                $order->full_address = $request->input('full_address');
                $order->regId = $user->regId;
                $order->total_amount = $request->input('total_amount');
                $order->status = 'Pending';
                $order->payment_method = $request->input('payment_method');
                $cart_details_array = $request->input('cart_details');
    
                if ($order->save())
                { // decode json string into  array
                    $arrayName = json_decode($cart_details_array, true);
                    $assign_jobs->order_id = $order->order_id;
                    $assign_jobs->save();
    
                    // printing array values
                    foreach ($arrayName as $row)
                    {
                        $order_details = new Order_details;
    
                        $quantity = $arrayName[$i]['quantity'];
                        $subServiceId = $arrayName[$i]['subServiceId'];
                        $unitPrice = $arrayName[$i]['unitPrice'];
                        $total = $unitPrice * $quantity;
    
                        $order_details->order_id = $order->order_id;
                        $order_details->quantity = $quantity;
                        $order_details->s_id = $subServiceId;
                        $order_details->amount = $total;
    
                        $order_details->save();
    
                        $i++;
    
                    }
                    //Temp work before radius
                    
                    //ORDER DETAILS LOGIC
                    $circle_radius = 3959;
                    $max_distance = 3;
                    $lat = $request->input('latitude');
                    $lng = $request->input('longitude');

                    $results = DB::select(
                        'SELECT * FROM
                            (SELECT status, s_id, verufied_by, regId , fullName , r_id , nickName , pri_lat, job_status, pri_long, (' . $circle_radius . ' * acos(cos(radians(' . $lat . ')) * cos(radians(pri_lat)) *
                                cos(radians(pri_long) - radians(' . $lng . ')) +
                                sin(radians(' . $lat . ')) * sin(radians(pri_lat))))
                                AS distance
                                FROM per_info) AS distances
                            WHERE distance < ' . $max_distance . '
                            AND r_id = 1
                            AND job_status = 1
                            AND verufied_by IS NOT NULL
                            AND status = 1
                            AND regId NOT IN (SELECT worker_id FROM order_info_v WHERE status = "InProgress" and worker_id is not null)
                            AND FIND_IN_SET(' .$order->s_id .' , s_id) > 0
                            ORDER BY distance
                            LIMIT 5;
                        ');
                        
                    if(count($results) != 0 ){
                        foreach ($results as $result) {
                            $OrderBookingLogic = new OrderBookingLogic();
                            $OrderBookingLogic->reg_id = $result->regId;
                            $OrderBookingLogic->status = 0;
                            $OrderBookingLogic->order_id = $order->order_id;
                            $OrderBookingLogic->save();
                        }
                    }
                    
                    

                    $stoken = DB::table('users')->select('users.firebase_token')
                        ->where([['users.r_id', '=', 1], ['users.firebase_token', '!=', '']])
                        ->orderBy('id', 'desc')
                        ->limit(1)
                        ->get();
                    
    
                    $token = $stoken[0]->firebase_token;
                    //$this->send_notification($token,  $order->order_id, $order->longitude, $order->latitude, $order->total_amount, Carbon::parse(now())->format('dM H:i:s'), $request->input('service_name'), $request->input('service_image'));
    
                    return response()
                        ->json(array(
                        "status" => "true",
                        "message" => "Successfull."
                    ));
                }
    
                // $json_string = '[{"quantity":3,"service_id":4,"service_image":"https://ichef.bbci.co.uk/news/320/media/images/80319000/jpg/_80319922_183953925.jpg","service_name":"Plumber","subServiceId":9,"subServiceImage":"https://i.ytimg.com/vi/oEGHtB5XV_0/maxresdefault.jpg","subServiceName":"Tank Repair","totalAmount":600,"unitPrice":200},{"quantity":2,"service_id":4,"service_image":"https://ichef.bbci.co.uk/news/320/media/images/80319000/jpg/_80319922_183953925.jpg","service_name":"Plumber","subServiceId":10,"subServiceImage":"https://images.adsttc.com/media/images/5d9c/4216/284d/d1a1/9b00/02fe/slideshow/feature_-279A0964.jpg?1570521591","subServiceName":"Regular Tap","totalAmount":800,"unitPrice":100}]';
                
    
                // echo $elementCount;
                // return response()->json(array("status"=>"true", "message"=>"Location added"));
                
            }
            else
            {
                return response()
                    ->json(array(
                    "status" => "false",
                    "message" => "session expired"
                ));
    
            }

        }

    }
    
    public function order_booking_1(Request $request)
    {
        $headers = $request->header('Authentication');
        $response = $this->getSessionId($headers);
        if ($response != '1')
        {
            return $response;
        }
        else
        {   
            $results = DB::table('per_info')->where('session_id', $request->header('Authentication'))->first();
            if ($results)
            { 
                
                $i = 0;
                $order = new Orders;
                $assign_jobs = new Assign_jobs;
                $order->s_id = $request->input('s_id');
                $order->longitude = $request->input('longitude');
                $order->latitude = $request->input('latitude');
                $order->full_address = $request->input('full_address');
                $order->regId = $results->regId;
                $order->total_amount = $request->input('total_amount');
                $order->status = 'Pending';
                $order->payment_method = $request->input('payment_method');
                $cart_details_array = $request->input('cart_details');
                $order->payment_id = $request->input('payment_id');
                $order->city = $request->input('city');
                if ($order->save())
                { 
                    $arrayName = json_decode($cart_details_array, true);
                    $assign_jobs->order_id = $order->order_id;
                    $assign_jobs->save();
    
                    foreach ($arrayName as $row)
                    {
                        $order_details = new Order_details;
    
                        $quantity = $arrayName[$i]['quantity'];
                        $subServiceId = $arrayName[$i]['subServiceId'];
                        $unitPrice = $arrayName[$i]['unitPrice'];
                        $total = $unitPrice * $quantity;
    
                        $order_details->order_id = $order->order_id;
                        $order_details->quantity = $quantity;
                        $order_details->s_id = $subServiceId;
                        $order_details->amount = $total;
                        $order_details->save();
                        $services_share = DB::table('services_share')->where('service_id', $subServiceId)->get();
                        if($services_share != null){
                            foreach($services_share as $services_shares){
                                DB::table('order_share')->insert([
                                    'order_id' => $order->order_id,
                                    'share_id' => $services_shares->share_id,
                                    'order_details_id' => $order_details->order_details_id,
                                    'price' => $services_shares->share_actual_price * $order_details->quantity,
                                    'quantity' => $order_details->quantity,
                                ]);
                            }
                        }
                        $i++;
    
                    }

                    $order_share_data = DB::table('order_share')
                        ->selectRaw('*, sum(price) as sum')
                        ->groupBy('share_id')
                        ->where('order_id', $order->order_id)
                        ->get();
                    if(count($order_share_data) != 0){
                        for($i = 0; $i < count($order_share_data); $i++){
                            DB::table('order_share_total_amount')->insert([
                                'order_id' => $order->order_id,
                                'share_id' => $order_share_data[$i]->share_id,
                                'total_amount' => $order_share_data[$i]->sum
                            ]);
                        }
                    }

                    //Temp work before radius
                    $datetime = Carbon::parse($order->request_time)
                                        ->subMinutes(2)
                                        ->format('Y-m-d H:i:s');
                    DB::table('orders')
                            ->where('order_id', $order->order_id)
                            ->update(['request_time' => $datetime]);

                    // $returnValue = $this->requestInitiate($order->order_id);
    
                    return response()
                        ->json(array(
                        "status" => "true",
                        "message" => "Your booking has been confirmed. \nKama will contact you shortly",
                        "returnValue" => '',
                        "order" => $order->order_id
                    ));
                }
                
            }
            else
            {
                return response()
                    ->json(array(
                    "status" => "false",
                    "message" => "session expired"
                ));
    
            }

        }

    }
    
    
    public function shedule_booking(Request $request)
    {
        
        $headers = $request->header('Authentication');
        $response = $this->getSessionId($headers);
        if ($response != '1')
        {
            return $response;
        }
        else
        {
            $results = DB::select(DB::raw("select * from per_info where session_id =:session_id") , array(
                'session_id' => $request->header('Authentication') ,
            ));

            $user = DB::table('per_info')->where('session_id', $request->header('Authentication'))
                ->first();

            if ($results)
            { 
                $end_date = Carbon::parse($request->input('shedule_time'));
                $now = Carbon::now();
                if(!Carbon::parse($request->input('shedule_time'))->gt(Carbon::now())){
                    return response()
                        ->json(array(
                        "status" => "error",
                        "message" => "Invalid Date"
                    ));
                }
                
                $i = 0;
                $order = new Schedule;
                // $assign_jobs = new Assign_jobs;
                $order->s_id = $request->input('s_id');
                $order->longitude = $request->input('longitude');
                $order->latitude = $request->input('latitude');
                $order->full_address = $request->input('full_address');
                $order->regId = $user->regId;
                $order->total_amount = $request->input('total_amount');
                $order->status = 'Pending';
                $order->shedule_time = $request->input('shedule_time');
                $order->payment_method = $request->input('payment_method');
                $order->city = $request->input('city');
                $cart_details_array = $request->input('cart_details');
                $order->payment_id = $request->input('payment_id');
                if ($order->save())
                { 
                    $arrayName = json_decode($cart_details_array, true);
                    // $assign_jobs->order_id = $order->order_id;
                    // $assign_jobs->save();
    
                    foreach ($arrayName as $row)
                    {
                        $order_details = new Schedule_details;
    
                        $quantity = $arrayName[$i]['quantity'];
                        $subServiceId = $arrayName[$i]['subServiceId'];
                        $unitPrice = $arrayName[$i]['unitPrice'];
                        $total = $unitPrice * $quantity;
    
                        $order_details->order_id = $order->id;
                        $order_details->quantity = $quantity;
                        $order_details->s_id = $subServiceId;
                        $order_details->amount = $total;
    
                        $order_details->save();
    
                        $i++;
    
                    }
                    //Temp work before radius
                    
                    return response()
                        ->json(array(
                        "status" => "true",
                        "message" => "Your booking has been confirmed for " . date('l jS F h:i A', strtotime($request->input('shedule_time'))) .". Kama will contact you few hours before your selected time.",
                        "order" => $order->id
                    ));
                }
                
            }
            else
            {
                return response()
                    ->json(array(
                    "status" => "false",
                    "message" => "session expired"
                ));
    
            }

        }

    }

    public function faqs($r_id){
        $faq = DB::table('faq_table')
                ->where('role_id', $r_id)
                ->get();

        return response()
            ->json(array(
            "status" => "true",
            "message" => "Faqs expired",
            "faq_list" => $faq
        ));
    }
    
    public function check_assigned(){
        $order = DB::table('orders')
            ->where('orders.assign_to', '!=', null)
            ->get();
        foreach($order as $orders){
            $request_order = DB::table('request_orders')
                ->where('request_orders.order_id', '=', $orders->order_id)
                ->get();
                
            foreach($request_order as $request_orders){
                DB::table('request_orders_history')->insert([
                    'order_id' => $request_orders->order_id,
                    'reg_id' => $request_orders->reg_id
                ]);
            }
            DB::table('request_orders')->where('request_orders.order_id', '=', $orders->order_id)->delete();
        }
        return response()
            ->json(array(
            "status" => "false",
            "message" => "session expired",
            "data" => $order
        ));
    }
    
    public function requestInitiate($order_id){
        $order = DB::table('orders')->where('order_id', $order_id)->first();
        if(is_null($order->assign_to)){
            $latitude = $order->latitude;
            $longitude = $order->longitude;
            $circle_radius = 3959;
            $max_distance = $order->custom_distance;
            $lat = $latitude;
            $lng = $longitude;

            $results = DB::select(
                        'SELECT * FROM
                            per_info
                            WHERE r_id = 1
                            AND job_status = 1
                            AND verufied_by IS NOT NULL
                            AND status = 1
                            AND regId NOT IN (SELECT worker_id FROM order_info_v WHERE status = "InProgress" and worker_id is not null)
                            AND regId NOT IN (select b.reg_id FROM request_orders b WHERE b.order_id = ' . $order->order_id . ')
                            AND regId NOT IN (select c.reg_id FROM request_orders c WHERE c.flag = 1)
                            AND FIND_IN_SET(' .$order->s_id .' , s_id) > 0;
                        ');
            $distance_array = array() ;           
            for($i = 0; $i < count($results); $i++){
                $per_lat = $results[$i]->pri_lat;
                $pri_long = $results[$i]->pri_long;
                $per = $per_lat.','.$pri_long;
                $order_lat = $lat.','.$lng;
                $distance = $this->googleMapFunction($per, $order_lat);
                $distance_pieces = explode(" ", $distance);
                
                $distance_array[] = array(
                                        "distance" => $distance_pieces[0],
                                        "unit" => $distance_pieces[1],
                                        "user_id" => $results[$i]->regId,
                                        "id" => $i
                                        );
            }
            
            
            foreach ($distance_array as $key => $row) {
                $distance[$key]  = $row['distance'];
                $id[$key] = $row['id'];
            }
            
            // as of PHP 5.5.0 you can use array_column() instead of the above code
            $distance  = array_column($distance_array, 'distance');
            $id = array_column($distance_array, 'id');
            
            // Sort the data with volume descending, edition ascending
            // Add $data as the last parameter, to sort by the common key
            array_multisort($distance, SORT_ASC, $id, SORT_ASC, $distance_array);
                
                // AND regId not in (select b.reg_id FROM request_orders b WHERE b.order_id = ' . $order->order_id . ')
            if(count($distance_array) != 0){
                if((float)$distance_array[0]['distance'] <= $order->custom_distance){

                    DB::table('request_orders')->insert(
                        [   
                            'order_id' => $order->order_id,
                            'reg_id' => $distance_array[0]['user_id'],
                            'distance' => $distance_array[0]['distance'],
                            'flag' => 1
                        ]
                    );

                    $old_user = DB::table('request_orders')
                                    ->where('reg_id', '!=', $distance_array[0]['user_id'])
                                    ->where('order_id', $order->order_id)
                                    ->where('flag', 1)
                                    ->first();
                    if($old_user != null){
                        $old_user = DB::table('users')
                                    ->where('regId', $old_user->reg_id)
                                    ->first();
                        $this->notification_dismissed($old_user->firebase_token, 'notification_dismissed', null, null, null);
                    }
                    
                    DB::table('orders')
                          ->where('order_id', $order->order_id)
                          ->update(['request_time' => Carbon::now()]);
                          
                    $stoken = DB::table('users')->select('users.firebase_token')
                                ->where([
                                            ['users.r_id', '=', 1],
                                            ['users.regId', '=', $distance_array[0]['user_id']],
                                            ['users.firebase_token', '!=', '']
                                        ])
                                
                                ->orderBy('id', 'desc')
                                ->limit(1)
                                ->first();
                    if($stoken != null){
                        $token = $stoken->firebase_token;
                    }else{
                        $token = '';
                    }
                    $service = DB::table('services')->where('s_id', $order->s_id)->first();
                    $request_orders = DB::table('request_orders')->where('order_id', $order->order_id)->where('reg_id',$distance_array[0]['user_id'])->first();
                    $this->send_notification($token,  $order->order_id, $order->longitude, $order->latitude, $order->total_amount, Carbon::parse(now())->format('d-M-y h:iA'), $request_orders->distance);
                    return $results;
                }else{
                    DB::table('orders')
                        ->where('order_id', $order_id)
                        ->update(['custom_distance' => 5]);
                    $this->requestInitiate($order_id);
                    return response()
                        ->json(array(
                        "status" => "No Kamay Found",
                        "message" => $results
                    ));
                }
                
            }else{
                DB::table('orders')
                    ->where('order_id', $order_id)
                    ->update(['custom_distance' => 5]);
                return response()
                        ->json(array(
                        "status" => "No Kamay Found",
                        "message" => $results
                    ));
            }
        }else{
            return response()
                ->json(array(
                "status" => "Assigned To Someone"
            ));
        }
        
    }
    

    public function orderList(Request $request)
    {
        $headers = $request->header('Authentication');
        $response = $this->getSessionId($headers);
        if ($response != '1')
        {
            return $response;
        }
        else
        {
            $user = DB::table('per_info')->where('session_id', $request->header('Authentication'))
                ->first();
            $wordlist = Register::where([['session_id', '=', $request->header('Authentication')]])->get();
            if ($wordlist->count() > 0)
            {
                 $results = DB::select( DB::raw("select os.order_id,DATE_FORMAT(os.create_at, '%d-%b-%y') as create_at,DATE_FORMAT(os.create_at, '%h:%i %p') as job_time , os.status, os.total_amount, (select name from services where s_id = os.s_id) as service_name, (select service_image from services where s_id = os.s_id) as service_image,os.payment_method, os.full_address from orders os where os.regId =:regId AND os.status != 'Completed' group by os.order_id ORDER BY os.order_id DESC"), array(
         'regId' => $user->regId,));
         
                return response()->json(array(
                    "status" => "true",
                    "message" => "orders List",
                    'my_order' => $results
                ));

            }
            else
            {
                return response()->json(array(
                    "status" => "false",
                    "message" => "Sesion Expired",
                    'my_order' => array()
                ));
            }
        }

    }

    public function kamaProfile(Request $request, $order_id)
    {
        $headers = $request->header('Authentication');
        $response = $this->getSessionId($headers);
        if ($response != '1')
        {
            return $response;
        }
        else
        {
            $wordlist = Register::where([['session_id', '=', $request->header('Authentication')]])->get();

            if ($wordlist->count() > 0)
            {
                $users = DB::table('per_info')
                    ->join('orders', 'per_info.regId', '=', 'orders.assign_to')
                    ->join('user_services', 'user_services.reg_id', '=', 'per_info.regId')
                    ->join('services', 'user_services.s_id', '=', 'services.s_id')

                    ->join('city', 'city.city_id', '=', 'per_info.city_id')
                    ->select('per_info.fullName', 'per_info.image', 'per_info.vehicle_no', DB::raw('group_concat(services.name) as expertise'), 'city.name', 'per_info.phn_no', 'per_info.regId', 'per_info.pri_lat','per_info.email', 'per_info.pri_long', 'per_info.cnic','per_info.rating_number','per_info.rating_count')
                    ->where('orders.order_id', '=', $order_id)->get();
                if ($users[0]->fullName == null) {
                    return response()->json(array(
                        "status" => "true",
                        "message" => "Kama not Found",
                        'kama_profile' => []
                    ));
                }
                if ($users->count() > 0)
                {

                    if($users[0]->rating_number != 0){
                        $users[0]->ratings = $users[0]->rating_number / $users[0]->rating_count;
                    }else{
                        $users[0]->ratings = 0;
                    }

                    return response()
                        ->json(array(
                        "status" => "true",
                        "message" => "Kama Found",
                        'kama_profile' => $users
                    ));
                }

                else
                {
                    return response()->json(array(
                        "status" => "true",
                        "message" => "Kama not Found",
                        'kama_profile' => $users
                    ));
                }
            }

            else
            {
                return response()->json(array(
                    "status" => "false",
                    "message" => "Sesion Expired",
                    'addresses' => array()
                ));
            }
        }
    }
    
    public function updateLocations(Request $request,  Address $id){
        $headers = $request->header('Authentication');
        $response = $this->getSessionId($headers);
        if ($response != '1')
        {
            return $response;
        }
        else
        {
            $id->update($request->all());
            /* UPDATE LOCATION IN SHOP TABLE */
            $client = new \GuzzleHttp\Client(['base_uri' => 'https://shop.kamay.biz/']);

            $response = $client->request('POST', '/api/user/update_location', ['form_params' => [
                'location_id' => $id->id,
                'title' => $request->input('title'),
                'address' => $request->input('address'),
                'session_id' => $request->header('Authentication'),
            ]]);
            return response()->json(array(
                           "status"=>"true", 
                           "message"=>"Profile updated"
                                   ));
        }
   }

    public function updateProfile(Request $request){
        $headers = $request->header('Authentication');
        $response = $this->getSessionId($headers);
        if ($response != '1')
        {
            return $response;
        }
        else
        {
            $user = DB::table('per_info')->where('session_id', $request->header('Authentication'))->first();
            $regId = Register::where('regId', $user->regId)->first();
            $regId->update($request->all());
            return response()->json(array(
                           "status"=>"true", 
                           "message"=>"Profile updated"
                                   ));
        }
   }

    public function totalBookingCounts(Request $request){
        $headers = $request->header('Authentication');
        $response = $this->getSessionId($headers);
        if ($response != '1')
        {
            return $response;
        }
        else
        {
            $user = DB::table('per_info')->where('session_id', $request->header('Authentication'))->first();
            $wordlist = Register::where([['session_id', '=', $user->session_id]])->get();
            if($wordlist->count()>0) {                   
                $results = DB::select( DB::raw("SELECT COUNT(order_id) as number_of_order FROM `orders` WHERE `regId` = $user->regId AND `status` = 'Completed'"));
                if($results > 0){
                    return response()->json(array("status"=>"true", "message"=> $results[0]->number_of_order));    
                }
            }
            else{
                return response()->json(array("status"=>"false", "message"=>"Sesion Expired"));
            }
        }
    }

    public function customerCheckOrderStatus(Request $request,  $order_id)
    {
        $headers = $request->header('Authentication');
        $response = $this->getSessionId($headers);
        if ($response != '1')
        {
            return $response;
        }
        else
        {
            $wordlist = Register::where([['session_id', '=', $request->header('Authentication')]])->get();

            if ($wordlist->count() > 0)
            {
                
               $users = DB::table('orders')  
                ->join('per_info', 'per_info.regId', '=', 'orders.regId')
                ->join('order_details', 'order_details.order_id', '=', 'orders.order_id')
                ->join('services', 'services.s_id', '=', 'order_details.s_id')
                ->selectRaw("orders.latitude, orders.longitude, orders.assign_to ,orders.order_id, orders.rate_by_customer, orders.status , orders.estimated_time, orders.total_amount, orders.arrived_at, DATE_FORMAT(orders.create_at, '%d-%b-%y') as create_at, DATE_FORMAT(orders.create_at, '%I:%i %p') as job_time, COUNT('order_details.s_id') as service_count, ifnull(GROUP_CONCAT(services.name),'Only Visit Charges') AS services_name")
                ->where('orders.order_id', '=', $order_id)
                ->get();

                foreach($users as $user){
                    if($user->rate_by_customer == null){
                        $user->rate_by_customer = 0;
                    }
                    if($user->assign_to != null){
                        $kama_name = DB::table('per_info')->where('regId', $user->assign_to)->first();
                        $user->kama_name = $kama_name->fullName;
                    }else{
                        $user->kama_name = '';
                    }

                }

                return response()
                    ->json(array(
                    "status" => "true",
                    "message" => "order_status found",
                    'order_status' => $users
                ));
            }

            else
            {
                return response()->json(array(
                    "status" => "false",
                    "message" => "Sesion Expired",
                    'status' => array()
                ));
            }

        }
    }

    public function customerInvoice(Request $request, $order_id)
    {
        $headers = $request->header('Authentication');
        $response = $this->getSessionId($headers);
        if ($response != '1')
        {
            return $response;
        }
        else
        {
            $wordlist = Register::where([['session_id', '=', $request->header('Authentication')]])->get();

            if ($wordlist->count() > 0)
            {

                $users = DB::select(DB::raw("select (SELECT per_info.fullName FROM per_info WHERE regId = os.assign_to) as kama_name,od.order_id,(select  count(distinct 1) from order_details o1,services s1
                                where o1.s_id=s1.s_id
                                and o1.order_id=os.order_id
                                and o1.add_work=1) as additional_work, os.amount_received, os.total_amount,os.payment_id, GROUP_CONCAT(s.name) as services, COUNT(s.name) as no_of_services, (select name from services where s_id = s.parent_id) as service_name, curdate() as old_service_date, os.payment_method , DATE_FORMAT(os.create_at, '%d-%b-%y') as service_date from order_details od, services s, orders os WHERE od.order_id = :order_id and s.s_id = od.s_id and os.order_id = od.order_id group by os.order_id
                    ") , array(
                    'order_id' => $order_id,
                ));
                
                if(count($users) == 0){
                    $results = DB::select( DB::raw("select per_info.fullName as kama_name, os.order_id, IFNULL(os.payment_id, 1), os.total_amount, os.amount_received, (select IFNULL(GROUP_CONCAT(s1.name),'Only Visit Charges') from order_details o1,services s1
                                where o1.s_id=s1.s_id
                                and o1.order_id=os.order_id) as services, (select IFNULL(COUNT(s1.name),0) from order_details o1,services s1
                                where o1.s_id=s1.s_id
                                and o1.order_id=os.order_id) as no_of_services, (select  count(distinct 1) from order_details o1,services s1
                                where o1.s_id=s1.s_id
                                and o1.order_id=os.order_id
                                and o1.add_work=1) as additional_work , os.start_job_date, os.end_job_date, os.s_id as service_id , (select name from services where s_id = os.s_id) as service_name, DATE_FORMAT(os.create_at, '%d-%b-%y') as service_date, os.payment_id, os.payment_method, os.arrived_at,os.status from   services s, orders os, per_info 
                                WHERE os.order_id = :order_id  and os.assign_to = per_info.regID
                                group by os.order_id"), array('order_id' => $order_id,));
                    $payment_option = DB::table('payment_option')->where('status', 1)->get();
                    return response()->json(array(
                        "status" => "true",
                        "message" => "Customer Invoice 0",
                        'invoice' =>  $results,
                        'payment_options' => $payment_option
                    ));
                }else{
                    $payment_option = DB::table('payment_option')->where('status', 1)->get();
                    return response()->json(array(
                        "status" => "true",
                        "message" => "Customer Invoice",
                        'invoice' => $users,
                        'payment_options' => $payment_option
                    ));
                }
            }

            else
            {
                return response()->json(array(
                    "status" => "false",
                    "message" => "Sesion Expired",
                    "invoice" => array()
                ));
            }
        }
    }
    
    public function updateImage(Request $request){
        $headers = $request->header('Authentication');
        $response = $this->getSessionId($headers);
        if ($response != '1')
        {
            return $response;
        }
        else
        {
            if ($request->hasFile('image')) {
                $image = $request->file('image');
                $filename = rand() . '.' . $image->getClientOriginalExtension();
                $path = $request->file('image')
                ->move(public_path("../public/image") , $filename);
                $user = DB::table('per_info')->where('session_id', $request->header('Authentication'))->update(['image' => url('/image/' . $filename)]);
                if($user == 1){
                    $data = DB::table('per_info')->select('regId', 'image')->where('session_id', $request->header('Authentication'))->first();
                    /* UPDATE IMAGE ON SHOP TABLE */
                    $client = new \GuzzleHttp\Client(['base_uri' => 'https://shop.kamay.biz/']);
                    $response = $client->request('POST', '/api/user/update_profile_image', ['form_params' => [
                        'session_id' => $request->header('Authentication'),
                        'image' => url('/image/' . $filename),
                    ]]);
                    return response()
                        ->json(array(
                        "status" => "true",
                        "message" => "Update Image",
                        "data" => $data,
                    )); 
                }else{
                    return response()
                        ->json(array(
                        "status" => "false",
                        "message" => "Image Not Update",
                        "data" => "",
                    )); 
                }
            }
        }
    }

    public function rateKama(Request $request, $order_id)
    {
        $headers = $request->header('Authentication');
        $response = $this->getSessionId($headers);
        if ($response != '1')
        {
            return $response;
        }
        else
        {
            $wordlist = Register::where([['session_id', '=', $request->header('Authentication')]])->get();

            if ($wordlist->count() > 0)
            {
                $orders = Orders::where('order_id', $order_id)->first();
                
                if($orders == null){
                    return response()
                        ->json(array(
                        "status" => "true",
                        "message" => "order updated",
                        "data" => $orders,
                    ));   
                }else{

                    if($request->has('rate_by_customer')) {
                        $rate = $request->input('rate_by_customer');
                        $rate_count = (int)$rate;
                        $kama_regid = DB::table('per_info')
                                            ->where('regId', $orders->assign_to)
                                            ->first();
                        if($rate_count < 3){
                            $supervisor_id = DB::table('per_info')
                                                ->join('users', 'users.regId', '=', 'per_info.regId')
                                                ->where('per_info.regId', $kama_regid->assigned_supervisor)
                                                ->first();
                            $getNotification = DB::table('notifications')->where('id', 11)->first();
                            $getbody = $getNotification->body;

                            $search2 = '$orders->order_id';
                            $getbody = str_replace($search2, $orders->order_id, $getbody) ;

                            $text = $getNotification->title;
                            $body = $getbody;

                            $this->notification_dismissed($supervisor_id->firebase_token, 'poor_reviews', $text, $body, $order_id);
                        }
                        $rating_number = $kama_regid->rating_number + $request->input('rate_by_customer');
                        $rating_count = $kama_regid->rating_count + 1;
                        DB::table('per_info')
                            ->where('regId', $kama_regid->regId)
                            ->update([
                                'rating_number' => $rating_number,
                                'rating_count' => $rating_count
                            ]);
                    }
                    if ($request->input('status') == 'Estimate Accepted') {
                        $getKamaToken = DB::table('users')
                                                ->where('users.regId', $orders->assign_to)
                                                ->first();
                        $notification = db::table('notifications')->where('id', 8)->first();
                        $this->notification_dismissed($getKamaToken->firebase_token, 'estimate_accepted', $notification->title, $notification->body, $order_id);
                    }


                    $orders->update($request->all());
                    return response()
                        ->json(array(
                        "status" => "true",
                        "message" => "order updated",
                        "data" => $orders,
                    ));
                }
            }

            else
            {
                return response()
                    ->json(array(
                    "status" => "false",
                    "message" => "Sesion Expired"
                ));

            }
        }
    }

    public function backOfficeNotification($body, $regId = null){ 
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://backoffice.kamay.biz/send-notification/$body/$regId");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        curl_close($ch);     
    }

    public function send_unassgined_notification($token, $order_id){
        $order = DB::table('orders')->where('order_id',$order_id)->first();
        $service = DB::table('services')->where('s_id', $order->s_id)->first();
        $url = "https://fcm.googleapis.com/fcm/send";
        $serverKey = 'AAAAwHKXicQ:APA91bEsDHSUpggTIpe0d3aM19NoG3WTFzH7bcmgQm-mVpghtkOSYVa_D70wLfgitImGi1pKAjjInVMpQfTCUmS8XfWxE-cJHQ_PvZ6tfCtD7YNVz3EE3tunnYEHo6z26c_1lF1ApKOP';
        $getNotification = DB::table('notifications')->where('id', 12)->first();
        $gettitle = $getNotification->title;
        $search =  '$service->name';
        $gettitle = str_replace($search, $service->name, $gettitle) ;

        $getbody = $getNotification->body;
        $search1 = '$order->total_amount';
        $search2 = '$order_id';
        $search3 = '$order->full_address';
        $getbody = str_replace($search1, $order->total_amount, $getbody) ;
        $getbody = str_replace($search2, $order_id, $getbody);
        $getbody = str_replace($search3, $order->full_address, $getbody);

        $text = $gettitle;
        $body = $getbody;

        $data = array(
            // "order" => $order,
            "order_id" => $order_id,
            "flag" => 'unassigned',
            'title' => $title,
            'body' => $body
        );
        $notification = array(
            'title' => $title,
            'body' => $body,
            'sound' => 'default',
            'badge' => '1'
        );
        $arrayToSend = array(
            'to' => $token,
            // 'notification' => $notification,
            'priority' => 'high',
            'data' => $data
        );
        $json = json_encode($arrayToSend);
        $headers = array();
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Authorization: key=' . $serverKey;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        //Send the request
        $response = curl_exec($ch);
        //Close request
        if ($response === false)
        {
            die('FCM Send Error: ' . curl_error($ch));
        }
        curl_close($ch);
    }

    public function send_notification_test()
    {
        $url = "https://fcm.googleapis.com/fcm/send";

        //$token = "eGK3k4t8RBiKmyiqQQjjjY:APA91bFex_QPCX1OluKDUEKAz4jNszwBJDZuu1wQz-r9YnwIdOzg9jit1vX8KsuWKiVUc_lNv1CJ7CTEWZO6OGOusAj4k8dNSMybeB29EmEJIgCpYfIBnKH98RT3Co_M51VbS3X2GT3J";
        //// $token = "fYTLx-sHS96IpD_S8ZEHrW:APA91bGL4tPHiM-OjocfXPfkO79hOuIT9CsWCcc50CGyDznPs_WjptJn52t4pwVt6Ea9D2Y4UGXYMS9PwiFt74WfYVNM1grAbLeND-BhicxqgjvLk7z2vKTDFcgWYY70UlBsQZYpEHML";
        // $serverKey = 'AAAAPai5eTo:APA91bGAS-xDMQpSytsstRwKcOrHnug3CyGjQ1vdqwYi2t2g0Ne1dpO59lW7oQXtDVKQazWhK-KUu4bjmC2L932rUFIajcF8ddhFzMQ6zIVP-hkavs8e-huXwwEzEmqLF_bpkud3tsup';
        $order_id = 945;
        $longitude = "213123213";
        $latitude = "213123213";
        $total_amount = 12390;
        $created_at = "time";
        $distance = "2 km";
        $serverKey = 'AAAAwHKXicQ:APA91bEsDHSUpggTIpe0d3aM19NoG3WTFzH7bcmgQm-mVpghtkOSYVa_D70wLfgitImGi1pKAjjInVMpQfTCUmS8XfWxE-cJHQ_PvZ6tfCtD7YNVz3EE3tunnYEHo6z26c_1lF1ApKOP';
        $title = "New Booking";
        $body = "new job arrived and Job id#" . 945;
        $order = DB::table('orders')->where('order_id',945)->first();
        $service = DB::table('services')->where('s_id', 2)->first();
        $token ="dWEnWBl4Q-ukV5NObFiHzI:APA91bEDwkEG--oN2FqL2qDk8jTfVd4nL-DKozy50a14GzXaru9AHYGz3BPNRY3WEBjpos9SAddXZKnJzi6qWSh2596hDRi8RByVmRRP6z4vRro-RBOy61pbivSzTBOTa15uFzRTWhLH";
        $data = array(
            // "order" => $order,
            "order_id" => $order_id,
            "service_name" => $service->name,
            "longitude" => $longitude,
            "latitude" => $latitude,
            "total_amount" => $total_amount,
            "created_at" => $created_at,
            "service_image" => $service->service_image,
            "distance" => $distance,
            "flag" => 'new_booking',
            'title' => $title,
            'body' => $body

        );
        $notification = array(
            'title' => $title,
            'body' => $body,
            'sound' => 'default',
            'badge' => '1'
        );
        $arrayToSend = array(
            'to' => $token,
            // 'notification' => $notification,
            'priority' => 'high',
            'data' => $data
        );
        $json = json_encode($arrayToSend);
        $headers = array();
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Authorization: key=' . $serverKey;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        //Send the request
        $response = curl_exec($ch);
        //Close request
        if ($response === false)
        {
            die('FCM Send Error: ' . curl_error($ch));
        }
        curl_close($ch);

    }


    public function send_notification($token, $order_id, $longitude, $latitude, $total_amount, $created_at, $distance)
    {
        $url = "https://fcm.googleapis.com/fcm/send";

        //$token = "eGK3k4t8RBiKmyiqQQjjjY:APA91bFex_QPCX1OluKDUEKAz4jNszwBJDZuu1wQz-r9YnwIdOzg9jit1vX8KsuWKiVUc_lNv1CJ7CTEWZO6OGOusAj4k8dNSMybeB29EmEJIgCpYfIBnKH98RT3Co_M51VbS3X2GT3J";
        //// $token = "fYTLx-sHS96IpD_S8ZEHrW:APA91bGL4tPHiM-OjocfXPfkO79hOuIT9CsWCcc50CGyDznPs_WjptJn52t4pwVt6Ea9D2Y4UGXYMS9PwiFt74WfYVNM1grAbLeND-BhicxqgjvLk7z2vKTDFcgWYY70UlBsQZYpEHML";
        // $serverKey = 'AAAAPai5eTo:APA91bGAS-xDMQpSytsstRwKcOrHnug3CyGjQ1vdqwYi2t2g0Ne1dpO59lW7oQXtDVKQazWhK-KUu4bjmC2L932rUFIajcF8ddhFzMQ6zIVP-hkavs8e-huXwwEzEmqLF_bpkud3tsup';
        $serverKey = 'AAAAwHKXicQ:APA91bEsDHSUpggTIpe0d3aM19NoG3WTFzH7bcmgQm-mVpghtkOSYVa_D70wLfgitImGi1pKAjjInVMpQfTCUmS8XfWxE-cJHQ_PvZ6tfCtD7YNVz3EE3tunnYEHo6z26c_1lF1ApKOP';
        $title = "New Booking";
        $body = "new job arrived and Job id#" . $order_id;
        $order = DB::table('orders')->where('order_id',$order_id)->first();
        $service = DB::table('services')->where('s_id', $order->s_id)->first();
        $data = array(
            // "order" => $order,
            "order_id" => $order_id,
            "service_name" => $service->name,
            "longitude" => $longitude,
            "latitude" => $latitude,
            "total_amount" => $total_amount,
            "created_at" => $created_at,
            "service_image" => $service->service_image,
            "distance" => $distance,
            "flag" => 'new_booking',
            'title' => $title,
            'body' => $body

        );
        $notification = array(
            'title' => $title,
            'body' => $body,
            'sound' => 'default',
            'badge' => '1'
        );
        $arrayToSend = array(
            'to' => $token,
            // 'notification' => $notification,
            'priority' => 'high',
            'data' => $data
        );
        $json = json_encode($arrayToSend);
        $headers = array();
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Authorization: key=' . $serverKey;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        //Send the request
        $response = curl_exec($ch);
        //Close request
        if ($response === false)
        {
            die('FCM Send Error: ' . curl_error($ch));
        }
        curl_close($ch);

    }
    
    public function panic(Request $request){
        $headers = $request->header('Authentication');
        $response = $this->getSessionId($headers);
        if ($response != '1')
        {
            return $response;
        }
        else
        {
            $user = DB::table('per_info')->where('session_id', $request->header('Authentication'))
                ->first();
            $order_id = $request->input('order_id');
            $reg_id = $user->regId;
            $role_id = $user->r_id;
            $reason = $request->input('reason');
            $order = DB::table('orders')->where('order_id', $order_id)->first();
            $get_user = DB::table('per_info')->where('regId', $order->assign_to)->first();
            $checkData = DB::table('panic_reported')
                        ->where([
                                    ['order_id', $order_id],
                                    ['reg_id', $reg_id]
                                ])
                        ->first();
            if($checkData != null){
                
                 return response()
                    ->json(array(
                    "status" => "true",
                    "message" => "You have already action that panic",
                    "data" => false
                ));
                
            }
        
            $results = DB::table('panic_reported')->insertGetId(
                [
                    'order_id' => $order_id,
                    'reg_id' => $reg_id,
                    'role_id' => $role_id,
                    'reason' => $reason,
                    'status' => 2,
                    'supervisor_id' => $get_user->assigned_supervisor
                ]
            );

            DB::table('admin_notify')->insert([
                'notify_status' => 3,
                'panic_id'=> $results

            ]);
            
            $panic_details = DB::table('per_info')
                                    ->join('orders', 'per_info.regId', '=', 'orders.regId')
                                    ->join('per_info as worker_info', 'orders.assign_to', '=', 'worker_info.regId')
                                    ->select(
                                        'per_info.fullName AS reported_full_name',
                                        'per_info.r_id AS reported_role_id',
                                        'per_info.image as image',
                                        'per_info.phn_no as phone_number',
                                        'orders.order_id AS order_id',
                                        'orders.assign_to as assign_to',
                                        'worker_info.fullName AS worker_full_name',
                                        'worker_info.assigned_supervisor AS supervisor_id'
                                    )
                                    //->where('per_info.assigned_supervisor', $user->regId)
                                    ->where('orders.order_id', $order_id)
                                    ->first();
                                    
            //dump($panic_details);die();
            $getKamaFirbaseToken = DB::table('users')->select('users.firebase_token')
                        ->where('users.regId', '=', $panic_details->supervisor_id)
                        ->first();
            
            $getNotification = DB::table('notifications')->where('id', 10)->first();
            $getbody = $getNotification->body;

            $search2 = '$panic_details->order_id';
            $getbody = str_replace($search2, $panic_details->order_id, $getbody);

            $text = $getNotification->title;
            $body = $getbody;

            $flag = "panic";

            $this->panic_send_notification($getKamaFirbaseToken->firebase_token,  $panic_details->order_id, $text, $body, $flag);
            
            $body = 'customer_panic';
            $this->backOfficeNotification($body, $results);
            return response()->json(array("status"=>"true", "message"=>"Panic generated",'data'=>true));
        }
    }

    public function panic_send_notification($firebase_token, $order_id, $text, $body, $flag = null){
        $url = "https://fcm.googleapis.com/fcm/send";
        $serverKey = 'AAAAwHKXicQ:APA91bEsDHSUpggTIpe0d3aM19NoG3WTFzH7bcmgQm-mVpghtkOSYVa_D70wLfgitImGi1pKAjjInVMpQfTCUmS8XfWxE-cJHQ_PvZ6tfCtD7YNVz3EE3tunnYEHo6z26c_1lF1ApKOP';
        $data = array(
            "title" => $text,
            'body' => $body,
            'flag' => $flag
        );
        $notification = array(
            'title' => $text,
            'body' => $body,
            'sound' => 'default',
            'badge' => '1',
        );
        $arrayToSend = array(
            'to' => $firebase_token,
            // 'notification' => $notification,
            'priority' => 'high',
            'data' => $data
        );
        $json = json_encode($arrayToSend);
        $headers = array();
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Authorization: key=' . $serverKey;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        //Send the request
        $response = curl_exec($ch);
        //Close request
        if ($response === false)
        {
            die('FCM Send Error: ' . curl_error($ch));
        }
        curl_close($ch);
    }
    
    public function scheduleJobs(Request $request){
        $headers = $request->header('Authentication');
        $response = $this->getSessionId($headers);
        if ($response != '1')
        {
            return $response;
        }
        else
        {
            $value = '';
            $user = DB::table('per_info')->where('session_id', $request->header('Authentication'))->first();
            
           
            $value = 'schedule';
                    $schedual = DB::table('schedule_orders')
                                ->join('services', 'services.s_id', '=', 'schedule_orders.s_id')
                                ->join('per_info', 'per_info.regId', '=', 'schedule_orders.regId')
                                ->select(
                                    'schedule_orders.id AS order_id',
                                    'schedule_orders.regId AS customer_id',
                                    DB::raw('DATE_FORMAT(schedule_orders.shedule_time, "%d-%b-%y %h:%m %p") as job_date'),
                                    'per_info.fullname as worker_name',
                                    'per_info.image as customer_image',
                                    'per_info.email as customer_email',
                                    'per_info.phn_no as customer_phone',
                                    'services.s_id AS service_id',
                                    'services.name AS service_name',
                                    'services.service_image AS service_image',
                                    'schedule_orders.full_address AS full_address',
                                    'schedule_orders.payment_method AS payment_method',
                                    'schedule_orders.total_amount as total_amount'
                                )
                                ->where('per_info.regId',$user->regId)
                                ->orderBy('schedule_orders.id', 'desc')
                                ->get(); 
            
                return response()
                    ->json(array(
                    "status" => "true",
                    "message" => "Schedule jobs for customer",
                    "schedule_jobs" => $schedual,
                ));
            }
    }
    
    public function scheduleJobsDetails($order_id, Request $request){
        $headers = $request->header('Authentication');
        $response = $this->getSessionId($headers);
        if ($response != '1')
        {
            return $response;
        }
        else
        {
                $order = DB::table('schedule_orders')
                                ->select('id', 'total_amount', DB::raw('DATE_FORMAT(shedule_time, "%d-%b-%y %h:%m %p") as service_date'), 'amount_received', 'payment_method', 'assign_to')
                                ->where('id', $order_id)
                                ->first();
                    
                    
                    $service_details = DB::table('schedule_orders')
                            ->join('schedule_order_details', 'schedule_order_details.order_id', '=', 'schedule_orders.id')
                            ->join('services', 'services.s_id', '=', 'schedule_order_details.s_id')
                            ->select('schedule_order_details.order_id',
                                     'services.name', 
                                     'schedule_order_details.quantity',
                                     'schedule_orders.assign_to as assign_to'
                            )
                            ->where('schedule_orders.id', $order_id)
                            ->get();
                
                return response()
                    ->json(array(
                        "status" => "true",
                        "message" => "Job Order Details",
                        "service_details" => $service_details,
                        "order" => $order,
                ));
        }
    }

    public function getPaymentList(Request $request){
        $headers = $request->header('Authentication');
        $response = $this->getSessionId($headers);
        if ($response != '1'){
            return $response;
        }
        else{
            $payment_option = DB::table('payment_option')->where('status', 1)->get();
            return response()
                    ->json(array(
                    "status" => "true",
                    "message" => "Payment List",
                    "data" => $payment_option,
                ));
        }
    }

    public function getOrderCompleted(Request $request){
        $headers = $request->header('Authentication');
        $response = $this->getSessionId($headers);
        if ($response != '1')
        {
            return $response;
        }
        else
        {
            $user = DB::table('per_info')->where('session_id', $request->header('Authentication'))
                ->first();
            $wordlist = Register::where([['session_id', '=', $request->header('Authentication')]])->get();
            if ($wordlist->count() > 0)
            {
                 $results = DB::select( DB::raw("select os.order_id,DATE_FORMAT(os.create_at, '%d-%b-%y') as create_at,DATE_FORMAT(os.create_at, '%h:%i %p') as job_time , os.status, os.total_amount, (select name from services where s_id = os.s_id) as service_name, (select service_image from services where s_id = os.s_id) as service_image,os.payment_method, os.full_address from orders os where os.regId =:regId AND os.status = 'Completed' group by os.order_id ORDER BY os.order_id DESC"), array(
         'regId' => $user->regId,));
         
                return response()->json(array(
                    "status" => "true",
                    "message" => "orders List",
                    'my_order' => $results
                ));

            }
            else
            {
                return response()->json(array(
                    "status" => "false",
                    "message" => "Sesion Expired",
                    'my_order' => array()
                ));
            }
        }
    }

    public function notification_dismissed($token, $flag, $title = null, $body = null, $order_id){
        $url = "https://fcm.googleapis.com/fcm/send";
        $serverKey = 'AAAAwHKXicQ:APA91bEsDHSUpggTIpe0d3aM19NoG3WTFzH7bcmgQm-mVpghtkOSYVa_D70wLfgitImGi1pKAjjInVMpQfTCUmS8XfWxE-cJHQ_PvZ6tfCtD7YNVz3EE3tunnYEHo6z26c_1lF1ApKOP';
        $data = array(
            "flag" => $flag,
            "order_id" => $order_id,
            'title' => $title,
            'body' => $body
        );
        $notification = array(
            'title' => $title,
            'body' => $body,
            'sound' => 'default',
            'badge' => '1',
            'flag' => $flag,
        );
        $arrayToSend = array(
            'to' => $token,
            'priority' => 'high',
            'data' => $data
        );
        $json = json_encode($arrayToSend);
        $headers = array();
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Authorization: key=' . $serverKey;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        //Send the request
        $response = curl_exec($ch);
        //Close request
        if ($response === false)
        {
            die('FCM Send Error: ' . curl_error($ch));
        }
        curl_close($ch);

    }
    
    public function isAlive(){
        $per_info = DB::table('per_info')->where('r_id', 1)->where('job_status', 1)->get();
        foreach($per_info as $per_infos){  
            $firebase_tokken = DB::table('users')->where('regId', $per_infos->regId)->first();
            $flag = "is_alive";
            $url = "https://fcm.googleapis.com/fcm/send";
            $serverKey = 'AAAAwHKXicQ:APA91bEsDHSUpggTIpe0d3aM19NoG3WTFzH7bcmgQm-mVpghtkOSYVa_D70wLfgitImGi1pKAjjInVMpQfTCUmS8XfWxE-cJHQ_PvZ6tfCtD7YNVz3EE3tunnYEHo6z26c_1lF1ApKOP';
            $data = array(
                "flag" => $flag,
            );
            $notification = array(
                'title' => '',
                'body' => '',
                'sound' => 'default',
                'badge' => '1',
                'flag' => $flag,
            );
            $arrayToSend = array(
                'to' => $firebase_tokken->firebase_token,
                // 'notification' => $notification,
                'priority' => 'high',
                'data' => $data
            );
            $json = json_encode($arrayToSend);
            $headers = array();
            $headers[] = 'Content-Type: application/json';
            $headers[] = 'Authorization: key=' . $serverKey;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            //Send the request
            $response = curl_exec($ch);
            //Close request
            if ($response === false)
            {
                die('FCM Send Error: ' . curl_error($ch));
            }
            curl_close($ch);
            DB::table('per_info')->where('regId', $per_infos->regId)->update(array('job_status' => 0));
        }
    }

    public function getCityID($city_name){
        $city_id = DB::table('city')->whereRaw('lower(name) like (?)',["%{$city_name}%"])->first();
        return $city_id;
    }

    public function googleCity($lat, $long){
        $distance_data = file_get_contents(
            'https://maps.googleapis.com/maps/api/geocode/json?latlng='.$lat.','.$long.'&key=AIzaSyAggB51ZRomTQ28c6SIlT0Zkjtd2boQ3V0'
        );
        $distance_arr = json_decode($distance_data);  
        $add = $distance_arr->results[0]->formatted_address;
        $value = explode(",", $add);
        $count = count($value);
        $country = $value[$count-1];
        $state = $value[$count-2];
        $city = $value[$count-3];
        $city = str_replace("City","", $city);
        $city = trim($city);
        $city_db = DB::table('city')->whereRaw('lower(name) like (?)',["%{$city}%"])->first();
        return $city_db->city_id;
    }


    public function googleMapFunction($from_latlong, $to_latlong){
        $distance_data = file_get_contents(
                                'https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&origins='.$from_latlong.'&destinations='.$to_latlong.'&key=AIzaSyAggB51ZRomTQ28c6SIlT0Zkjtd2boQ3V0'
                            );
        $distance_arr = json_decode($distance_data);
        $pieces = explode(" ", $distance_arr->rows[0]->elements[0]->distance->text);
        $number = (float) $pieces[0];
        if($pieces[1] == 'm'){
            $number = $number * 0.001;
        }
        return $number . ' km';
    }

    public function payment(Request $request){
        $headers = $request->header('Authentication');
        $response = $this->getSessionId($headers);
        if ($response != '1'){
            return $response;
        }else{

            $order_id = $request->input('order_id');
            $payment_id = $request->input('payment_id');
            $card_number = $request->input('card_number');
            $card_expiry = $request->input('card_expiry');
            $card_cvv = $request->input('card_cvv');
            $user = DB::table('per_info')->where('session_id', $request->header('Authentication'))->first();
            $order = DB::table('orders')->where('order_id', $order_id)->first();
            $date1 = Carbon::createFromFormat('my', $card_expiry);
            $date2 = Carbon::now()->format('my');
            $date2 = Carbon::createFromFormat('my', $date2);
            $result = $date1->gte($date2);
            if($result){
                //true
                $jc = new ApiJazzCashController;

                // request type 
                $jc->reqType = 'PAY';;
                // request data
                $jc->set_data([
                    "pp_IsRegisteredCustomer" => "No",
                    "pp_ShouldTokenizeCardNumber" => "No",
                    "pp_CustomerID" => $user->regId,
                    "pp_CustomerEmail" => $user->email,
                    "pp_CustomerMobile" => $user->phn_no,
                    "pp_TxnType" => "MPAY",
                    "pp_TxnRefNo" => "T".date('YmdHis'),
                    "pp_Amount" => $order->total_amount * 100,
                    "pp_TxnCurrency" => "PKR",
                    "pp_TxnDateTime" => date("YmdHis"),
                    "pp_C3DSecureID" => "",
                    "pp_BillReference" => "billRef",
                    "pp_Description" => "Description of transaction of Order Id " . $order_id,
                    "pp_CustomerCardNumber" => $card_number,
                    "pp_CustomerCardExpiry" => $card_expiry,
                    "pp_CustomerCardCvv" => $card_cvv,
                ]);

                $response_send = $jc->send();
                $response = json_decode($response_send);
                $responseCode = (int) $response->responseCode;
                if($responseCode != 0){
                    //not Success

                }else{
                    //Success
                    $payment_option = DB::table('payment_option')->where('id', $request->get('payment_id'))->first();
                    DB::table('orders')
                        ->where('order_id', $order_id)
                        ->update([
                            'amount_received' => $order->total_amount,
                            'payment_id' => $request->get('payment_id'),
                            'payment_method' => $payment_option->name
                        ]);
                }
            }else{
                return response()
                    ->json(array(
                    "status" => "error",
                    "message" => "Invalid Card Expiry"
                ));
            }
            
            return response()
                ->json(array(
                "status" => "true",
                "message" => "Payment Return",
                "data" => $response,
            ));

        }
    }

}

