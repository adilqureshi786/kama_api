<?php

namespace App\Http\Controllers;
use App\Orders;

use Illuminate\Http\Request;

class PushNotificationController extends Controller
{

    public function getSessionId($session_id)
    {
        if ($session_id != '')
        {
            $user = DB::table('per_info')->where('session_id', $session_id)->first();
            if ($user === null)
            {
                return response()->json(array(
                    "status" => "false",
                    "message" => "User Not Exist",
                    "user" => null
                ));
            }
            else
            {
                return '1';
            }
        }
        else
        {
            return response()
                ->json(array(
                "status" => "false",
                "message" => "Authentication Error",
                "user" => null
            ));
        }
    }


    public function send_notification()
	{
        $url = "https://fcm.googleapis.com/fcm/send";

        $serverKey = 'AAAAwHKXicQ:APA91bEsDHSUpggTIpe0d3aM19NoG3WTFzH7bcmgQm-mVpghtkOSYVa_D70wLfgitImGi1pKAjjInVMpQfTCUmS8XfWxE-cJHQ_PvZ6tfCtD7YNVz3EE3tunnYEHo6z26c_1lF1ApKOP';
        $token = "eD_U9W0yT-WP9dK1pimJ6k:APA91bGNkZbH8namu9sieqkjAnGOx6wHKgC4VQGz-RemFrRCtnSco3acy4xmAvZwQitcqnEU998C4jIcNv1JI3Ag25YPQ7hwRwlb4r7BNJtDYQrNSlonDZsG1rqpeFXvHmaFiDJd92TX";
        $title = "Testig New Booking";
        $body = "new job arrived";
        $data = array (
            "order_id" => "49", 
            "service_name" => "Plumber",
            "longitude" => "67.07111343741417",
            "latitude" => "24.873271541333704",
            "total_amount" => "800",
            "created_at" => "2020-09-19 02:00:04",
            "service_image" => "https://4.imimg.com/data4/XT/WR/ANDROID-28709303/product-500x500.jpeg" 
          
        );
        $notification = array('title' =>$title , 'body' => $body, 'sound' => 'default', 'badge' => '1', 'title' =>$title);
        $arrayToSend = array('to' => $token, 'notification' => $notification,'priority'=>'high','data'=> $data);
        $json = json_encode($arrayToSend);
        $headers = array();
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Authorization: key='. $serverKey;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST,"POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        curl_setopt($ch, CURLOPT_HTTPHEADER,$headers);
        //Send the request
        $response = curl_exec($ch);
        //Close request
        if ($response === FALSE) {
            die('FCM Send Error: ' . curl_error($ch));
        }
        curl_close($ch);   
      
    }
}
