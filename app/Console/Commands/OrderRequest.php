<?php

namespace App\Console\Commands;
use App\OrderBookingLogic;
use App\Person;
use DB;
use Illuminate\Console\Command;

class OrderRequest extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:request';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Kamay Request every 2 minutes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Status = 0 Null
        // Status = 1 Send Request
        // Status = 2 Accepted

        $max = OrderBookingLogic::max('order_id');
        $checkIfSend = DB::select(DB::raw("select * from order_booking_logic where order_id =:order_id AND status = 2") , array(
            'order_id' => $max,
        ));
        if(count($checkIfSend) == 0){
            //Not Accept By Any One
            $orderLogics = DB::select(DB::raw("select * from order_booking_logic where order_id =:order_id AND status = 0") , array(
                'order_id' => $max,
            ));
            if(count($orderLogics) > 0){
                $result = OrderBookingLogic::find($orderLogics[0]->id);
                $result->status = 1;
                $result->save();
            }else{
                $this->info('Send to All Kamay, Not Accepted by any Kamay');
            }
        }else{
            print_r($checkIfSend);
            $this->info('Accept By One Kamay');
        }
    }
}
