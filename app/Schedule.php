<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    protected $table = 'schedule_orders';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = [
    	'regId',
    	'longitude',
    	'latitude',
    	'total_Amount',
    	'create_at',
    	'is_accept',
    	'status',
    	'payment_method',
    	'amount_received',
    	'remark_by_kama',
        'rate_by_kama',
        'estimated_time',
        'arrived_at',
        'remark_by_customer',
        'rate_by_customer',
        'shedule_time',
        'payment_id',
        'city'
    ];
    
    public function shedule_details()
    {
        return $this->hasMany('App\Schedule_details');
    }
}
