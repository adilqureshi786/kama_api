<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $table = 'address';
    public $timestamps = false;
    protected $primaryKey = 'id';
    protected $fillable = [
				'title',
				'longitude',
				'latitude',
				'address'
				
    ];
    
     public function per_info()
    {
        return $this->hasMany('App\Register');
    }
}
