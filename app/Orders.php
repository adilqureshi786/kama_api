<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
    protected $table = 'orders';
    protected $primaryKey = 'order_id';
    public $timestamps = false;
    protected $fillable = [
    	'regId',
    	'longitude',
    	'latitude',
    	'total_Amount',
    	'create_at',
    	'is_accept',
    	'status',
    	'payment_method',
        'payment_id',
    	'amount_received',
    	'remark_by_kama',
        'rate_by_kama',
        'estimated_time',
        'arrived_at',
        'remark_by_customer',
        'rate_by_customer',
        'custom_distance',
        'city'
    ];
    
    public function order_details()
    {
        return $this->hasMany('App\Order_details');
    }
}
