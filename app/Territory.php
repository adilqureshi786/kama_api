<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Territory extends Model
{
    protected $table = 'territory';
    protected $primaryKey = 'terr_id';
}
