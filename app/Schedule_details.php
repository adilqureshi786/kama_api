<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Schedule_details extends Model
{
     protected $table = 'schedule_order_details';
    protected $primaryKey = 'id';
	public $timestamps = false;
	// protected $dates = ['created_at', '	start_job_date', 'end_job_date'];
    protected $fillable = [
    	'order_id',
    	's_id',
    	'amount',
    	'quatity'
];
    
    public function schedule()
  {
    return $this->belongsTo('App\Schedule', 'id');
  }
}
