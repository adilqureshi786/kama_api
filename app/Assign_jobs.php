<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Assign_jobs extends Model
{
    protected $table = 'assign_jobs';
    protected $primaryKey = 'order_id';
    public $timestamps = false;
    protected $fillable = [
    	'regId',
    	'suggest',	
    	'is_accept',
    	'order_id'
    ];
}
