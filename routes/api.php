<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//KAMAY WORKER START r_id = 1
//Route::post('kamay/login','kamayUserController@kamay_login');
Route::get('shedule_cronjobs','kamayUserController@sheduleCronjobs');
Route::get('cronjobs_old','kamayUserController@cronjobs');
// Route::get('cronjobs','kamayUserController@newcronjobs');
Route::get('cronjobs','kamayUserController@cronjobslogic');
Route::get('cronjobs_logic','kamayUserController@cronjobslogic');
Route::get('check_assigned_cronjobs','kamayUserController@check_assigned');
Route::get('kamay/services','ShowProfile@getServices');
Route::get('kamay/cities','ShowProfile@showCities');
Route::get('kamay/territories/{city_id}','ShowProfile@showTerritories');
Route::post('kamay/register','ShowProfile@userRegistration');
Route::post('kamay/sendsms','SmsController@sendSmsApiFunc');
Route::post('kamay/user_exist','ShowProfile@userExistence');
Route::post('kamay/supervisor/profile','ShowProfile@supervisor');
Route::get('kamay/active_jobs','ShowProfile@activeJobs');
Route::get('kamay/unassigned/{latitude}/{longitude}','ShowProfile@unassignedJobs');
Route::get('kamay/order_details/{order_id}','ShowProfile@orderDetails');
Route::put('kamay/is_accept/{order}','ShowProfile@isAccept');
Route::get('kamay/myBooking','ShowProfile@viewMyBooking');
Route::get('kamay/datetime','ShowProfile@timecurrent');
Route::put('kamya/order_start/{order}','ShowProfile@orderStart');
Route::get('kamay/customer_detail/{order_id}','ShowProfile@userOrder');
Route::get('kamay/customer_order_detail/{order_id}','ShowProfile@customerOrderDetail');
Route::put('kamay/job_status','ShowProfile@jobStatus');
Route::get('kamay/payments','ShowProfile@payment');
Route::put('kamay/updateFirebaseToken','ShowProfile@updateFirebaseToken');
Route::put('kamay/update_kama_location','ShowProfile@updateKamaLocation');
Route::get('kamay/addwork/{order_id}','ShowProfile@addWork');
Route::get('kamay/add_remove_services/{order_details_id}','ShowProfile@addWorkRemove');
Route::post('kamay/add_customer_services','ShowProfile@addOrders');
Route::get('kamay/user_profile_update','ShowProfile@userProfileUpdate');
//dateTime 1 for start update and 2 for end update
Route::put('kamay/update_order_time/{order_id}/{value}','ShowProfile@updateOrderTime');
Route::put('kamay/order_completed/{order_id}','ShowProfile@orderCompleted');
Route::get('kamay/check_order_status','ShowProfile@checkOrderStatus');
Route::post('kamay/panic', 'ShowProfile@panic');
Route::get('kamay/user_sub_services/{s_id}','kamayUserController@subServices');
Route::post('kamay/apply_job','ShowProfile@applyJob');
Route::get('kamay/is_alive_new','kamayUserController@isAlive');
Route::get('kamay/get_faq/{r_id}','kamayUserController@faqs');
Route::get('kamay/kama_profile','ShowProfile@myKamaProfile');
Route::get('test/{city_id}/{order_id}','kamayUserController@getUserByCity');

//KAMAY WORKER END

//Notification
Route::get('push','PushNotificationController@send_notification');

//Token Authentication START // CAUTION
Route::get('/login/{phn_no}/{password}','SessionController@login_details');
Route::post('/registeration','SessionController@insert_data');
Route::get('/view','SessionController@view_data');
//Token Authentication END
 
//KAMAY CUSTOMER START r_id = 3
Route::post('customer/sendsms','SmsController@sendApiCall1');
Route::get('customer/cities','ShowProfile@showCities');
Route::get('customer/territories/{city_id}','ShowProfile@showTerritories');
Route::put('customer/profile_update', 'kamayUserController@updateProfile');
Route::get('customer/user_services','kamayUserController@services');
Route::get('customer/user_sub_services/{s_id}','kamayUserController@subServices');
Route::post('customer/signUp','kamayUserController@signUp');
Route::post('customer/customerLogin','kamayUserController@login');
Route::get('customer/locations','kamayUserController@locations');
Route::put('customer/update_location/{id}','kamayUserController@updateLocations');
Route::post('customer/locationInsert','kamayUserController@locationInsert');
Route::post('customer/orderBookingOld','kamayUserController@order_booking');
Route::post('customer/orderBooking','kamayUserController@order_booking_1');
Route::post('customer/schedule_booking','kamayUserController@shedule_booking');
Route::post('customer/requestInitiate/{order_id}','kamayUserController@requestInitiate');
Route::get('customer/customer_order_list','kamayUserController@orderList');
Route::get('customer/kama_profile/{order_id}','kamayUserController@kamaProfile');
Route::get('customer/customer_check_order_status/{order_id}','kamayUserController@customerCheckOrderStatus');
Route::get('customer/customer_invoice/{order_id}','kamayUserController@customerInvoice');
Route::put('customer/order_update/{order_id}','kamayUserController@rateKama');
Route::get('customer/total_bookings_count','kamayUserController@totalBookingCounts');
Route::get('customer/user_profile_update','kamayUserController@userProfileUpdate');
Route::put('customer/updateFirebaseToken','ShowProfile@updateFirebaseToken');
Route::get('customer/job_order_details/{order_id}','kamayUserController@jobOrderDetails');
Route::post('customer/update_image','kamayUserController@updateImage');
Route::post('customer/delete_location','kamayUserController@deleteLocation');
Route::post('customer/panic', 'kamayUserController@panic');
Route::get('customer/schedule_jobs', 'kamayUserController@scheduleJobs');
Route::get('customer/schedule_jobs_details/{order_id}', 'kamayUserController@scheduleJobsDetails');
Route::get('customer/google-map/{order_id}', 'kamayUserController@requestInitiate');
Route::get('customer/get_payment_list', 'kamayUserController@getPaymentList');
Route::post('customer/payment', 'kamayUserController@payment');
Route::get('customer/get_faq/{r_id}','kamayUserController@faqs');
Route::get('customer/get_completed_order','kamayUserController@getOrderCompleted');
//KAMAY CUSTOMER END
 
//KAMAY SUPERVISOR START r_id = 2
Route::post('supervisor/sendsms','SmsController@sendApiCall1');
Route::post('supervisor/login', 'SupervisorController@loginSupervisor');
Route::get('supervisor/dashboard/{duration}', 'SupervisorController@supervisorDashboard');
Route::get('supervisor/panic_details/{duration}/{status}', 'SupervisorController@supervisorPanicDetails');
Route::get('supervisor/new_jobs/{duration}', 'SupervisorController@supervisorNewJobs');
Route::get('supervisor/new_jobs_details/{id}', 'SupervisorController@supervisorNewJobsDetails');
Route::get('supervisor/new_kamay', 'SupervisorController@newKamay');
Route::get('supervisor/review_checklist/{id}', 'SupervisorController@reviewChecklist');
Route::get('supervisor/new_jobs_status/{duration}', 'SupervisorController@supervisorNewJobsStatus');
Route::get('supervisor/all_job', 'SupervisorController@supervisorAllJob');
Route::get('supervisor/incomplete_doc', 'SupervisorController@incompleteDoc');
Route::get('supervisor/my_kamay', 'SupervisorController@myKamay');
Route::get('supervisor/my_kamay_map', 'SupervisorController@myKamayMap');
Route::get('supervisor/nearby_kamay/{distance}', 'SupervisorController@nearByKamay');
Route::post('supervisor/checklist_update', 'SupervisorController@checklistUpdate');
Route::get('supervisor/order_details/{order_id}','SupervisorController@orderDetails');
Route::get('supervisor/jobs_status_supervisor/{status}/{duration}', 'SupervisorController@JobsStatusForSupervisor');
Route::get('supervisor/poor_reviews/{duration}', 'SupervisorController@getPoorReviews');
Route::post('supervisor/get_profile','SupervisorController@supervisorProfile');
Route::get('supervisor/kama_profile/{id}','SupervisorController@kamaProfile');
Route::post('supervisor/update_panic','SupervisorController@updatePanic');
Route::put('supervisor/updateFirebaseToken','ShowProfile@updateFirebaseToken');
Route::get('supervisor/get_faq/{r_id}','kamayUserController@faqs');
Route::get('supervisor/schedule_jobs_details/{order_id}', 'SupervisorController@scheduleOrderDetails');

//KAMAY SUPERVISOR END



//KAMAY VENDOR 
Route::get('vendor_service','RegisterController@vendorProducts');
Route::post('create_vendor','RegisterController@createVendor');
